"""
settings.py

Configuration for Flask app

Important: Place your keys in the secret_keys.py module,
           which should be kept out of version control.

"""
import os
from application.secret_keys import CSRF_SECRET_KEY, SESSION_KEY

TOKEN = os.environ['PUBSUB_VERIFICATION_TOKEN']
TOPIC = os.environ['PUBSUB_TOPIC']
TOPIC_PROJECT = os.environ['TOPIC_APP_NAME']
PROJECT = os.environ['GOOGLE_CLOUD_PROJECT']

ITEM_APP_BASE_URL = os.environ.get('ITEM_APP_URL','http://localhost:8000')
BASE_APP_BASE_URL = os.environ.get('BASE_APP_URL','http://localhost:5000')
INV_APP_BASE_URL = os.environ.get("INV_APP_URL",'http://localhost:8080')
ITEM_VIEW_APP_BASE_URL = os.environ.get("ITEM_VIEW_APP_URL",'http://localhost:5050')
FUNCTIONS_BASE_URL = os.environ.get('FUNCTIONS_URL','http://localhost:8000')
REPORTING_BASE_URL = os.environ.get('REPORTING_APP_URL','https://reporting.apps.colororchids.com/dashboard/list')


class Config(object):
    # Set secret keys for CSRF protection
    SECRET_KEY = CSRF_SECRET_KEY
    CSRF_SESSION_KEY = SESSION_KEY
    # Flask-Cache settings
    CACHE_TYPE = 'gaememcached'

    # PUBSUB Config stuff
    PUBSUB_VERIFICATION_TOKEN = TOKEN
    PUBSUB_TOPIC = TOPIC
    PUBSUB_TOPIC_PROJECT = TOPIC_PROJECT
    GCLOUD_PROJECT = PROJECT

    # APP Bases
    ITEM_APP = ITEM_APP_BASE_URL
    BASE_APP = BASE_APP_BASE_URL
    INV_APP = INV_APP_BASE_URL
    ITEMVIEW_APP = ITEM_VIEW_APP_BASE_URL

    # Admin Pages:
    ITEM_FIELD_UPDATES = ITEM_APP +"/admin_containers"
    ITEM_ADMIN_HOME = ITEM_APP+"/admin_home"
    ITEM_OPTION_FIELDS = ITEM_APP +"/admin/options/?first=true"
    ITEM_ORDER_EMAIL = ITEM_APP +"/admin/invoice_email/?first=true"
    ITEM_MASTER_ITEMS = ITEM_APP+"/master_items"
    ITEM_ITEM_PUSH = ITEM_APP+"/admin_item_push/"
    ITEM_UPLOAD = ITEM_APP+"/upload_xslx"
    ITEM_RECIPE_UPDATE = ITEM_APP+"/admin_recipe"
    ITEM_SUPPLIER_UPDATE = ITEM_APP+"/admin/Supplier/?first=true"  #Remember to update the Data Storage Type for this one
    PLANT_ADMIN_PRODVIEW = INV_APP+"/admin_productin_view/Plants"
    VASES_ADMIN_PRODVIEW = INV_APP+"/admin_productin_view/Vase"

    # App Pages:
    ITEM_CUST_TRK = ITEM_APP+"/page/customer/?first=true"
    ITEM_CUST_ORDERS = ITEM_APP+"/customer_orders/"
    ITEM_CUST_ITEMS = ITEM_APP+"/customer_items/"
    ITEM_APP_HOME = ITEM_APP+"/home"
    ITEM_RESERVES = ITEM_APP+"/item_reserves"

    PLANT_PROD_VIEW = INV_APP+"/production_view/Plants/{}"
    PLANT_APP_HOME = INV_APP+"/inventory/Plants"
    INV_NEW_HOME = INV_APP+'/inventory_select'
    INV_SUMMARY = INV_APP+'/reports/inventory/summary'
    INV_REPORTING = REPORTING_BASE_URL

    VASE_PROD_VIEW = INV_APP+"/production_view/Vase/{}"
    VASE_APP_HOME = INV_APP+"/itemmonth/summary/Vase"

    BASE_APP_HOME = BASE_APP+"/"

    ITEMVIEW_APP_HOME = ITEMVIEW_APP


class Development(Config):
    DEBUG = True
    # Flask-DebugToolbar settings
    DEBUG_TB_PROFILER_ENABLED = True
    DEBUG_TB_INTERCEPT_REDIRECTS = False
    CSRF_ENABLED = True

class WIP(Config):
    DEBUG = True
    # Flask-DebugToolbar settings
    DEBUG_TB_PROFILER_ENABLED = True
    DEBUG_TB_INTERCEPT_REDIRECTS = False
    CSRF_ENABLED = True

class Testing(Config):
    TESTING = True
    DEBUG = True
    CSRF_ENABLED = True


class Production(Config):
    DEBUG = False
    CSRF_ENABLED = True
