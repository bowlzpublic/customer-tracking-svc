"""
forms.py

Web forms based on Flask-WTForms

See: http://flask.pocoo.org/docs/patterns/wtforms/
     http://wtforms.simplecodes.com/

"""

#from flaskext import wtf
#from wtforms.form import Form
#from wtforms.fields import TextField,TextAreaField
from wtforms import fields
from flask_wtf import FlaskForm
from wtforms.validators import DataRequired
from wtforms.ext.appengine.ndb import model_form as mf1

class RestUserAdd(FlaskForm):
    username = fields.TextField("username",validators=[DataRequired()])
    password = fields.PasswordField("password",validators=[DataRequired()])


#WeekForm = mf1(GrowWeek, FlaskForm, 
##            field_args={
#                'week_number' : dict(validators=[DataRequired()]),
#                'year' : dict(validators=[DataRequired()])  })

##PlantGrowSupplyForm = mf1(PlantGrowSupply, FlaskForm)

