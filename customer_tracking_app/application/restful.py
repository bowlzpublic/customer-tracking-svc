'''
Created on Dec 17, 2016

@author: jason

THis is just my way of making the REST stuff work
'''
from datetime import datetime
from datetime import timedelta

import sys, traceback, logging, json

from flask import make_response, jsonify

import jmespath

from application import app_utils, model_definitions
from application import model_definitions
from application.decorators import login_required, admin_required
from application.errors import InvalidUsage
from application.models import LoggingMessages, DataStorageType, StorageBlob, DataNumber, \
    DataNumberLookup, Image, ReportingConfig, QuickStorage


form_options = {'boolean':{'type':'fixed',
                           'values':['true','false'],
                           'field':'boolean',
                           'key':'id',
                           'name':'boolean',
                           'filters':[]},
                'field_types':{'type':'fixed',
                                'values':['string','int','float','currency','date','datetime','list','group'],
                                'field':'string',
                                'key':'id',
                                'name':'field_types',
                                'filters':[]}}

updates = {'Customer':{'options':[],
                      'update_name':'CustomerTracked',
                      'fields':{'customer_name':'i'},
                      'order':['customer_name'],
                      'style':'in_line'}
           }


@login_required
def get_option_data(request_args):

    if 'option_field' in request_args:
        r = StorageBlob.get_option_data(request_args)
        return jsonify(r)

    return jsonify({'json_path':'','label':'','value':''})

@login_required
def get_update_info(update_name):
    if update_name in updates.keys():
        return jsonify({'status':'success','message':'Pulled update info for: '+update_name,'payload':updates[update_name]})
    else:
        return jsonify({'status':'failed','message':'Update Name Does Not Exist','payload':{}})

@login_required
def add_logging_message(lMessage, lType):
    lm = LoggingMessages.create_log_message(lMessage,lType)
    return jsonify(lm.update_resp())

@admin_required
def update_storage_field(inDict):
    retDict = {"data":[]}
    if inDict:
        if inDict['action'] =='remove':
            ## DELETE
            storage_name = inDict.get('model_name',None)
            data = inDict.get('data',None)
            if data and storage_name:
                DataStorageType.removeStorageFields(storage_name,data)
            return jsonify(retDict)
        else:
            data = inDict.get('data',None)
            storage_name = inDict.get('model_name',None)
            if data and storage_name:  # data should be an array
                dst = DataStorageType.get_dataStorageType(storage_name)
                for fldData in data:
                    dst.add_storage_fields(fldData)
                    fldData['DT_RowId'] = fldData['field_name']
                    fldData.pop('id',None)
                    retDict['data'].append(fldData)
            return jsonify(retDict)


@login_required
def process_blob_dt(processed):
    '''
      -- GET:  { 'action':'get',
                 'data': {
                         'parent_id': customer-100,   # this is the id <DataNumberLookup> of the parent of this sb
                         'id': <id of the data item>  # Or the DataNumberLookup
                         'model_name': <name of the data type>,
                         'page_type': 'TypeListing' or 'TypeDisplay'  # TypeListing is a get all, TypeDisplay is a get as from a parent
                 }}

                 EXAMPLE RESPONSE:  {"data":[
                                             {
                                             "DT_RowId":5838406743490560,
                                             "alternate_names":"Bigger",
                                             "location_name":"The Big Loc",
                                             "location_number":"location-113",
                                             "primary_indicator":"True"
                                             },
                                             {
                                             "DT_RowId":6401356696911872,
                                             "alternate_names":"Smaller",
                                             "location_name":"Small Loc",
                                             "location_number":"location-114",
                                             "primary_indicator":"False"
                                             }]
                                     }


      -- CREATE: { 'action': 'create',
                   'data': [{
                         'parent_id': customer-100,   # this is the id <DataNumberLookup> of the parent of this sb
                         'page_type': 'TypeDisplay',
                         'data': {data elements to save},
                         'id':  0  # Set to 0 for a new element
                         'model_name': <name of the data type>,
                         'option_data': NoneType  # Seems like this is always set to None
                   }] # Array because we can save more than one at a time  }

                   EXAMPLE RESPONSE:  {"data":
                                            [{"DT_RowId":5838406743490560,
                                              "alternate_names":"Bigger",
                                              "location_name":"The Big Locs",
                                              "location_number":"location-113",
                                              "primary_indicator":"True"}]}


      -- EDIT: {  'action': 'edit',
                  'data': [{
                           'parent_id': customer-100,   # this is the id <DataNumberLookup> of the parent of this sb
                           'page_type': 'TypeDisplay',
                           'data': {data elements to save},
                           'id': <id of the data item> the id of the item that we are editing
                           'model_name': <name of the data type>
                           'option_data': NoneType  # think this is always null or "NoneType"
                  }] # an Array so that we can update more than 1 at a time}

                  EXAMPLE RESPONSE: {"data":
                                          [{"DT_RowId":6119881720201216,
                                          "alternate_names":"bbbb",
                                          "location_name":"sss",
                                          "location_number":"location-115",
                                          "primary_indicator":"False"}]}


      -- DELETE: { 'action': 'remove',
                   'data': [{
                            'parent_id': customer-100,   # this is the id <DataNumberLookup> of the parent of this sb
                            'page_type': 'TypeDisplay',
                            'data' {data elements to remove},
                            'id': <id of the data item to remove> the id of the item that we are deleting
                            'model_name': <name of the data type>
                            'option_data" NoneType # think it is always this way'
                   }] # an Array so that we can delete more than 1 at a time}

                   EXAMPLE RESPONSE:  {"data":
                                            [{"DT_RowId":6119881720201216,
                                            "alternate_names":"bbbb",
                                            "location_name":"sss",
                                            "location_number":"location-115",
                                            "primary_indicator":"False"}]}

    '''

    response = None

    if processed['action'] == 'create':
        response = process_create(processed['data'])
        action = 'create'
        for el in processed['data']:
            setup_update_task(action,el['model_name'],el['id'],el['parent_id'],response['data'])
    elif processed['action'] == 'get':
        response = process_get(processed['data'])
    elif processed['action'] == 'edit':
        response = process_edit(processed['data'])
        action = 'edit'
        for el in processed['data']:
            setup_update_task(action,el['model_name'],el['id'],el['parent_id'],el['data'])
    elif processed['action'] == 'remove':
        response = process_delete(processed['data'])
        action = 'remove'
        for el in processed['data']:
            setup_update_task(action,el['model_name'],el['id'],el['parent_id'],el['data'])
    else:
        response = {'status':'not written yet'}

    return jsonify(response)

def publish_init_start():
    '''
    Send the message to the task queue to read all StorageBlobs and publish them
    '''
    __create_task('sb-bigquery','/publish_all',{"action":"init"})

def load_api_plant_items():
    __create_task('plant-items', '/load_plant_items')

def publish_all_interval():
    '''
    Here we are publishing an interval of everything... like a snapshot (make it real easy to replicate status)
    '''
    nowDt = 'INTERVAL_'+datetime.now().strftime('%Y%m%d')
    __create_task('sb-bigquery', '/publish_all', {"action":"interval_"+nowDt})

def set_pi_update_task(dnl):
    '''

    :param dnl: Data Number Lookup
    :return: nada
    '''
    __create_task('plant-items','/update_plant_item_cache',{'dnl':dnl})

def do_publish_all(action="init"):
    '''
    This should be executed as a background task.. that then sends another task to the front!!
    '''
    data_types = DataStorageType.get_all_field_containers()
    rptNames = ReportingConfig.getAllReporting()
    for data_type in data_types:
        if data_type in rptNames:
            sbq = StorageBlob.getSB_of_Type(data_type)
            
            for sb in sbq:
                parId = sb.parent_id
                setup_update_task(action,sb.data_type,sb.id,parId)

    imq = Image.getAllImages()
    for img in imq:
        setup_image_task(img.image_number,action)

def setup_image_task(imgNum,action='create'):
    __create_task('image-bigquery', '/publish_image_data', {'image_number':imgNum,'action':action})

def setup_update_task(action, model_name, sb_id, parent_id, data=None):
    '''
    Send the data... should be attaching data for the following (create, remove, edit and delete)
    The rest will be custom
    '''
    if get_dataTypeReportingStatus(model_name):
        params = {'action':action, 'model_name': model_name, 'id':sb_id, 'parent_id':parent_id, 'data':json.dumps(data)}
        __create_task('sb-bigquery', '/publish_data_update', params)

def process_delete(data,model_name=None):
    resp = {'data':[]}
    for processed in data:
        storeBlob = StorageBlob.get_by_dnl(processed['id'])
        r = storeBlob.delete_resp()
        if r['status'] == 'failed':
            print("there was a problem trying to delete")
        else:
            update_qs(processed['id'],None,'delete')
        resp['data'].append(processed['data'])

    return resp

@login_required
def get_master_data():
    #sb = StorageBlob.query(StorageBlob.data_type == "admin_items").get()
    md = StorageBlob.getSB_of_Type('master_items')
    ret_master = []
    for m in md:
        data = m.get_dict()
        data['id'] = m.id
        ret_master.append(data)
    return jsonify({"master_items":ret_master})

def process_edit(data,model_name=None):
    resp = {'data':[]}
    for processed in data:
        sb = process_edit_sub(processed['id'],processed['data'])

        resp['data'].append(prep_sb_blob_data(sb,'update'))

    ## TURNING OFF FOR NOW 3/6/2018 process_options_data(opt_to_process)

    return resp

def process_edit_sub(dnl, newData):
    sb = StorageBlob.get_by_dnl(dnl)
    sb.update_data(newData)
    sb.validate_data()
    post_sb_save(sb.data_type, sb).update_ndb()
    return sb

def process_create(data,model_name=None):
    resp = {'data':[]}
    for processed in data:
        sb = StorageBlob.create_blob_parent(processed['model_name'], processed['data'], processed['parent_id'],processed['collection'])
        post_sb_save(sb.data_type, sb).update_ndb()

        upd = prep_sb_blob_data(sb,'create')
        resp['data'].append(upd)

    return resp

def post_sb_save(dataStorageType,storageBlob):
    if dataStorageType == 'cust_plant_item' or dataStorageType == 'plant_item' or dataStorageType == 'item_order':
        if dataStorageType == 'cust_plant_item' or dataStorageType == 'plant_item':
            set_pi_update_task(storageBlob.data_number_lookup)
        return post_sb_recipe_calc(dataStorageType,storageBlob)

    return storageBlob


def post_sb_recipe_calc(dataStorageType,storageBlob):
    data = storageBlob.get_dict()
    calc = caculate_recipe_cost(data, dataStorageType)
    listPrice = int_or_float(data['List_Price'])

    profitMargin = "{:.2f}".format(listPrice - float(calc['total_cost']))

    data['profit_margin'] = profitMargin
    data['recipe_cost'] = "{:.2f}".format(calc['total_cost'])
    storageBlob.update_data(data)
    return storageBlob



def int_or_float(value):
    try:
        return float(value)
    except:
        return 0.0


def process_get(processed,model_name=None):
    resp = {'data':[]}
    dnl = processed.get('id',None)
    parentDnl = processed.get('parent_id',None)

    if dnl and dnl != "":
        sb = StorageBlob.get_by_dnl(dnl)
        resp['data'].append(prep_sb_blob_data(sb))
    else:
        if parentDnl is None or parentDnl.strip() == '':
            qKey = "StorageBlob_"+processed['model_name']
            qResp = QuickStorage.getValue(qKey)
            if qResp is not None:
                return qResp

        sbs = StorageBlob.get_SB_from_Collection(parentDnl,processed['model_name'])

        for sb in sbs:
            resp['data'].append(prep_sb_blob_data(sb))

        if parentDnl is None or parentDnl.strip() == '':
            QuickStorage.setValue(qKey,resp,neverExpire=True)

    return resp


def prep_sb_blob_data(sb_blob, action=None):
    if sb_blob:
        dt = sb_blob.get_dict()
        dt['DT_RowId'] = dt['id']
        parId = sb_blob.parent_id
        if parId == 'StorageBlob':
            dt['parent_id'] = ""
        else:
            dt['parent_id'] = parId

        dt['parent_path'] = sb_blob.parent_path
        dt['path'] = sb_blob.path
        if action:
            update_qs(dt['data_number_lookup'],dt,action)
        return dt
    return {}

def update_qs(dnl, data, action):
    '''
    Question of the day... should this be done using events?
    '''
    if action == 'update' or action == 'create' or action == 'delete':
        dataType = ""
        if data is None:
            dataType = dnl.split("-")[0]
        else:
            dataType = data['data_type']

        qKey = "StorageBlob_"+dataType
        qResp = QuickStorage.getValue(qKey)
        if qResp:
            item = jmespath.search("data[?data_number_lookup == '"+dnl+"'] | [0]", qResp)
            if item:
                if action == 'update' or action == 'create':  # should not be create.. but covering bases
                    item.update(data)
                else:
                    qResp['data'].remove(item)
                    data = {'data_number_lookup':dnl}
            else:
                if action == 'update' or action == 'create':
                    if isinstance(qResp,list):
                        qResp.append(data)
                    if qResp.get('data',None) is not None:
                        if isinstance(qResp['data'],list):
                            qResp['data'].append(data)
                        else:
                            logging.error("Nothing to update... not a data element")
                    else:
                        logging.error("Nothing to update, no data and the resp is not an array??")
            QuickStorage.setValue(qKey,qResp,neverExpire=True)

        if dataType in ['options','recipe_costing']:
            StorageBlob.update_option_data(dataType,data,action=='delete')

@login_required
def process_blob_update_dt(jsonBlob):
    ks = jsonBlob.keys()
    blobId = None
    update = {}
    for k in ks:
        if k != 'action':
            d = parse_key(k)
            update[d['field']] = jsonBlob[k]
            if not blobId:
                blobId = d['id']


    upd = StorageBlob.get_by_dnl(blobId)
    dt_return = {}
    data = upd.get_dict()
    data['DT_RowId'] = upd['id']
    dt_return['data'] = [data]
    return jsonify(dt_return)

def parse_key(d):
    logging.debug(d)
    return d

def get_schema_info(schema_name):
    schema = DataStorageType.get_dataStorageType(schema_name)
    if schema:
        return schema.get_schema(True)
    else:
        return None

@login_required
def process_option_field(args):
    pArgs = app_utils.process_gen_args(args)
    r = StorageBlob.get_option_data(pArgs)
    return jsonify(r)

@login_required
def process_blob_update(jsonBlob, blob_id):

    return jsonify({'status':'success','message':'model updated'})

def get_image_data(img_num,action='create'):
    img = Image.getImageInfo(img_num)
    return fill_image_data(img,action)

def fill_image_data(img,action='create'):
    imgData = {'image_number':img.image_number,
               'image_url':img.image_url,
               'name':img.name,
               'description':img.description,
               'datastore_id': img.id,
               'datastore_parent_id': None,
               'business_parent_key': None,
               'parent_data_name': None,
               'added_by': img.added_by if img.added_by else 'System',
               'updated_by': img.updated_by if img.updated_by else 'System',
               'business_key': img.image_number,
               'added_dt': img.timestamp,
               'updated_dt': img.up_timestamp,
               'process_dt': datetime.now().isoformat(),
               'data_transaction_type': action,
               'data_status': 'Active'}

    imgSchema = {'fields':[{'field_name':'image_number','field_type':'string','field_required':False},
                           {'field_name':'image_url','field_type':'string','field_required':False},
                           {'field_name':'name','field_type':'string','field_required':False},
                           {'field_name':'description','field_type':'string','field_required':False}]}

    return {'data':imgData,'schema':imgSchema}

def get_dataTypeReportingStatus(dataType):
    ''' For the data type, 'customer','location','address'... etc..
        is reporting turned on for this... thus should we publish an update
    '''

    rptStatus = ReportingConfig.isReportingOn(dataType)
    return rptStatus

def updateReportingStatus(storeName,status):
    ''' get the reporting config and set the status '''
    repConfig = ReportingConfig.get_reporting(storeName)
    if status:
        repConfig.turn_on()
    else:
        repConfig.turn_off()
    return "on" if status else "off"

def get_storage_blob(sbId):
    sb = StorageBlob.get_by_dnl(sbId)
        
    if sb:
        parentInfo = {'parentId':sb.get_parent_id()}
        if parentInfo['parentId']:
            sbPar = sb.get_parent()
            if sbPar:
                parentInfo['parentModelName'] = sbPar.data_type
                parentInfo['parentDataLookup'] = sbPar.data_number_lookup
            else:
                parentInfo['parentModelName'] = "unknown"
                parentInfo['parentDataLookup'] = "unknown"
        else:
            parentInfo['parentModelName'] = None
            parentInfo['parentDataLookup'] = None
            parentInfo['parentId'] = None


        return {'status':'success',"storageBlob": sb, 'parentInfo':parentInfo}
    else:
        return {'status':'failure','storageBlob':None, 'parentInfo': None}

def get_schema(model_name):
    schema = DataStorageType.get_dataStorageType(model_name)

    if schema:
        sDict = schema.get_schema(True)
        # Add additional fields

        sDict['fields'].append({'field_name':"business_parent_key", 'field_type':'string'})
        sDict['fields'].append({'field_name':"business_key", 'field_type':'string'})
        sDict['fields'].append({'field_name':"parent_data_name", 'field_type':'string'})
        sDict['fields'].append({'field_name':"added_by", 'field_type':'string', 'field_required':True})
        sDict['fields'].append({'field_name':"updated_by", 'field_type':'string', 'field_required':True})
        sDict['fields'].append({'field_name':"added_dt", 'field_type':'datetime'})
        sDict['fields'].append({'field_name':"updated_dt", 'field_type':'datetime'})
        sDict['fields'].append({'field_name':"data_transaction_type", 'field_type':'string'})
        sDict['fields'].append({'field_name':"data_status", 'field_type':'datetime'})
        sDict['fields'].append({'field_name':"process_dt",'field_type':'datetime', 'field_required':True})

        return {'status':'success','schema':sDict}

    return {'status':'failure','schema':None}

def process_field_updates(jsonIn):
    if jsonIn.get('action',"FAILED") == 'save':
        # do something
        try:
            DataStorageType.save_or_update(jsonIn)
            return jsonify({'status':'success'})
        except:
            traceback.print_exc(file=sys.stdout)
            logging.error("Unexpected error: {}".format(sys.exc_info()[0]) )
            return jsonify({'status':'failed','message':sys.exc_info()[0]})
    else:
        entry = jsonIn['data'][0]['data']
        entry['DT_RowId'] = jsonIn['data'][0]['id']
        resp = {'data':[entry]}
        return jsonify(resp)

def get_recipe_item(recipe_item):
    status = "success"
    data = None
    sb = StorageBlob.get_by_dnl(recipe_item)
    if sb:
        data = sb.get_dict()
    else:
        status = "failed"
    return jsonify({"status": status, "name":recipe_item,"data":data})

def caculate_recipe_cost(data, dataStorageType):
    dst = DataStorageType.get_dataStorageType(dataStorageType)
    recipe_fields = dst.get_fields_by_category('Recipe')
    return __recipe_cost_calc(recipe_fields,data)

def __recipe_cost_calc(recipe_fields, data):
    recipe_vals= []
    pack_size = data['Pack_Size']
    if not pack_size.isdigit():
        pack_size = 1
    else:
        pack_size = int(pack_size)
        if pack_size <= 0:
            pack_size = 1

    rFlds = recipe_fields.keys()
    [recipe_vals.append({'field':rFld,'value':data.get(rFld,'')}) for rFld in rFlds]
    recipe_resp = {'total_cost':0.0, 'values':{}}
    for val in recipe_vals:
        recipe_cost = None
        try:
            if val['field'] == 'Ethyl_Block' or val['field'] == 'Heat_Pack':
                recipe_cost = {'field':val['field'],'price':__get_lookup_price(val['field'].replace('_',' '),val['value'])}
            elif val['field'] == 'Plants':
                for plant in val['value']:
                    pl = plant['plant']
                    cost = __get_recipe_cost(pl)
                    cost['price'] = int(plant['qty']) * cost['price']
                    if recipe_cost is None:
                        recipe_cost = cost
                    else:
                        recipe_cost['price'] = recipe_cost['price'] + cost['price']
            else:
                recipe_cost = __get_recipe_cost(val['value'])

            if recipe_cost:
                recipe_resp['values'][recipe_cost['field']] = recipe_cost
        except Exception as e:
            logging.error("Problem processing field:  {}, (Error: {})".format(val['field'],str(e)))
            raise e

    tc = 0.0

    prices = recipe_resp['values'].keys()
    for val in prices:
        x = recipe_resp['values'][val]
        p = float(x['price'])
        if x['field'] == 'Ethyl_Block' or x['field'] == 'Heat_Pack':
            p = p/pack_size
        tc = tc + p

    recipe_resp['total_cost'] = tc
    return recipe_resp

def __get_lookup_price(recipe_name,num):
    nbr = 0.0
    if num.isdigit():
        nbr = int(num)
    else:
        return 0.0

    defVal = 0.0
    matches = StorageBlob.query_with_jmespath('recipe_costing',"[?name == '"+recipe_name+"']",'price_each',False)
    if len(matches) > 0:
        match = matches[0]['value']
        if match.strip() != "":
            defVal = float(match)

    return defVal * nbr
def __recipe_split(inValue):
    if isinstance(inValue,list):
        return ",".join(inValue)
    return inValue

def __get_recipe_cost(recipe_val):
    if isinstance(recipe_val,list):
        resp = []
        for recipe_val_entry in recipe_val:
            entry_resp = __get_recipe_cost__entry(recipe_val_entry)
            if entry_resp is not None:
                resp.append(entry_resp)
        base_resp = {'field':None,'price':0}
        for respEntry in resp:
            base_resp['field'] = respEntry['field']
            base_resp['price'] = base_resp['price'] + respEntry['price']
        return base_resp
    else:
        return __get_recipe_cost__entry(recipe_val)

def __get_recipe_cost__entry(recipe_val):
    if recipe_val:
        #vals = __recipe_split(recipe_val).split(",")
        resp = {'field':None,'price':0}
        #for val in vals:
        if recipe_val.strip() != "":
            recipeId, _ = recipe_val.split('|')
            sb = StorageBlob.get_by_dnl(recipeId)
            if sb:
                name = sb.data['item_type']
                price = sb.data['price_each']
                if price and price.strip() != "":
                    price = float(price)
                else:
                    price = 0.0
                resp['field'] = name
                resp['price'] = resp['price'] + price
            else:
                raise Exception("Unable to find information for: {}, update value and retry".format(recipe_val))
        if resp['field']:
            return resp
    return None

def __create_task(task_queue, task_url, task_params={}):
    logging.info("No tasks being created at the moment... next release")
    #task = taskqueue.add(queue_name=task_queue, url=task_url,params=task_params)
    #logging.info('Task Added:  NAME = {}, ETA = {}'.format(task.name, str(task.eta)))
