'''
Created on Dec 4, 2017

@author: jason
'''
from flask import render_template, jsonify
import logging
from application import app
from application.errors import InvalidUsage
from application.views.admin.admin_main import AdminHome, AdminContainerSelect, AdminContainerUpdate, \
    Admin_Display, Admin_ItemPage, AdminBulkLoad, AdminInvoice,AdminItemUpdatePush,Admin_RecipePage,AdminUser, \
    AdminUploadSpreadsheet
from application.views.admin.admin_secret import AdminUpdate
from application.views.login.login_main import LoginHome, Page_Display, \
    Item_Display, Item_Master_Display, Order_Display, Order_Form, Get_Custom_HTML, Show_Item, Item_Reserve
from application.views.public.public_warmup import PublicWarmup


# URL dispatch rules
# App Engine warm up handler
# See http://code.google.com/appengine/docs/python/config/appconfig.html#Warming_Requests
app.add_url_rule('/_ah/warmup', 'public_warmup', view_func=PublicWarmup.as_view('public_warmup'))
app.add_url_rule('/', 'public_index', view_func=PublicWarmup.as_view('public_index'))


# Basic Admin stuff
app.add_url_rule('/admin_update', 'admin_update',view_func=AdminUpdate.as_view('admin_update'), methods=['GET','POST'])
app.add_url_rule('/admin_home','admin_home', view_func=AdminHome.as_view('admin_home'), methods=['GET'])
app.add_url_rule('/admin_containers', 'admin_fields', view_func=AdminContainerSelect.as_view("admin_fields"), methods=['GET'])
app.add_url_rule('/admin_container/<path:container_name>/update', 'admin_container', view_func=AdminContainerUpdate.as_view('admin_container'), methods=['GET'])
app.add_url_rule('/admin_invoice_test','admin_inv',view_func=AdminInvoice.as_view("admin_inv"),methods=['GET'])

# Base Home Page
app.add_url_rule('/home','login_home',view_func=LoginHome.as_view('login_home'), methods=['GET'])

#One Page Application
'''
    The path will tell you the storage name
    (any part that starts with "id_" is the lookup value for the previous path part)
    When updating data, all that is needed is the following
    /root_storage_name/id_of_root/parent_of_data/id_of_parent/storage_name/id_of_storage

    This will be used to build this dictionary
    {'root':{'name':'foo','id':15461618},
     'parent':{'name':'bar','id':84965},
     'base':{'name': 'address','id': 7894997 }}

    If the request is for root then parent will be None and so will be base

    If the request is data that the parent is the same as root,
    then the value of base will be None

    this will return the data

    UPDATE: Feb 18, 2018.. .this was implemented differently... more to come
    '''
app.add_url_rule('/page/<path:path>/','page_display',view_func=Page_Display.as_view('page_display'),methods=['GET'])
app.add_url_rule('/customer_items/','cust_items',view_func=Item_Display.as_view('cust_items'),methods=['GET'])
app.add_url_rule('/master_items/','mstr_items',view_func=Item_Master_Display.as_view('mstr_items'),methods=['GET'])
app.add_url_rule('/item_reserves/','reserves',view_func=Item_Reserve.as_view('reserves'),methods=['GET'])
app.add_url_rule('/customer_orders/','cust_orders',view_func=Order_Display.as_view('cust_orders'),methods=['GET'])
app.add_url_rule('/get_order/','get_order',view_func=Order_Form.as_view('get_order'),methods=['GET'])
app.add_url_rule('/get_page/<path:path>/','get_page',view_func=Get_Custom_HTML.as_view('get_page'),methods=['GET'])

app.add_url_rule('/show_item/<itemNum>','show_item',view_func=Show_Item.as_view('show_item'),methods=['GET'])
'''
Same as page_display, except only admins can go here
'''
app.add_url_rule('/admin/<path:path>/', 'admin_display',view_func=Admin_Display.as_view('admin_display'),methods=['GET'])


app.add_url_rule('/admin_item/<path:path>/','admin_item',view_func=Admin_ItemPage.as_view('admin_item'),methods=['GET'])
app.add_url_rule('/admin_bulk/','admin_bulk',view_func=AdminBulkLoad.as_view('admin_bulk'),methods=['GET'])
app.add_url_rule('/admin_item_push/','admin_item_push',view_func=AdminItemUpdatePush.as_view('admin_item_push'),methods=['GET'])
app.add_url_rule('/admin_recipe','admin_recipe', view_func=Admin_RecipePage.as_view('admin_recipe'),methods=['GET'])
app.add_url_rule('/admin_user','admin_user', view_func=AdminUser.as_view('admin_user'),methods=['GET'])

app.add_url_rule('/upload_xslx','upload_xslx',view_func=AdminUploadSpreadsheet.as_view('upload_xslx'),methods=['GET','POST'])
# Error handlers

#Handle thrown Exceptions
@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    logging.error("Processing an Error: "+str(error.to_dict()))
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response

# Handle 404 errors
@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

# Handle 500 errors
@app.errorhandler(500)
def server_error(e):
    return render_template('500.html'), 500
