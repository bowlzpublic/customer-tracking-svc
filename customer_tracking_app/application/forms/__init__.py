from wtforms import fields
from flask_wtf import FlaskForm
from wtforms.validators import DataRequired

class RestUserAdd(FlaskForm):
    username = fields.TextField("username",validators=[DataRequired()])
    password = fields.PasswordField("password",validators=[DataRequired()])