'''
House the code that will do all of the publishing to the topic

'''
import os, base64,json
#from googleapiclient.discovery import build
from google.cloud import pubsub

class PublisherClient:
    class __PublisherClient:
        def __init__(self):
            self.client = pubsub.PublisherClient()
        
    instance = None
    def __init__(self):
        if not PublisherClient.instance:
            PublisherClient.instance = PublisherClient.__PublisherClient()

    def __getattr__(self, name):
        return getattr(self.instance, name)

def publish_data(data, topic, project):
    clt = PublisherClient().client

    topic_path = 'projects/{project_id}/topics/{topic}'.format(
        project_id=project,
        topic=topic
    )   

    clt.publish(topic, str.encode(json.dumps(data)))
