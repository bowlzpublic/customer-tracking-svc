'''
Author: Jason Bowles
Pulling in the code to create the swagger API
'''
from flask_restplus import Api
from .storage_blob import create_models, StorageBlob
#from .blob import create_models

api = Api(version='1.0', title='Customer Item API', doc='/doc',
    description='API to get and update data in the customer item app')

sbModels = create_models(api)
