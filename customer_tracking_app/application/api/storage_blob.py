from flask_restplus import Resource, fields, Namespace

from application.models import DataStorageType, StorageBlob

def get_schemas():
    schemas = {}
    dsts = DataStorageType.query(DataStorageType.storage_name == 'contact')
    for dst in dsts:
        dstFields = dst.get_fields_dict(False)
        for field in dstFields.keys():
            dstFields[field] = dstFields[field].to_dict()
            del dstFields[field]['parent_container']
            del dstFields[field]['option_container']
        schemas[dst.storage_name] = {'storage_name':dst.storage_name,
                                  'is_list':dst.isList,
                                  'extends':dst.extends,
                                  'fields': dstFields}

    return schemas
def create_namespace(api,name):
    '''
    get the storageBlob namespace we'll call it "data"
    Example: ns = api.namespace('todos', description='TODO operations')
    '''
    return api.namespace(name,description='Operations on the '+name+' data model')


def create_models(api):
    '''
    todo = api.model('Todo', {
    'id': fields.Integer(readOnly=True, description='The task unique identifier'),
    'task': fields.String(required=True, description='The task details')})
    '''
    schemas = get_schemas()
    models = schemas.keys()
    rpMdls = {}
    for model in models:
        flds = {}
        dstFields = schemas[model]['fields']

        for fld in dstFields:
            field = dstFields[fld]
            desc = field['description'].strip()
            if desc == "":
                desc = "The '"+field['field_name']+"' field in the "+model+" data model"

            flds[field['field_name']] = fields.String(description=desc)

        flds['key'] = fields.String(description="the url safe key to access and update this "+model+" item")

        rpMdls[model] = {'namespace': create_namespace(api,model),'model':api.model(model,flds), 'name': model}

    return rpMdls

class StorageBlob(Resource):
    '''
    See if we can use to dynamically create a Resource
    '''
    def __init__(self, namespace, model):
        self.ns = namespace
        self.mocel = model

    def get(self, key):
        sb = StorageBlob.get_by_urlkey(key)
        return sb.data
