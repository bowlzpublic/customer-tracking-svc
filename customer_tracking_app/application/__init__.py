"""
Initialize Flask app

"""
from flask import Flask, jsonify, request, make_response, render_template
import os, sys, json, logging,datetime,requests
import traceback
from flask_debugtoolbar import DebugToolbarExtension
from werkzeug.debug import DebuggedApplication
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

from application.decorators import admin_required, login_required, api_required
from application import app_utils, model_definitions
from application.models import DataStorageType, Image, StorageBlob, ApiUser, EmailNotifications
from application.models.tasks import send_task
from application.models.pub_sub import publish_message, publish_item_refresh
from application.errors import InvalidUsage
from application.models.utils import UploadItemData
from application.views.common import generate_invoice, generate_simple_invoice, email_order

from . import settings
# Imports the Cloud Logging client library
import google.cloud.logging

app = Flask('application')
CORS(app)

# Instantiates a client
client = google.cloud.logging.Client()

# Retrieves a Cloud Logging handler based on the environment
# you're running in and integrates the handler with the
# Python logging module. By default this captures all logs
# at INFO level and higher
client.get_default_handler()
client.setup_logging()

appConfig = os.getenv('FLASK_CONF',"PROD")
logging.info("The application is configured for: "+appConfig)

if appConfig == 'TEST':
    app.config.from_object('application.settings.Testing')

elif appConfig == "DEV":
    # Development settings
    app.config.from_object('application.settings.Development')
    # Flask-DebugToolbar
    toolbar = DebugToolbarExtension(app)

    # Google app engine mini profiler
    # https://github.com/kamens/gae_mini_profiler
    app.wsgi_app = DebuggedApplication(app.wsgi_app, evalex=True)

elif appConfig == "WIP":
    # WIP settings
    app.config.from_object('application.settings.WIP')
    # Flask-DebugToolbar
    toolbar = DebugToolbarExtension(app)

    # Google app engine mini profiler
    # https://github.com/kamens/gae_mini_profiler
    app.wsgi_app = DebuggedApplication(app.wsgi_app, evalex=True)

else:
    app.config.from_object('application.settings.Production')

# Pull in URL dispatch routes
from . import urls
from . import restful
from . import pubsub_util

from application.models import SalesInventoryAPI

salesRest = SalesInventoryAPI(app.logger)

#from application.api import api # TODO will work on this later
#
#api.init_app(app)

def warm_up():
    logging.info("calling warmup...")
    try:
        model_definitions.setup_env(appConfig)
    except Exception as inst:
        logging.info(type(inst))    # the) exception instance
        logging.info(inst.args)      # arguments stored in .args
        logging.error(str(inst))          # __str__ allows args to be printed directly
        msg = traceback.format_exc()
        logging.error(msg)
    logging.info("Warm Up Finished...")


warm_up()
'''
This is just stuff for the api user endpoints... so that I can admin the usage of api users
'''
@app.route('/api/auth',methods=['GET'])
def get_api_token():
    d = request.args
    userName = d['userName']
    userPass = d['userPass']
    token = ApiUser.getToken(userName,userPass)
    response = {"status":"success","message":"new token generated for user: "+userName,"token":token}
    if not token:
        response['status'] = "failed"
        response["message"] = "invalid user name and/or password"
    return jsonify(response)

@app.route('/api/users',methods=['GET','POST'])
def api_users():
    if request.method == "GET":
        users = ApiUser.getUsers()
        for user in users:
            user['DT_RowId'] = user['userName']

        return jsonify({'data':users})
    else:
        data = json.loads(request.form['user_api'])
        userName = data['data'][0]['userName']
        userPass = data['data'][0]['userPass']
        if data['action'] == 'create':
            ApiUser.createUser(userName, userPass)
        if data['action'] == 'edit':
            ApiUser.updateUserPass(userName,userPass)
        if data['action'] == 'remove':
            ApiUser.deleteUser(userName)
            return jsonify({'data':[]})
        return jsonify({'data': [{'DT_RowId': userName, 'userName': userName, 'userPass': userPass}]})


    return jsonify({'data':[{'DT_RowId':'admin','userName':'admin','userPass':'admin'}]})

@admin_required
@app.route('/api/get_users',methods=['GET'])
def get_all_users():
    return jsonify(ApiUser.getUsers())

@admin_required
@app.route('/api/create_user',methods=['GET'])
def create_user():
    d = request.args
    userName = d['userName']
    userPass = d['userPass']
    ApiUser.createUser(userName,userPass)
    return jsonify({"userName":userName,"userPass":userPass})

@admin_required
@app.route('/api/update_user',methods=['GET'])
def update_user():
    d = request.args
    userName = d['userName']
    userPass = d['userPass']
    ApiUser.updateUserPass(userName,userPass)
    return jsonify({"userName":userName,"userPass":userPass})

@admin_required
@app.route('/api/delete_user',methods=['GET'])
def delete_user():
    d = request.args
    userName = d['userName']
    ApiUser.deleteUser(userName)
    return jsonify({"userName":userName,"userPass":None})

'''
API Rerouting... so I don't run into CORS problems
'''
@app.route('/api/sales_inventory/reserve/create',methods=['POST'])
def create_cust_reserve():
    try:
        reserveInfo = request.get_json()
        reserve_date = datetime.datetime.fromisoformat(reserveInfo.get('reserve_date',datetime.datetime.now().isoformat()))
        customer_id = reserveInfo['customer_id']
        location_id = reserveInfo['location_id']
        item_id = reserveInfo['item_id']
        num_reserved = reserveInfo['num_reserved']
        inventory_location = reserveInfo.get('inventory_location',None)
        return jsonify(salesRest.createReserveAPI(reserve_date,customer_id,location_id,item_id,num_reserved,invLoc=inventory_location))
    except Exception:
        msg = traceback.format_exc()
        logging.error(msg)
        return jsonify({'error': msg}),500

@app.route('/api/sales_inventory/reserve/update',methods=['POST'])
def update_cust_reserve():
    try:
        reserveInfo = request.get_json()
        reserve_date = reserveInfo.get('reserve_date',datetime.datetime.now().isoformat())
        if reserve_date.strip() == '':
            reserve_date = datetime.datetime.now()
        else:
            reserve_date = datetime.datetime.fromisoformat(reserve_date)
        reserve_id = reserveInfo['reserve_id']
        customer_id = reserveInfo['customer_id']
        location_id = reserveInfo['location_id']
        item_id = reserveInfo['item_id']
        num_reserved = reserveInfo['num_reserved']
        inventory_location = reserveInfo.get('inventory_location',None)
        return jsonify(salesRest.updateReserveAPI(reserve_id, customer_id, location_id,item_id,num_reserved,reserve_date,invLoc=inventory_location))
    except Exception:
        msg = traceback.format_exc()
        logging.error(msg)
        return jsonify({'error': msg}),500

@app.route('/api/sales_inventory/reserve/delete/<reserve_id>',methods=['GET'])
def delete_cust_reserve(reserve_id):
    try:
        return jsonify(salesRest.deleteReserveAPI(reserve_id))
    except Exception:
        msg = traceback.format_exc()
        logging.error(msg)
        return jsonify({'error': msg}),500

@app.route('/api/sales_inventory/reserve/getall/<reserve_date>',methods=['GET'])
def getall_cust_reserve(reserve_date):
    try:
        reserveDate = datetime.datetime.fromisoformat(reserve_date)
        return jsonify(salesRest.getAllReservesAPI(reserveDate))
    except Exception:
        msg = traceback.format_exc()
        logging.error(msg)
        return jsonify({'error': msg}),500

@app.route('/api/sales_inventory/reserve/getsummary/<reserve_date>',methods=['GET'])
def getsumm_cust_reserve(reserve_date):
    try:
        reserveDate = datetime.datetime.fromisoformat(reserve_date)
        return jsonify(salesRest.getSummReservesAPI(reserveDate))
    except Exception:
        msg = traceback.format_exc()
        logging.error(msg)
        return jsonify({'error': msg}),500

@app.route('/api/sales_inventory/reserve/get/<reserve_id>',methods=['GET'])
def get_cust_reserve(reserve_id):
    try:
        return jsonify(salesRest.getReserveAPI(reserve_id))
    except Exception:
        msg = traceback.format_exc()
        logging.error(msg)
        return jsonify({'error': msg}),500

'''
The following is stuff to get information straight from blob storage for the purpose of an api
'''

@api_required
@app.route('/api/data/sb/v1/plant_items',methods=['GET'])
def api_get_plant_item_list():
    #id_token = request.headers['Authorization'].split(' ').pop()
    #logging.info("Validating Token: "+id_token)
    #if not ApiUser.validateToken(id_token):
    #    return 'Unauthorized', 401

    resp = StorageBlob.get_all_plant_items()

    return jsonify(resp)

@app.route('/load_plant_items',methods=['POST','GET'])
def load_plant_items():
    #resp = StorageBlob.get_all_plant_items(use_cache=False)
    publish_item_refresh()
    resp = {'status':'success'}
    return jsonify(resp)

@app.route('/task_load_plant_items',methods=['GET'])
def task_load_plant_items():
    restful.load_api_plant_items()
    return jsonify({"status": "success"})

@api_required
@app.route('/api/data/sb/v1/get_item/<item_number>',methods=['GET'])
def api_get_item(item_number):
    #id_token = request.headers['Authorization'].split(' ').pop()
    #if not ApiUser.validateToken(id_token):
    #    return 'Unauthorized', 401

    sb = StorageBlob.get_plant_item(item_number)

    if not sb:
        return render_template('404.html'), 404

    return jsonify(sb)


@login_required
@app.route('/calculate_recipe_cost/<data_storage_type>',methods=['POST'])
def calc_recipe_from_json(data_storage_type):
    plant_item = request.form
    resp = restful.caculate_recipe_cost(plant_item,data_storage_type)
    return jsonify({'status':'success','cost':resp})

@login_required
@app.route('/calculate_recipe_cost_item/<item_nums>', methods=['GET'])
def calc_recipe_from_item(item_nums):
    idList = item_nums.split('-')
    sb = StorageBlob.create_key(idList).get()
    resp = restful.caculate_recipe_cost(sb.data,sb.data_type)
    return jsonify({'status':'success','cost':resp})

@app.route('/update_recipe_costs',methods=['POST'])
def update_recipe_costs():
    jsonData = request.get_json()
    try:
        resp = restful.caculate_recipe_cost(jsonData['data'],jsonData['data_type'])
        return jsonify({'status':'success','cost':resp})
    except Exception:
        msg = traceback.format_exc()
        logging.error(msg)
        return jsonify({'error': msg}),500


@admin_required
@app.route('/update_report_status',methods=['GET'])
def update_dst_reporting_status():
    name = request.args.get('storage_name')
    status = request.args.get('status')

    statusOn = True if status.lower() =='true' else False
    newStatus = restful.updateReportingStatus(name,statusOn)
    return jsonify({'status':'success','value':newStatus})

@admin_required
@app.route('/update_fields',methods=['GET','POST'])
def rest_update_fields():
    uJson = {}
    if request.method == "GET":
        d = request.args
        uJson = json.loads(d['update'])
    else:
        d = request.form
        uJson = json.loads(d['update'])
        uJson['all_data'] = json.loads(d['all_data'])

    return restful.process_field_updates(uJson)

@app.route('/update_schema_fields',methods=['GET','POST'])
def rest_update_schema_fields():
    uJson = {}
    if request.method == "GET":
        d = request.args
        uJson = json.loads(d['update'])
    else:
        d = request.form
        uJson = json.loads(d['update'])

    proc_d = {'action': uJson['action'],'model_name':uJson['parent_id'], 'data':[]}
    for dd in uJson['data']:
        dd['data']['id'] = None if dd.get('id',"1") == "1" else dd['id']
        proc_d['data'].append(dd['data'])

    return restful.update_storage_field(proc_d)

@admin_required
@app.route('/admin_object_update',methods=['GET','POST'])
def admin_sb_update():
    jsonData = request.get_json()
    jsonData['data'].pop('DT_RowId',None)
    jsonData['data'].pop('id',None)
    restful.process_edit_sub(jsonData['ancestry_list'],jsonData['data'])
    return jsonify({'status':'success'})

@app.route('/email_system_admin',methods=['POST'])
def email_system_admin():
    jsonData = request.get_json()
    subj = jsonData['subject']
    msg = jsonData['body']
    user = request.headers.get('X-Goog-Authenticated-User-Email','none:system@analyticssupply.com').split(":")[1]
    msg = msg + "\n\nUSER: \n"+user
    eType = 'admin_notification'
    EmailNotifications.send_email(eType,subj,msg)
    return jsonify({'status':'success'})

@app.route('/plant_item_update',methods=['POST'])
def update_plant_item():
    jsonData = request.get_json()
    action = jsonData['action']
    try:
        if (action == 'EDIT' or action == 'update'):
            action = 'update'
            sb = restful.process_edit_sub(jsonData['data']['data_number_lookup'],jsonData['data'])
            resp = restful.prep_sb_blob_data(sb,action)
            jsonData['data'] = resp
        
        if (action == 'create'):
            resp =  restful.process_create([jsonData])
            jsonData['data'] = resp['data'][0]

        jsonData['status'] = 'success'
        jsonData['msg'] = 'congrats... it worked'
        return jsonify(jsonData)
    except Exception:
        msg = traceback.format_exc()
        logging.error(msg)
        return jsonify({'error': msg}),500


@app.route('/storage_blob/dt_update',methods=['GET','POST'])
def rest_blob_dt_update():
    uJson = {}
    model_name = request.args.get('model_name','base')
    model_id = request.args.get('model_id',None)
    processed_args = app_utils.process_args(request.args)
    processed_args['collection'] = model_name
    if request.method == "GET":
        '''
        code to mimic the post for datatables
        '''
        uJson['action'] = 'get'
        uJson['data'] = processed_args
        uJson['data']['model_name'] = model_name

        if model_id:
            uJson['data']['id'] = model_id
    else:
        d = request.form
        uJson = json.loads(d['update'])
        for p in uJson['data']:
            for k in processed_args.keys():
                p[k] = processed_args[k]

    try:
        return restful.process_blob_dt(uJson)
    except InvalidUsage as iu:
        logging.error(iu)
        msg = traceback.format_exc()
        logging.error(msg)
        if iu.payload and iu.payload['errors']:
            msg = "There was a problem with one of the fields"
            if len(iu.payload['errors']) > 0:
                msg = iu.payload['errors'][0]
            return jsonify({"error":msg})
        return jsonify({'error': msg})
    except Exception:
        msg = traceback.format_exc()
        logging.error(msg)
        return jsonify({'error': msg}), 500

@app.route('/get_options_data/<option_type>',methods=['GET'])
def get_option_data(option_type):
    '''
    This is designed to make getting options easier
    3.23.20 added this into the chkImageLoad, which does nothing.. but now loads page options 
    '''
    if option_type == 'options' or option_type == 'recipe_costing':
        force = request.args.get('force','false').lower() == 'true'
        results = StorageBlob.get_all_option_data(option_type,force)
        return jsonify({'status':'success','message':'loaded','data':results})

    return jsonify({'status':'failed','message':'Option Type does not exist'}), 400

@app.route('/get_options')
def get_field_options():
    return restful.get_option_data(request.args)

@app.route('/get_box_dimensions/<box_id>',methods=['GET'])
def fetch_box_dimensions(box_id):
    logging.info("Getting box dimensions: "+str(box_id))
    return restful.get_recipe_item(box_id)

@app.route('/get_image/<image_name>',methods=['GET'])
def fetch_image(image_name):
    logging.info("Getting Image: " + str(image_name))
    image = restful.get_image_data(image_name,action="fetch")
    resp = {"status": "Not Found", "data": "None"}
    if image:
        resp['status'] = "success"
        resp['data'] = image
    return jsonify(resp)

@app.route('/upload_image',methods=['GET','POST'])
def uploading_image():
    if request.method == "GET":
        imgs = Image.getAllImages()
        return jsonify({'files':imgs})

    image_file = request.files['upload']
    imgDict = Image.createImage(image_file)
    restful.setup_image_task(imgDict['image_number']) # publish the image info

    respObj = {'image_id':imgDict['image_number'],'image_url':imgDict['image_url']}

    retDict = {'upload':{'id':respObj},'files':{'images':{imgDict['image_number']:respObj}}}
    # For the response object refer here: https://editor.datatables.net/manual/server#File-upload  (Server-to-client)
    return jsonify(retDict)

@app.route('/upload_spreadsheet',methods=['POST'])
def upload_spreadsheet():
    try:
        xslx_file = request.files['filepond']
        email = request.headers.get('X-Goog-Authenticated-User-Email','none:system@analyticssupply.com').split(":")[1]
        uid = UploadItemData.getInstance()
        uri = uid.upload_spreadsheet(xslx_file,email)
        return jsonify({'status':'success','blob_uri':uri})
    except Exception:
        msg = traceback.format_exc()
        logging.error(msg)
        return jsonify({'error': msg}), 500
        
@app.route('/get_master_data')
def app_master_data():
    return restful.get_master_data()

@login_required
@app.route('/order/<onum>')
def send_email(onum=None):
    email = request.headers.get('X-Goog-Authenticated-User-Email','none:system@analyticssupply.com').split(":")[1]
    #email = users.GetCurrentUser().email()
    email_order(int(onum),email, request.base_url)
    return jsonify({"status":"success"})

@login_required
@app.route('/get_schema/<schema_name>')
def get_schema_json(schema_name):
    logging.info("Getting Schema: "+str(schema_name))
    schema = restful.get_schema_info(schema_name)
    resp = {"status":"Not Found","schema":"None"}
    if schema:
        resp['status'] = "success"
        resp['schema'] = schema
    return jsonify(resp)
    
    
## ----------- START NEW PUBSUB -----------##
## all pubsub will call the same method and then use that to pass attributes
@login_required
@app.route('/pubsub/download',methods=['GET'])
def publish_download_message():
    email = request.headers.get('X-Goog-Authenticated-User-Email','none:system@analyticssupply.com').split(":")[1]
    location_id = request.args.get('location_id',None)
    customer_id = request.args.get('customer_id',None)

    payload = {'emailAddress':email,'download_type':'customer'}
    if customer_id is not None:
        payload["customer_id"] = customer_id
    
    if location_id is not None:
        payload["location_id"] = location_id
    
    try:
        publish_message('download','do download data',payload)
        return jsonify({'status':'success'})
    except Exception:
        msg = traceback.format_exc()
        logging.error(msg)
        return jsonify({'error': msg}), 500

@login_required
@app.route('/pubsub/download_mstr',methods=['GET'])
def publish_download_master_message():
    email = request.headers.get('X-Goog-Authenticated-User-Email','none:system@analyticssupply.com').split(":")[1]

    payload = {'emailAddress':email,'download_type':'master'}
    
    try:
        publish_message('download','do download data',payload)
        return jsonify({'status':'success'})
    except Exception:
        msg = traceback.format_exc()
        logging.error(msg)
        return jsonify({'error': msg}), 500
        
## ----------- START NEW TASKS ------------##
## create tasks end point starts with /tasks/create/...
## process tasks end point starts with /tasks/process/...


## ---- END TASKS ----


@login_required
@app.route('/start_publish_all',methods=['GET'])
def start_task_pub_all():
    restful.publish_init_start()
    return jsonify({'status':'success'})

@login_required
@app.route('/start_publish_interval',methods=['GET'])
def start_interval_pub_all():
    restful.publish_all_interval()
    return jsonify({'status':'success'})

@login_required
@app.route('/update_plant_item_cache',methods=['POST','GET'])
def update_plant_item_cache():
    #dnl = request.values.get('dnl')
    #
    #  Not implementing this logic at this moment... 
    #
    #sb = StorageBlob.get_by_dnl(dnl)
    #sb.update_plant_memcache()
    #StorageBlob.update_memcache_pi_all()
    return jsonify({'status':'success'})

@login_required
@app.route('/publish_all',methods=['POST','GET'])
def process_task_pub_all():
    action = request.values.get('action')
    restful.do_publish_all(action)
    return jsonify({'status':'success'})

@login_required
@app.route('/publish_image_data',methods=['POST','GET'])
def publish_image_data():
    imgNum = request.values.get('image_number')
    action = request.values.get('action')
    imgInfo = restful.get_image_data(imgNum,action)

    pubData = {'app_name': 'CustomerTracking'}
    pubData['status'] = 'Active'
    pubData['action'] = action
    pubData['model_name'] = 'image_store'
    pubData['payload'] = imgInfo['data']
    pubData['schema'] = imgInfo['schema']

    pubsub_util.publish_data(pubData, app.config['PUBSUB_TOPIC'], app.config['PUBSUB_TOPIC_PROJECT'])
    return jsonify({"status":"success"})


@login_required
@app.route('/publish_data_update',methods=['POST','GET'])
def publish_data_update():
    '''
    Get data to publish: action, model_name, sb_id, data

    - Application Source Name
    - Status (Active/Inactive) - (Inactive when it is deleted)
    - Action (what was done)
    - Data Name (Route, Customer, Location)
    - Payload
      - process_dt
      - updated_dt
      - added_dt
      - updated_by
      - added_by
      - datastore_id
      - datastore_parent_id
      - business_key
      - business_parent_key
      - parent_data_name
      - data_transaction_type
      - data_status
      - data elements...

    '''
    try:
        action = request.values.get('action')
        model_name = request.values.get('model_name')
        sb_id = request.values.get('id')
        data = None
        status = 'Active'
        sb = None
        ancestors = json.loads(request.values.get('ancestors'))

        if action != 'init':
            data = json.loads(request.values.get('data'))

        if action != 'remove':
            sbResp = restful.get_storage_blob(sb_id)
            if sbResp['status'] == 'success':
                sb = sbResp['storageBlob']
                if action not in ['create','edit']:
                    data = sb.data
            else:
                logging.info("Problem with this dataset... nothing more to do")
                return jsonify({"status":"failure"})
        else:
            status = 'Inactive'


        ######### Added to all schemas!!! ############
        data['datastore_id'] = int(sb_id)
        data['datastore_parent_id'] = 0
        data['business_parent_key'] = "unknown"
        data['parent_data_name'] = "unknown"
        data['added_by'] = 'System'
        data['updated_by'] = 'System'
        data['business_key'] = "unknown"
        data['added_dt'] = datetime.datetime.now().isoformat()
        data['updated_dt'] = datetime.datetime.now().isoformat()
        data['process_dt'] = datetime.datetime.now().isoformat()
        data['data_transaction_type'] = action
        data['data_status'] = status
        ######### Added to all schemas!!! ############

        if action != 'remove':
            ########  NOW GET THE ACTUAL INFO FROM THE STORAGE BLOB... IF IT WASN'T DELETED!!  ###########
            #sb = sbResp['storageBlob']  # Should have been retrieved just above
            data['added_by'] = sb.added_by.nickname() if sb.added_by else 'System'
            data['updated_by'] = sb.updated_by.nickname() if sb.updated_by else 'System'
            data['added_dt'] = sb.timestamp.isoformat()
            data['updated_dt'] = sb.up_timestamp.isoformat()
            data['business_key'] = sb.data_number_lookup
            if sbResp['parentInfo']:
                parInfo = sbResp['parentInfo']
                data['datastore_parent_id'] = parInfo['parentId']
                data['business_parent_key'] = parInfo['parentDataLookup']
                data['parent_data_name'] = parInfo['parentModelName']

        pubData = {'app_name': 'CustomerTracking'}
        pubData['status'] = status
        pubData['action'] = action
        pubData['model_name'] = model_name
        pubData['payload'] = data
        pubData['schema'] = None

        schema = restful.get_schema(model_name)
        if schema['status'] == 'success':
            pubData['schema'] = schema['schema']
        else:
            logging.warning("can't publish without a schema... ERROR WIL ROBINSON... ")
            return jsonify({"status":"failure"})

        pubsub_util.publish_data(pubData, app.config['PUBSUB_TOPIC'], app.config['PUBSUB_TOPIC_PROJECT'])
        return jsonify({"status":"success"})
    except:
        traceback.print_exc(file=sys.stdout)
        logging.error("Unexpected error: {}".format(sys.exc_info()[0]))
        return jsonify({"status":"failed"})

if __name__ == '__main__':
    app.run(host='0.0.0.0',debug=True)
