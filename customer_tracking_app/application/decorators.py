"""
decorators.py

Decorators for URL handlers

"""

from functools import wraps
from flask import redirect, request, abort
from application.models import ApiUser
from application import app_utils


def login_required(func):
    """Requires standard login credentials"""
    @wraps(func)
    def decorated_view(*args, **kwargs):
        userEmail = app_utils.get_user_email()
        if not userEmail:
            #return redirect(users.create_login_url(request.url))
            abort(401)  # Unauthorized
        return func(*args, **kwargs)
    return decorated_view


def admin_required(func):
    """Requires App Engine admin credentials"""
    @wraps(func)
    def decorated_view(*args, **kwargs):
        userEmail = app_utils.get_user_email()
        if not userEmail:
            #return redirect(users.create_login_url(request.url))
            abort(401)  # Unauthorized
        #if users.get_current_user():
        #    if not users.is_current_user_admin():
        #        abort(401)  # Unauthorized
        return func(*args, **kwargs)
        #return redirect(users.create_login_url(request.url))
    return decorated_view


def api_required(func):
    """Requires user to have an api credential established and pass in a token"""
    @wraps(func)
    def decorated_view(*args, **kwargs):
        id_token = request.headers['Authorization'].split(' ').pop()
        if not ApiUser.validateToken(id_token):
            #return 'Unauthorized', 401
            abort(401)  # Unauthorized
        return func(*args,**kwargs)
    return decorated_view
