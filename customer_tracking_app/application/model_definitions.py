'''
Created on Dec 18, 2017

@author: jason

----------------------------------------------------------------
----- on 3/10/2020 it was decided to clear this class out... 
to see it as it was previously.. check https://gitlab.com/jasonbowles/customer-tracking/-/blob/VMarch10_2020/customer_tracking_app/application/model_definitions.py
----------------------------------------------------------------
'''
from application.models import DataStorageType, StorageBlob,\
    AppConfiguration
from application.plant_items import master_plant_items
import random
import os, logging


def get_option_jmespath_simple(storeName, matchField, matchValue, selectField):
    '''
    Expected:  SelectSimple:options.name:dude:item_name:false
    Broken down:  <Type>:<StorageName>.<FieldName>:<MatchValue>:<ItemSelect>:<IncludeId>  (last one is optional, default is False)
    '''
    return 'SelectSimple:'+storeName+"."+matchField+":"+matchValue+":"+selectField

def get_option_jmespath_adv(storeName, jmPath, selectField):
    '''
    Expected:  SelectAdv:options[?name == dude && ?active == true]:business_type
    Broken down:  <Type>:<StorageName>:<jmespath_expression>:<ItemSelect>:<IncludeId>  (last one is optional, default is False)
    '''
    return 'SelectAdv:'+storeName+":"+jmPath+":"+selectField


def get_option_jmespath(argOptionName):
    optionJmespath = "options[?option_name == '__OPTION_NAME__'].{option_id: id, label: '{{idx}}. {{value}}', value: option_value, json_path: 'options[?id == `{{option_id}}`].option_name'}"
    return optionJmespath.replace("__OPTION_NAME__", argOptionName)
"""
=============================================================================================

BEGINNING OF UPDATES...
=============================================================================================
"""
# PUT UPDATES HERE
"""
=============================================================================================

END OF UPDATES...
=============================================================================================

"""

def checkList(retList, setupFunc):
    s = checkOrInstall(setupFunc)
    if s:
        retList.append(s)

def checkOrInstall(setupFunc):
    cfg = setupFunc(True) # Get the configuration information

    didSetup = None

    if not AppConfiguration.has_been_configured(cfg['cfgName'], cfg['cfgNumber']):
        logging.info("running configuration: "+cfg['cfgName']+"("+str(cfg['cfgNumber'])+")")
        cfg = setupFunc(False)
        AppConfiguration.add_configuration(**cfg)
        didSetup = cfg['cfgData']

    return didSetup

def create_data_def():
    funcs = []
    #funcs.append(create_option_data)
    

    #=========================
    # UPDATES
    #=========================
    #funcs.append(updateCustomerItem)
    

    si = []

    for aFunc in funcs:
        resp = checkOrInstall(aFunc)
        if resp:
            si.append(resp)

    return si

def setup_appengine():
    si = create_data_def()
    #checkList(si, create_master_items)

    return si

def setup_local():
    si = create_data_def()
    #checkList(si, create_master_items)
    #checkList(si, create_test_data)
    #checkList(si, testImageTable)

    return si

def setup_env(appEnv):
    if appEnv == "PROD":
        print(setup_appengine())
    else:
        print(setup_local())
