// as of 4/7/2019 all should be using this class for the storage_blob stuff
var storage_items = {};
var dtc_items = {};

function replace_all(instr, old, nw) {
	while (instr.search(old) >= 0) {
		instr = instr.replace(old, nw);
	}
	return instr;
}

function update_vals(json_path, jObj) {
	const regex = /(\{\{\s*(\w+)\s*\}\})/g;

	let m;
	var replace = []
	if (isNotNull(jObj)) {
		while ((m = regex.exec(json_path)) !== null) {
			// This is necessary to avoid infinite loops with zero-width matches
			if (m.index === regex.lastIndex) {
				regex.lastIndex++;
			}

			if (m.length > 2 && isNotNull(get_or_default(jObj, m[2], null))) {
				replace.push({ o: m[1], n: jObj[m[2]] });
				//json_path = replace_all(json_path,m[1],jObj[m[2]]);
			}
		}
		for (var i = 0; i < replace.length; i++) {
			json_path = json_path.replace(replace[i].o, replace[i].n);
		}
	}
	return json_path;
}

function GetException(message) {
	this.message = message;
	this.name = 'GetException';
}

/**
 *
 * @param obj The JSON object that we want a value from
 * @param attr The attribute that we are looking for
 * @param default_val The default value if the object does not contain the attribute yet
 * @returns  The value from the JSON object or the default value
 */
function get_or_default(obj, attr, default_val) {
	if (obj[attr] === undefined) {
		return default_val;
	}
	return obj[attr];
}

/**
 *
 * @param a string that may have a number in brackets
 * @returns javascript object {strVal: string part, numVal: number part} or null if the string doesn't contain
 *          bracket with a number inside
 */
function getArrayNumber(element) {
	var regExp = /\[([0-9]*)\]/;
	var regVal = regExp.exec(element);
	if (isNotNull(regVal)) {
		return { strVal: regVal.input.slice(0, regVal.index), numVal: Number(regVal[1]) };
	}
	return null;
}

/**
 *  Like the java toString function
 *
 * @param obj the thing to convert to a string
 * @returns the string representation of the object
 */
function toString(obj) {
	return String(obj);
}

/**
 * @returns true if the object is null
 */
function isNull(obj) {
	var isNull = false;
	if (obj === null || obj === undefined) {
		isNull = true;
	}
	return isNull;
}

/**
 * @returns true if the object is not null
 */
function isNotNull(obj) {
	return !isNull(obj);
}

/**
 * 
 * @param {any object} obj 
 */
function isEmpty(obj){
	if (isNotNull(obj)){
		if (obj.trim() === ''){
			return true
		}
		return false
	}
	return true
}

/**
 * For string comparisons
 *
 * @param thisVal
 * @param thatVal
 * @returns
 */
function equalsIgnoresCase(thisVal, thatVal) {
	if (isString(thisVal) && isString(thatVal)) {
		if (thisVal.toLowerCase() == thatVal.toLowerCase()) {
			return true;
		}
		return false;
	}
	return false;
}

/**
 * Checks if the variable "myVar" is equal to a string object
 * @returns
 */
function isString(myVar) {
	if (typeof myVar === 'string' || myVar instanceof String) {
		return true
	}
	return false;
}

/**
 * Holds the storage fields for a Group
 *
 * Each Storage Container represents a JSON object schema
 *
 * Storage containers only hold the id initially
 *
 * For example
 *
 * Customer:
 *   - Name: Jason
 *   - Age: 32
 *   - Nicknames: [Jay,JB]
 *   - Addresses: [
 *       - Type: Home
 *       - Street: 111 Somewhere St.
 *       - City: Peoria
 *       - State: IL
 *       - Zipcode: 61753
 *       ,
 *       - Type: Work
 *       - Street: 123 Anywhere Blvd.
 *       - City: Havanna
 *       - State: UT
 *       - Zipcode: 23164
 *       ]
 *   - Employer:
 *     - Name: Zeller Electric
 *     - Address:
 *       - Type: Home
 *       - Street: 365 War Memorial
 *       - City: Bartonville
 *       - State: IL
 *       - Zipcode: 74892
 *
 * Is Represented BY:
 *
 * StorageContainer (id: 1)
 *   - StorageField (Name, Type = String)
 *   - StorageField (Age, Type = Int)
 *   - StorageField (Nicknames, Type = StorageFieldArray, isList: True)
 *   - StorageField (Addresses, Type = StorageContainerArray, isList: True)
 *     - StorageField (Address, Type = StorageContainer)
 *       - StorageField (Type, Type = String)
 *       - StorageField (Street, Type = String)
 *       - StorageField (City, Type = String)
 *       - StorageField (State, Type = String)
 *       - StorageField (Zipcode, Type = String)
 *     - StorageField (Address, Type = StorageContainer)
 *       - StorageField (Type, Type = String)
 *       - StorageField (Street, Type = String)
 *       - StorageField (City, Type = String)
 *       - StorageField (State, Type = String)
 *       - StorageField (Zipcode, Type = String)
 *   - StorageField (Employer, Type = StorageContainer)
 *     - StorageField (Name, Type = String)
 *     - StorageField (Address, Type = StorageContainer)
 *       - StorageField (Type, Type = String)
 *       - StorageField (Street, Type = String)
 *       - StorageField (City, Type = String)
 *       - StorageField (State, Type = String)
 *       - StorageField (Zipcode, Type = String)
 *
 *
 *       Uses:
 *       1.  Keep your data separate and then as needed to validate, store, etc.. loadJson do what is needed
 *       	a. First clear the object by calling "clear()"
 *       	b. Then call load_object
 *
 *       2.  Create a copy of the StorageContainer and store each element in an object
 *
 */
class StorageContainer {
	constructor(id, storage_name, fields, isList, storage_url, parent_id, scExtends) {
		this.id = id;
		this.storage_name = storage_name;  // also known as group name in the StorageField
		this.fields = fields;
		this.ext_container = null;
		if (scExtends !== undefined && scExtends !== null && scExtends !== 'None') {
			this.ext_container = scExtends;
		}
		this.isList = isList;
		this.storage_url = storage_url;
		this.parent_id = parent_id;

		this.aFieldUpdated = false;
		this.dataLoaded = false;

		this.field_storage = {};

		this.store_fields();
		this.storage_key = null;
		this.raw_json = null;
		this.parent = null;
		this.field_name = null;
	}

	/**
	 *  function to save the table and passing in values
	 *
	 */
	save_table_vals(argRepeats, argExtends, successCallback, failureCallback) {
		this.ext_container = argExtends;
		this.is_list = argRepeats;
		this.save_table(successCallback, failureCallback);
	}

	save_table(success_callback, failure_callback) {
		var update = {
			action: 'save',
			model_name: this.storage_name,
			repeating: this.is_list,
			extending: this.ext_container,
			data: []
		};


		var options = {
			url: '/update_fields',
			data: { update: JSON.stringify(update) },
			dataType: 'json'
		};

		if (success_callback === undefined) {
			success_callback = function (data) { console.log(data) };
			failure_callback = function (reason) { console.log(reason) };
		}

		if (failure_callback === undefined) {
			failure_callback = function (reason) { console.log(reason) };
		}

		ajax(options).then(success_callback).catch(failure_callback);
	}

	/**
	 *
	 * set the value of the field name if this is child container
	 */
	set_field_name(fldName) {
		this.field_name = fldName;
	}

	/**
	 * Get the field name when this is a child container
	 */
	get_field_name() {
		return this.field_name;
	}

	/**
	 *
	 * Set the parent of this Storage Field, it should be a StorageContainer
	 *
	 */
	set_parent(strg_cntr) {
		this.parent = strg_cntr;
	}

	/**
	 *
	 * @return the StorageContainer which is the parent of this field
	 *
	 */
	get_parent() {
		return this.parent;
	}

	/**
	*
	* @return the jmespath
	*/
	get_json_path(deep = true) {
		var self_path = "{{field_name}}";

		if (isNull(this.parent)) {
			return this.storage_name + "[?id == `{{" + this.storage_name + "_id}}`]";
		} else {
			if (deep) {
				var parent_path = this.parent.get_json_path();
				self_path = parent_path + "." + self_path;
			}
			if (this.isList) {
				self_path = self_path + "[?id == `{{" + this.storage_name + "_id}}`] | [0]";
			}
			self_path = update_vals(self_path, { field_name: this.get_field_name() });
		}

		return self_path;
	}

	update_field(fieldName, value) {
		var fldDef = get_or_defalut(this.field_storage, fieldName, null);
		if (fldDef != null) {
			fldDef.field.setValue(value);
			return true;
		}
		return false;
	}

	create_copy() {
		var new_fields = [];
		for (var i = 0; i < this.fields.length; i++) {
			new_fields.push(this.fields[i].create_copy())
		}
		return new StorageContainer(0, this.storage_name, new_fields, this.isList, this.storage_url, 0, this.ext_container);
	}

	set_parent_id(parentId) {
		this.parent_id = parentId;
	}

	/**
	 * get the json object for this schema structure
	 * @returns the json object for this schema structure
	 */
	getJson() {
		var jsonObj = { id: this.id };
		var arrLen = this.fields.length;
		if (!this.dataLoaded) {
			return jsonObj;
		}
		for (var i = 0; i < arrLen; i++) {
			var field = this.fields[i];
			var fObj = field.getField();
			jsonObj[fObj.fieldName] = fObj.fieldValue;
		}
		return jsonObj;
	}

	/**
	 * Get array of fields for json data object
	 */
	getJsonData() {
		var data = [];
		var self = this;
		$.each(this.fields, function (i, el) {
			data.push(el.getJsonData());
		});

		return data;
	}

	/**
	 * Get the data table info for the columns for Storage Fields
	 */
	process_fields() {
		var order = ['field_name', 'field_type', 'field_default', 'is_key_field', 'field_required', 'is_option_filled', 'option_container', 'is_list', 'group_name', 'field_order', 'category'];

		var editFields = [];
		var usedFields = [];

		var cols = [];
		cols.push({ data: null, defaultContent: '', className: 'select-checkbox', orderable: false });

		if (this.fields !== null && this.fields !== undefined && this.fields.length > 0) {
			var allFlds = this.fields[0]._properties;
			for (var j = 0; j < order.length; j++) {

				for (var f = 0; f < allFlds.length; f++) {
					if (allFlds[f] === order[j]) {
						var proc = this.process_field(allFlds[f]);
						editFields.push(proc.field);
						cols.push(proc.col);
						usedFields.push(allFlds[f]);
						break;
					}
				}
			}

			for (var j = 0; j < allFlds.length; j++) {
				var key = allFlds[j];
				if (usedFields.indexOf(key) < 0 && key !== 'DT_RowId') {
					var proc = this.process_field(key);
					editFields.push(proc.field);
					cols.push(proc.col);
					usedFields.push(key);
				}
			}
		}

		return { editFields: editFields, cols: cols };
	}
	/**
	 *  Get storage field property datatable meta data
	 */
	process_field(inField) {
		var column = { data: inField, title: createLabel(inField) };
		var aField = { label: createLabel(inField), name: inField }
		if (inField === "is_list" || inField === "is_option_filled" || inField === "is_key_field" || inField === "field_required") {
			aField['type'] = "select";
			aField['options'] = [{ label: "True", value: true },
			{ label: "False", value: false }];
		}
		if (inField === "field_type") {
			aField['type'] = "select";
			aField['options'] = [
				{ label: "String", value: "string" },
				{ label: "Currency", value: "currency" },
				{ label: "Float", value: "float" },
				{ label: "Int", value: "int" },
				{ label: "Boolean", value: "boolean" },
				{ label: "Date", value: "date" },
				{ label: "Datetime", value: "datetime" },
				{ label: "Group", value: "group" },
				{ label: "Auto Increment", value: "auto_inc" },
				{ label: "Read Only", value: "readonly" },
				{ label: "Auto Date", value: "dateauto" }]
		}
		return { field: aField, col: column };
	}

	/**
	 * -------------
	 * | Here is where we'll build the datatables... pass in a table name here
	 * -------------
	 */
	create_dt_tables(tableId) {
		var dtInfo = this.process_fields();

		var model_name = this.storage_name;
		var parent_id = this.id;

		var editor = new $.fn.dataTable.Editor({
			ajax: {
				url: "/update_schema_fields",
				type: "POST",
				data: function (in_data) {
					var updated_data = add_model_name_schema(in_data, model_name, parent_id);
					return updated_data;
				},
				dataType: "json",
				error: function (xhr, error, thrown) {
					console.log(error);
					console.log(xhr.resposeText);
					console.log(thrown);
				}
			},
			table: "#" + tableId,
			fields: dtInfo.editFields,
			formOptions: {
				inline: {
					submit: 'allIfChanged'
				}
			}
		});


		var table = $('#' + tableId).DataTable({
			dom: "Bfrtip",
			data: this.getJsonData(),
			columns: dtInfo.cols,
			responsive: true,
			"order": [[10, "asc"]],
			keys: {
				columns: ':not(:first-child)',
				keys: [9],
				editor: editor,
				editOnFocus: true
			},
			select: {
				style: 'os',
				selector: 'td:first-child'
			},
			buttons: [
				{ extend: "create", editor: editor },
				{ extend: "edit", editor: editor },
				{ extend: "remove", editor: editor }
			]
		});

		return { dt_table: table, dt_editor: editor };
	}
	/**
	 * Load a json object that is represented by this schema
	 * Iterate through the keys and store the value in the StorageField
	 * If the fields is a Group, it is stored as a StorageContainer... the instantiation and load will set that trigger off
	 * (RECURSIVELY!! muh hahahaha)
	 * @jsonObj the JSON object that needs to be stored, each field is stored along side its StorageField
	 */
	load_object(jsonObj) {
		this.raw_json = jsonObj;
		this.process_group(this, jsonObj);
		this.dataLoaded = true;
		this.createKey();
	}


	/**
	 * Process the json object.. if this is a group recursively process
	 *
	 * TODO need to process arrays and plain fields.  The idea is that the data will be loaded in the schema
	 *
	 * StorageField
	 *   when Group the value is StorageContainer
	 *   when isList the value is Array and each element is a StorageField
	 *
	 *   So when a repeated Group occurs then it is stored as a isList == true and each element is a Group StorageField
	 *
	 */
	process_group(storageContainer, field_grp) {
		var storage = storageContainer.getFieldStorage();
		var attrs = Object.keys(field_grp);
		var arrLen = attrs.length;
		for (var i = 0; i < arrLen; i++) {
			var attr = attrs[i];
			var value = field_grp[attr];
			var fld_def = get_or_default(storage, attr, null);
			if (fld_def != null) {
				if (fld_def.field_type == 'group') {
					var struct = fld_def.field.structure;
					if (fld_def.field.is_list) {
						fld_def.field.value = []
						if (Array.isArray(value)) {
							for (var g = 0; g < value.length; g++) {
								var groupMember = value[g];
								var sc = struct.create_copy();
								sc.load_object(groupMember);
								fld_def.field.value.push(sc);
							}
						}
					} else {
						var sc = struct.create_copy();
						sc.load_object(value);
						fld_def.field.value = sc;
					}

				} else {
					if (fld_def.field.isList()) {
						store_array_sf(fld_def.field, value)
					} else {
						fld_def.field.value = value;
					}
				}
			}
		}
	}

	/**
	 * send the data to the server to be stored, if this is NEW, save the id in the id field
	 */
	save_data() {
		this.save_table();
	}

	/**
	 * iterate through the fields and store them by name.
	 * assumption is that there are no duplicate field names
	 */
	store_fields() {
		var arrLen = this.fields.length;
		for (var i = 0; i < arrLen; i++) {
			var fld = this.fields[i];
			this.field_storage[fld.field_name] = { field_type: fld.field_type, field: fld };  // just a regular field to store
		}
	}

	getFieldStorage() {
		return this.field_storage;
	}

	/**
	 *
	 *  Method expects that the passed name might be a string xpath
	 *  Examples:
	 *  foo.name = get the attribute "name" from the "foo" object
	 *
	 */
	getFieldStorageByName(inputName) {
		var names = inputName.split(".");
		if (names.length > 0) {
			if (names[0] == this.storage_name && names.length > 1) {
				names = names.slice(1);
			}
			return this.getFieldStorageByNameInternal(names, names.length);
		}
		return null;
	}

	getFieldByName(inputName) {
		var fldStorage = this.getFieldStorageByName(inputName);
		if (isNotNull(fldStorage)) {
			return fldStorage.getField();
		}
		return null;
	}

	setFieldByName(inputName, inputValue) {
		var fldStorage = this.getFieldStorageByName(inputName);
		if (isNotNull(fldStorage)) {
			fldStorage.setValue(inputValue);
		}
		else {
			throw new GetException("Did not find field to set: " + inputName)
		}
	}

	validateValue(inputName, inputValue) {
		var fldStorage = this.getFieldStorageByName(inputName);
		if (isNotNull(fldStorage)) {
			fldStorage.setValue(inputValue);
			return fldStorage.validate();
		}
	}

	/**
	 *
	 *
	*/
	getFieldStorageByNameInternal(nameArray, arrLen) {
		if (arrLen == 0) {
			throw new GetException("Nothing to get, you didn't specify a field for the container");
		}
		var names = nameArray;
		var curElement = names[0];
		var arrVal = getArrayNumber(curElement);
		if (isNotNull(arrVal)) {
			curElement = arrVal.strVal;
			var elementNbr = arrVal.numVal;
			var fld = this.field_storage[curElement];
			if (isNull(fld)) {
				throw new GetException("That field (" + curElement + ") doesn't even exist!!!");
			}
			if (fld.field.is_list) {
				// just making sure here
				if (fld.field_type == "group") {
					var getSc = fld.field.value[elementNbr];
					if (isNull(getSc)) {
						throw new GetException("There is no container for that number... " + elementNbr + ", you went too far!!");
					} else {
						return getSc.getFieldStorageByNameInternal(names.slice(1), arrLen - 1);
					}
				} else {
					var getFld = fld.field.value[elementNbr];
					if (isNull(getFld)) {
						throw new GetException("There is no field for that number... " + elementNbr + ", you went too far!!");
					} else {
						return getFld;  // Return the field Storage
					}

				}
			}
			else { throw new GetException("Passed a number to a non-array element!!!! Dummy!"); }
		}
		else {
			var fld = this.field_storage[curElement];
			if (isNotNull(fld)) {
				if (fld.field_type == "group" && names.length > 1) {
					return fld.field.value.getFieldStorageByNameInternal(names.slice(1), arrLen - 1);
				}
				else {
					return fld.field;  // Return the field Storage
				}
			}

		}
		return null;
	}

	/**
	 * Get the field names for this storageContainer
	 */
	getFieldNames() {
		var names = [];
		var arrLen = this.fields.length;
		for (var i = 0; i < arrLen; i++) {
			names.push(this.fields[i].field_name);
		}
		return names;
	}

	/**
	 * Validate the fields in this container
	 *
	 * call the validate function on each field in the container...
	 * @returns {valid: <boolean>, errors: [{field: <field_name>, msg: <message>}, ...]}
	 */
	validate() {
		var jsonObj = { valid: true, errors: [] };
		var arrLen = this.fields.length;
		for (var i = 0; i < arrLen; i++) {
			var field = this.fields[i];
			var valid_f = field.validate();
			if (!valid_f.valid) {
				jsonObj.valid = false;
				for (var e = 0; e < valid_f.errors.length; e++) {
					jsonObj.errors.push(valid_f.errors[e]);
				}
			}
		}
		return jsonObj;
	}

	createKey() {
		var key_fields = this.getKeyData();
		this.storage_key = "";
		var keyLen = key_fields.length;
		for (var k = 0; k < keyLen; k++) {
			this.storage_key = this.storage_key + key_fields[k];
			if (k < (keyLen - 1)) {
				this.storage_key = this.storage_key + "-"
			}
		}
		return this.storage_key;
	}

	getKeyData() {
		var key_data = [];
		var arrLen = this.fields.length;
		for (var i = 0; i < arrLen; i++) {
			var field = this.fields[i];
			if (field.is_key_field) {
				if (!field.isList() && !(field.isGroup() && field.is_list) && !(field.isGroup() && field.value.dataLoaded)) {
					if (field.isGroup()) {
						key_data.push(field.value.createKey());
					} else {
						key_data.push(field.getField().fieldValue);
					}
				}
			}
		}
		return key_data;
	}

	/**
	 * Call the server to get this data element
	 */
	get_data() {
		$.get("/storage_blob/dt_update", { update: { model_name: this.storage_name, id: this.id } })
			.done(function (data) {
				var id = load_object(data);
				this.id = id;
			});
	}

}
/**
 * The Storage Field class represents 1 element of a storage item.
 * This can be used to validate each field and do the following:
 * 1. Handle field validation
 *    a. Is the field required
 *    b. Is there min and max length
 *    c. does the fields meet the type requirements (i.e. Is a integer really an integer)
 *
 * 2. Handle Groups
 * 	  a.  If the field is a group, what is stored in the value is actually a StorageContainer
 *
 * Valid Types:
 *   string
 *   int
 *   float
 *   currency  valid float and currency
 *   date
 *   datetime  valid date and datetime
 *   group (StorageContainer)
 *   field_array     validates as list and field_array
 *   boolean
 *   image <- the image isn't what gets stored, but rather the image name
 *   auto_inc <- field that is readonly and set on the server side
 *   readonly <- Is usually a copy of another field, but can't be updated and is a string
 */
class StorageField {
	constructor(argId, field_order, field_name, group_name, field_type, field_default, is_key_field, field_required, is_option_filled, option_data, repeated, category, structure, option_container, description) {
		var self = this;
		this.field_order = field_order;
		this.field_name = field_name;
		this.group_name = group_name;
		this.field_type = field_type;
		this.field_default = field_default;
		this.is_key_field = is_key_field;
		this.field_required = field_required;
		this.is_option_filled = is_option_filled;
		this.category = category;
		this.option_container = option_container
		this.description = description;
		this.is_list = repeated;

		// This is the list of properties that are stored on DataStore
		this._properties = Object.getOwnPropertyNames(this);

		this._id = argId;
		this.structure = structure;
		this.group_data = {};
		this.option_data = option_data;
		this.value = null;
		this.parent = null;

		if (this.field_type == "group") {
			this.value = this.structure;
		}

		this.valueFunction = (function (inValue) {
			return inValue;
		}).bind(this);

		if (this.field_type === 'complex' || this.field_type === 'complex_arr')
		{
			this.valueFunction = (function (inValue){
				if (inValue === undefined || inValue === null) {
					return "";
				}
				if (Array.isArray(inValue)){
					var vals = [];
					$.each(inValue, function (i, e) {
						var key = Object.keys(e)[0];
						vals.push(e[key].split("|").pop());
					});
					return vals.join(",");
				}else{
					var key = Object.keys(inValue)[0];
					return inValue[key].split("|").pop()
				}
			}).bind(this);
		}else{
			if (this.isOptionFilled() && this.option_container.startsWith("Select")) {
				var vals = this.option_container.split(":")
	
				if (vals.length === 5 && vals.pop().toLowerCase() === "true") {
					this.valueFunction = (function (inValue) {
						if (inValue === undefined || inValue === null) {
							return "";
						}
						if (Array.isArray(inValue)) {
							var vals = [];
							$.each(inValue, function (i, e) { 
								if(typeof e === "object"){
									var key = Object.keys(e)[0];
									vals.push(e[key].split("|").pop());
								}else{
									vals.push(e.split("|").pop()); 
								}
								
							});
							return vals.join(",");
						} else {
							if (typeof inValue === "object"){
								var key = Object.keys(inValue)[0];
								return inValue[key].split("|").pop()
							}else{
								return inValue.split("|").pop();
							}
						}
					}).bind(this);
				}
			}
		}
	}

	/**
	 *  Get json data object
	 */
	getJsonData() {
		var jObj = {
			DT_RowId: this._id
		};
		var props = this._properties;
		var self = this;
		$.each(props, function (i, el) {
			jObj[el] = self[el];
		});
		return jObj;
	}

	/**
	 *
	 * Set the parent of this Storage Field, it should be a StorageContainer
	 *
	 */
	set_parent(strg_cntr) {
		this.parent = strg_cntr;
	}

	/**
	 *
	 * @return the StorageContainer which is the parent of this field
	 *
	 */
	get_parent() {
		return this.parent;
	}

	/**
	 *
	 * @return the jmespath for this field
	 *
	 */
	get_json_path(deep = true) {
		var parent_path = this.parent.get_json_path(deep);
		if (this.isGroup() && this.structure !== null && this.structure !== undefined) {
			var grp_path = this.structure.get_json_path(deep);
			return update_vals(grp_path, { field_name: this.field_name })
		}
		return parent_path + "." + this.field_name;
	}

	/**
	 * this method is required to process arrays
	 *
	 * Structure holds the initial storage container.. once a field is set then the Storage container is in "value"
	 *
	 * This function is not meant to copy values.. but to copy the structure
	 */
	create_copy() {
		var new_structure = null;
		if (this.isGroup()) {
			new_structure = this.structure.create_copy();
		}
		var sf = new StorageField(this._id, this.field_order, this.field_name, this.group_name, this.field_type, this.field_default, this.is_key_field, this.field_required, this.is_option, this.option_date, this.is_list, this.structure, this.option_container, this.description);
		return sf;
	}

	setToReadOnly() {
		this.field_type = "readonly";
	}

	/**
	 * Is the field a string
	 * @returns boolean value
	 */
	isString() {
		return equalsIgnoresCase(this.field_type, "string");
	}

	/**
	 * Is the field a boolean
	 * @returns boolean value
	 */
	isBoolean() {
		return equalsIgnoresCase(this.field_type, "boolean");
	}

	/**
	 * Is the field a date
	 * @returns boolean value
	 */
	isDate() {
		return equalsIgnoresCase(this.field_type, "date") || equalsIgnoresCase(this.field_type, "datetime");
	}

	/**
	 * This field is readonly... initialized on creation of the container, but never updated
	 * Underlying this is a String
	 */
	isReadOnly() {
		return equalsIgnoresCase(this.field_type, "readonly");
	}

	/**
	 * This is the field type that auto updates to the current date time.. Like a last updated field
	 */
	isDateAuto() {
		return equalsIgnoresCase(this.field_type, "dateauto");
	}

	/**
	 * Is the field a date time
	 * @returns boolean value
	 */
	isDatetime() {
		return equalsIgnoresCase(this.field_type, "datetime");
	}

	/**
	 * Is the field a currencty
	 * @returns boolean value
	 */
	isCurrency() {
		return equalsIgnoresCase(this.field_type, "currency");
	}

	/**
	 * Is the field an integer
	 * @returns a boolean value
	 */
	isInt() {
		return equalsIgnoresCase(this.field_type, "int");
	}

	/**
	 * Is the field a Float
	 * @returns a boolean value
	 */
	isFloat() {
		return equalsIgnoresCase(this.field_type, "float") || equalsIgnoresCase(this.field_type, "currency");
	}

	/**
	 * Is the field an Image
	 * @returns a boolean value
	 *
	 */
	isImage() {
		return equalsIgnoresCase(this.field_type, 'image')
	}

	/**
	 *
	 * Is this field an auto incremented field
	 */
	isAutoInc() {
		return equalsIgnoresCase(this.field_type, "auto_inc")
	}

	/**
	 * Is the field a list
	 * @returns a boolean value
	 */
	isList() {
		return (isNotNull(this.is_list) && this.is_list && !this.isGroup());
	}

	/**
	 * validate if the field is an array of StorageField
	 * @returns a boolean value
	 */
	isFieldArray() {
		return equalsIgnoresCase(this.field_type, "field_array");
	}

	/**
	 * Is the field a group (Meaning the value is a StorageContainer)
	 * @returns a boolean value
	 */
	isGroup() {
		return equalsIgnoresCase(this.field_type, "group");
	}

	/**
	*
	* @returns a boolean value indicating this is an option select field
	*/
	isOptionFilled() {
		return this.is_option_filled;
	}

	/**
	 * Check if the field is valid given rules
	 * @returns a a boolean value
	 */
	validate_type() {
		return this.isString() || this.isDate() || this.isFloat() || this.isBoolean() || this.isInt() || this.isImage() ||
			this.isList() || this.isGroup() || this.isFieldArray() || this.isAutoInc() || this.isReadOnly() || this.isDateAuto();
	}


	/**
	 * The input value should be a regular piece of data, unless this element is a group
	 */
	setValue(input) {
		this.value = input;
	}


	/**
	 * returns just the value but not the field name
	 * @see getField for that -
	 * @returns value that is one of (simple value, array of simple values, StorageContainer or Array of StorageContainers)
	 */
	getValue() {
		return this.value;
	}

	/**
	 * The idea here is to update a group element
	 *   the assumption is that the group element has already been saved and the value is equal to the id
	 *   STEPS
	 *   1. Save the jsonObj into the dictionary
	 *   2. Push the saved object to the server sending the groupId along with it
	 *   3. Do this behind the scenes
	 *
	 */
	updateGroupData(jsonObj) {
		if (isGroup()) {
			if (this.value != null && this.value instanceof StorageContainer) {
				this.value.load_object(jsonObj);
			}
		}
	}

	/**
	 *
	 * get an array of key data
	 */
	getKeyData() {

	}

	/**
	 * convenience method to validate a value without overriding the stored value
	 *
	 * @returns {valid: true, errors: [{field: <field_name>, msg: <message>}]}
	 */
	validate_datum(datum) {
		var oldDatum = this.getValue();
		this.setValue(datum);
		var resp = this.validate();
		this.setValue(oldDatum);
		return resp;
	}

	/**
	 *
	 * @returns {valid: true, errors:[{field: <field_name>, msg: <message>}]}
	 */
	validate() {
		var validation = { valid: true, errors: [] }
		var actualValue = this.value;
		if ((this.isFloat() || this.isCurrency() || this.isInt()) && actualValue.trim() === '') {
			actualValue = null;
		}
		var value = this.getValueFrom(this.value);
		if (this.isAutoInc() || this.isReadOnly() || this.isDateAuto()) {
			return validation;
		}
		if (this.isList() && isNotNull(value)) {
			for (var l = 0; l < value.length; l++) {
				var lm = value[l];
				var valid_l = lm.validate();
				if (!valid_l.valid) {
					validation.valid = false;
					for (var e = 0; e < valid_l.errors.length; e++) {
						validation.errors.push(valid_l.errors[e]);
					}
				}
			}
		} else {
			if (this.isGroup() && isNotNull(value)) {
				if (this.is_list) {
					for (var g = 0; g < this.value.length; g++) {
						var gm = this.value[g];
						var valid_g = gm.validate();
						if (!valid_g.valid) {
							validation.valid = false;
							for (var e = 0; e < valid_g.errors.length; e++) {
								validation.errors.push(valid_g.errors[e]);
							}
						}
					}
				}
				else {
					var valid_g = this.value.validate();
					if (!valid_g.valid) {
						validation.valid = false;
						for (var e = 0; e < valid_g.errors.length; e++) {
							validation.errors.push(valid_g.errors[e]);
						}
					}
				}
			}
		}
		if (isNotNull(actualValue) && isNull(value.fieldValue)) {
			if (this.isOptionFilled() && actualValue.toLowerCase() === 'n/a'){
				// Allow a drop down selection to be "n/a".. added 3/20/2020
				return validation
			}
			validation.valid = false;
			validation.errors.push({ field: this.field_name, error: "The value entered: " + actualValue + ", is invalid for this field." });
			return validation
		}

		if (this.field_required && isNull(actualValue)) {
			validation.valid = false;
			validation.errors.push({ field: this.field_name, error: "The field: " + this.field_name + " is required" });
		}

		return validation;
	}

	/**
	 * @calls the getValueFromInput method passing the field_value
	 * @returns {fieldName: <name>, fieldValue: <value>}
	 */
	getField() {
		return this.getValueFrom(this.value);
	}

	/**
	 * @param inValue the value to set this StorageField
	 */
	setField(inValue) {
		this.value = this.getValueFrom(inValue);
	}

	/**
	 * safely get the correct value for the field
	 *
	 * if this is a group get the Json for that group
	 * @returns safe value that is loaded in small object {fieldName: <name>, fieldValue: <value>}
	 */
	getValueFrom(input) {
		var returnObj = { fieldName: this.field_name, fieldValue: null };
		if (input == null) {
			return returnObj;
		} else {
			if (this.isList()) { // Would be array of StorageFields
				if (Array.isArray(input)) {
					var arrLen = input.length;
					var resp = []
					for (var i = 0; i < arrLen; i++) {
						var inst = input[i];
						if (inst instanceof StorageField) {
							resp.push(inst.getField().fieldValue);
						}
						else {
							resp.push(inst);
						}
					}
					return { fieldName: this.field_name, fieldValue: resp };
				} else {
					// would it ever get here?
					return { fieldName: this.field_name, fieldValue: [] }; // return empty array
					//return {fieldName: this.field_name, fieldValue:JSON.parse(input)};
				}
			}
			if (this.isString()) {
				if (isNotNull(input)) {
					return { fieldName: this.field_name, fieldValue: toString(input) };
				}
			}
			if (this.isImage()){
				if (isNotNull(input) && Array.isArray(input)){
					return { fieldName: this.field_name, fieldValue: input  };
				}
			}
			if (this.isInt()) {
				if (!isNaN(input) &&
					parseInt(Number(input)) == input &&
					!isNaN(parseInt(input, 10))) {
					return { fieldName: this.field_name, fieldValue: parseInt(Number(input)) };
				} else {
					// assume it is something else and return null;
					return { fieldName: this.field_name, fieldValue: null };
				}
			}
			if (this.isFloat() || this.isCurrency()) {
				if (!isNaN(input) &&
					parseFloat(Number(input)) == input &&
					!isNaN(parseFloat(input, 10))) {
					return { fieldName: this.field_name, fieldValue: parseFloat(Number(input)) };
				} else {
					// assume it is something else and return null;
					return { fieldName: this.field_name, fieldValue: null };
				}
			}
			if (this.isBoolean()) {
				if (typeof (input) === "boolean") {
					returnObj = { fieldName: this.field_name, fieldValue: input };
				} else {
					if (typeof (input) === "string") {
						if (input.toLowerCase() === "true" || input.toLowerCase() === "false") {
							return { fieldName: this.field_name, fieldValue: input.toLowerCase === "true" };
						} else {
							return { fieldName: this.field_name, fieldValue: null }; // value was not true or false
						}
					} else {
						return { fieldName: this.field_name, fieldValue: null }; // not of type string
					}
				}
			}
			if (this.isDate() || this.isDatetime()) {
				if (Object.prototype.toString.call(input) === '[object Date]' && input instanceof Date) {
					returnObj = { fieldName: this.field_name, fieldValue: input };
				} else {
					if (typeof (input) === "string") {
						return { fieldName: this.field_name, fieldValue: new Date(input) };
					} else {
						return { fieldName: this.field_name, fieldValue: null };
					}
				}
			}
			/*
			 * Not sure how this will work
			 *
			 * OPTIONS
			 *
			 * Check if the input is a StorageContainer... if it is... load the data through the get_data method of the storage container
			 *   That should check if the data has already been fetched.
			 *
			 * The expectation is that the server only sends an id and name on page load, the page will fetch the remaining data
			 *
			 *
			 *
			 *
			 */
			if (this.isGroup()) {
				if (this.is_list) {
					var resp = [];
					if (Array.isArray(input)) {
						for (var g = 0; g < input.length; g++) {
							var gm = input[g];
							resp.push(gm.getJson());
						}
					} else {
						resp.push(input.getJson());
					}
					return { fieldName: this.field_name, fieldValue: resp };
				} else {
					if (input instanceof StorageContainer) {
						return { fieldName: this.field_name, fieldValue: input.getJson() };  // returns json structure or if not fetched.. just the id
					} else {
						return { fieldName: this.field_name, fieldValue: null };
					}
				}
			}
		}
		return returnObj;
	}


}

function compareStorageFields(thisStorageField, thatStorageField) {
	var thisOrder = parseFloat(get_or_default(thisStorageField, "field_order", 999.0));
	var thatOrder = parseFloat(get_or_default(thatStorageField, "field_order", 999.0));
	if (thisOrder < thatOrder)
		return -1;
	if (thisOrder > thatOrder)
		return 1;
	return 0;
}

function store_array_sf(sf, value) {
	sf.value = [];
	if (Array.isArray(value)) {
		for (var l = 0; l < value.length; l++) {
			var lm = value[l];
			var sf_copy = sf.create_copy();
			sf_copy.is_list = false
			sf_copy.setValue(lm);
			sf.value.push(sf_copy);
		}
	} else {
		if (isNotNull(value)) {
			var sf_copy = sf.create_copy();
			sf_copy.is_list = false
			sf_copy.setValue(value);
			sf.value.push(sf_copy);
		}
	}
}

class DtButtonClick {
	constructor(argTableId, argButtonType, argInitFunction) {
		this.table_id = argTableId;
		this.button_type = argButtonType;
		this.init_func = argInitFunction;
		this.initialized = false;
	}

	get_table_id() {
		return this.table_id;
	}

	get_button_type() {
		return this.button_type;
	}

	reset_info(in_table_id, in_button_type) {
		this.table_id = in_table_id;
		this.button_type = in_button_type;
	}

	init() {
		if (!this.initialized) {
			this.init_func();
			this.initialized = true;
		}
	}
}


/**
*
*
* The official way to link a storage container to a datatable!!!
*
*/
class DataTableCreator {
	constructor(parent_id, storage_container, fetchId, parent_path) {
		this.parent_id = parent_id;
		this.parent_path = parent_path;
		this.storage_container = storage_container;
		this.name = this.storage_container.storage_name;
		this.fetchId = fetchId;
		this.ancestry_names = this.create_ancestry_names();
		this.table_id = this.name + '-table';
		
		this.parent_field_name = this.get_parent_name(this.storage_container.parent, this.storage_container);

		this.dt_url = this.create_dt_url();

		this.option_fields = {};
		this.dtc_states = {};   // used to store specific states for the datatable
		this.dt_table = undefined;
		this.dt_editor = undefined;
		this.dt_fields = undefined;
		dtc_items[this.name] = this;
	}

	get_data(){
		return Array.from(this.dt_table.data());
	}

	set_dt_table(dt_table) {
		this.dt_table = dt_table;
		this.set_dtc_entry(dt_table, true);
	}

	set_dt_editor(dt_editor) {
		this.dt_editor = dt_editor;
		this.set_dtc_entry(dt_editor, false);
	}

	set_dt_objects(dt_table, dt_editor) {
		this.set_dt_table(dt_table);
		this.set_dt_editor(dt_editor);
	}

	/**
	*
	* This will call the parent and get the name of the parent and build
	* the ancestry_name
	*
	*/
	create_ancestry_names() {
		//
		var sc = this.storage_container.parent;
		var myArr = [];
		if (sc !== null) {
			while (sc.parent !== null) {
				myArr.unshift(this.get_parent_name(sc.parent, sc));
				sc = sc.parent;
			}
			myArr.unshift(sc.storage_name);
		}

		return myArr;
	}

	/**
	* Given the storage container and a parent, get the field name
	*  given by the parent
	*  ----- assumes the parents would not have dup group_names
	*/
	get_parent_name(parent, sc) {
		var fName = sc.storage_name;

		if (isNotNull(parent)) {
			var pFields = parent.fields;
			$.each(pFields, function (idx, fld) {
				if (fld.group_name === sc.storage_name) {
					fName = fld.field_name;
				}
			});
		}

		return fName;
	}

	/**
	*
	* This will call the parents recursively and build the json path
	*  Then use the parent_id to build out a fully functional json_path
	*   that can be used with jmespath search
	*
	*  UPDATE on 3/10/2020 switching to firestore...we will pass in the path to the collection
	*  We'll passing the following... path, parent_path, id and parent_id
	*
	*/
	create_json_path() {
		// Removed on 3/10/2020 (see above)
	}
	/**
	*
	* Create the url to call in order to update this table
	*
	*/
	create_dt_url() {
		var sc_model_name = this.parent_field_name;
		if (isEmpty(this.parent_field_name)){
			sc_model_name = this.storage_container.storage_name
		}

		if (this.fetchId === undefined) {
			var addl_args = "";
			var addl_args = "&parent_id=" + this.parent_id;
			return "/storage_blob/dt_update?model_name=" + sc_model_name + addl_args
		}
		else {
			var model_id = isNull(this.fetchId) ? "":this.fetchId;
			return "/storage_blob/dt_update?model_name=" + sc_model_name + "&model_id=" + model_id;
		}

	}

	/**
	*
	*  This is the function to create a table to store data.
	*   The end result is to create a "data table": https://datatables.net/
	*  params:
	*  - divId:  this is the div in the html where this will be placed
	*  - incl_buttons:  this is a list of buttons to include:
	*      available buttons:
	*       - new:  Create a new row
	*       - edit:  Edit the existing selection
	*       - duplicate:  Copy the selected record
	*       - view:  Pull a one page for this specific item (not to edit the values though)
	*
	*   The first action of this table is to do a call to load the initial data,
	*   If you already have data call "create_edit_table_table_with_data"
	*
	*   The return is the following object:
	*    - columns: The columns added to the data table
	*    - dt_editor:  the editor object for making updates to the data
	*    - dt_table:  this is the main datatable which is good for fetching the rows/columns
	*    - fields:  The simplified datatable specific fields  {label: <what is shown on screen>, name: <the name of the field>, type: datatable type}
	*    - json_path:  If you have the complete json loaded, you can use this with a jmespath search to find the list
	*    - name:  this is the name of the storage container (stoorage_name)
	*    - option_fields:  for option fields this could be pre-loaded with values
	*    - table_id:  This is the '#id' of the table, so you can access via jQuery or D3
	*/
	create_edit_data_table(divId, incl_buttons) {
		return this.create_edit_data_table_with_data(divId, incl_buttons, null);
	}

	/**
	 *  This function helps get the data ready for submission to the server
	 *
	 */
	add_model_name_core(data) {
		var vars = getUrlVars(this.url);
		if (isNull(storage_items[vars.model_name])){
			if (isNull(this.model_name)){
				vars.model_name = storage_items[pageData.args.model_name].getFieldStorage()[vars.model_name].field.group_name;
			} else{
				vars.model_name = this.model_name;
			}
		}
		var dtc = $('#' + vars.model_name + '-table').DataTable().dataTableCreator();
		var update = { action: data.action, data: [] };
		var keys = Object.keys(data.data);
		var models = {};
		for (var k = 0; k < keys.length; k++) {
			// first level should be the id
			// this code will remove the necessity to do this on the server
			var aKey = keys[k];
			var model = get_or_default(models, aKey, {});
			model['id'] = aKey;
			model['model_name'] = dtc.storage_container.storage_name;
			var mKeys = Object.keys(data.data[aKey]);
			var flds = {};
			for (var m = 0; m < mKeys.length; m++) {
				if (mKeys[m] !== 'action') {
					flds[mKeys[m]] = data.data[aKey][mKeys[m]];
				}
			}
			model['data'] = flds;
			var opFlds = dtc.option_fields;
			var opts = get_or_default(opFlds, model.id, null);
			model['option_data'] = "none";
			if (isNotNull(opts)) {
				model['option_data'] = opts;
			}

			models[aKey] = model;
		}
		var ms = Object.keys(models);
		for (var i = 0; i < ms.length; i++) {
			var m = models[ms[i]];
			update.data.push(m);
		}
		return { 'update': JSON.stringify(update) };
	}


	/**
	*
	*  Using the fields for the storage_container, create editor and table fields
	*
	*/
	process_dt_fields() {
		var editor_fields = [];
		var editor_cols = [];
		var hide_cols = [];
		var key_cols = [];
		var keyCnt = 2;
		var headers = ['', ''];
		var fields = jmespath.search(this.storage_container, "fields[?field_type!='group'].field_name");

		// add responsive control columns
		editor_cols.push({
			data: null, defaultContent: '',
			className: 'control',
			orderable: false
		});

		// add the check box
		editor_cols.push({
			data: null,
			defaultContent: '',
			className: 'select-checkbox',
			orderable: false
		});

		var arrayLength = fields.length;

		for (var i = 0; i < arrayLength; i++) {
			key_cols.push(keyCnt + i);
			var field = fields[i];
			var fldStorage = this.storage_container.field_storage[field].field;
			var lbl = createLabel(field);
			headers.push(lbl);
			var fi = { label: lbl, name: fldStorage.field_name };

			if (fldStorage.field_type === 'datetime' || fldStorage.field_type === 'date') {
				fi['type'] = 'datetime';
			}

			if (fldStorage.isAutoInc() || fldStorage.isReadOnly() || fldStorage.isDateAuto()) {
				fi['type'] = "readonly";
			}

			if (fldStorage.isDateAuto()) {
				hide_cols.push(fldStorage.field_name);
			}

			if (fldStorage.isOptionFilled()) {
				fi['type'] = 'select2';
				fi['opts'] = {
					placeholder: 'Please select a value from the dropdown list...',
					sorter: dropDownSort
				}
				fi['options'] = get_option_data(fldStorage);
			}

			var ci = { data: field, defaultContent: "<i>-- not set --</i>" };
			if (fldStorage.isCurrency()) {
				ci['render'] = $.fn.dataTable.render.number(',', '.', 2, '$');
			}

			if (fldStorage.isOptionFilled()) {
				ci['render'] = fldStorage.valueFunction
			}

			if (fldStorage.isImage()) {
				fi['type'] = 'uploadMany';
				fi['display'] = imageDisplay;
				fi['clearText'] = "clear";
				fi['noImageText'] = "No image";
				fi['ajax'] = '/upload_image';
				fi['className'] = "dt_image_full";
				ci['render'] = imageDisplay;
				ci['defaultContent'] = "No image";
				ci['className'] = "dt_image_full";
			}
			editor_fields.push(fi);
			editor_cols.push(ci);
		}
		return {
			editor_fields: editor_fields,
			editor_cols: editor_cols,
			headers: headers,
			key_columns: key_cols,
			hide_cols: hide_cols
		};
	}

	/**
	*
	*  Using D3, create the html table to be used to house the datatable,
	*   assumes that the we're being passed a div id
	*
	*/
	create_html_table(dv) {
		var table = d3.select('#' + dv)
			.append("div")
			.attr("class", "table-responsive")
			.append("table")
			.attr("class", "table table-striped table-bordered dt-responsive nowrap")
			.attr("cellspacing", "0")
			.attr("width", "100%")
			.attr("id", this.table_id);

		table.append('thead')
			.append('tr')
			.selectAll('th')
			.data(this.dt_fields.headers)
			.enter().append('th')
			.attr('id', function (d) { return 'hdr_' + d; })
			.text(function (d) { return d; });
	}

	_dt_editor_error(xhr,error,thrown){
		// go through and close all open forms.. .then submit error
		var tables = Object.keys(pageReturn.tables);
		tables.forEach(tbl => {
			var dtc = pageReturn.tables[tbl];
			dtc.dt_editor.close();
		});
		__handle_ajax_error(xhr);
	}

	/**
	*
	*  This function creates the editor that will us to
	*   update rows for the datatable
	*
	*/
	create_editor_dt() {
		var xtraFlds = getAdditionalEditorFields(this.name);
		var flds = this.dt_fields.editor_fields;
		if (xtraFlds.length > 0) {
			flds = this.dt_fields.editor_fields.slice().concat(xtraFlds);
		}


		var editor = new $.fn.dataTable.Editor({
			ajax: {
				url: this.dt_url,
				type: "POST",
				model_name: this.name,
				data: this.add_model_name_core,
				dataType: "json",
				error: this._dt_editor_error
			},
			table: "#" + this.table_id,
			fields: flds
		});
		this.set_dtc_entry(editor, false);

		this.dt_editor = editor;

		// Call out to add events

		// Do we have a custom form??
		var formTemplate = getCustomTemplates(this);
		if (formTemplate !== undefined) {
			editor.template(formTemplate);
		}

		// First add any "open" events
		var funcs = getEditorOpenFunctions(this);
		if (funcs !== undefined && funcs.length > 0) {
			$.each(funcs, function (idx, func) {
				editor.on('open', func);
			});
		}

		// allow for any "preSubmit" events
		var peFuncs = getEditorPreSubmitFunctions(this);
		if (peFuncs !== undefined && peFuncs.length > 0) {
			$.each(peFuncs, function (idx, func) {
				editor.on('preSubmit', func);
			});
		}

		// Now set any "close" events
		var cFuncs = getEditorCloseFunctions(this);
		if (cFuncs !== undefined && cFuncs.length > 0) {
			$.each(cFuncs, function (idx, func) {
				editor.on('close', func);
			});
		}

		// Set field change events
		addFieldChangeEvents(this);

		/**
		*
		*  this part handles the successful upload of a file (image) while the editor is open
		*   -- updated 3/23/2020:  The image information is now stored as part of the parent
		*/
		editor.on('uploadXhrSuccess', function (e, fieldName, json) {
			//imageInformation = json['files']['files'];
			var image = json['upload'];
			//var imgInfo = imageInformation[imgId];
			var startField = $('.' + fieldName);
			if (startField.length == 0) {
				startField = $('.DTE_Field_Name_' + fieldName);
			} else {
				startField = $(startField).parent();
			}
			$(startField).find('div.form_image_div').empty();
			var imgLocator = 'form_image_' + image.id.image_id + ''
			var imgTag = '<img class="' + imgLocator + '"src="' + image.id.image_url + '"/>';
			$(startField).find('div.form_image_div').append(imgTag);
		});
	}

	/**
	*
	* This will update the table options
	*
	*/
	editor_on_click(editor) {
		$('#' + this.table_id).on('click', 'tbody td:not(.child), tbody span.dtr-data', function (e) {
			// Ignore the Responsive control and checkbox columns
			if ($(this).hasClass('control') || $(this).hasClass('select-checkbox')) {
				return;
			}
			editor.inline(this, {
				submit: 'changed',
				onBlur: 'submit'
			});
			var fldType = $(this).find('.form-control').prop('tagName');
			if (fldType === "SELECT") {
				updateTableOptions(getDataTableInfo_Cell(this));
			}
		});
	}

	/**
	*
	* given the fields... use the storage_container to validate the value
	*
	*/
	validate_data(fields) {
		var errors = [];
		for (var i = 0; i < fields.length; i++) {
			var field = fields[i];
			var v = this.validate_field(field.name(), field.val());
			if (!v.valid) {
				var err = v.errors[0].error;
				if ($("div.DTED_Lightbox_Container").length == 0) {
					// means we are inline (I think)
					for (var e = 0; e < v.errors.length; e++) {
						var msg = v.errors[e];
						set_message(msg.error + " (" + msg.field + ")", 2000, true);
					}
				}

				errors.push({ name: field.name(), error: err });
			}
		}
		return errors;
	}
	
	/**
	 * given some data, (expected an object with keys as the field names)
	 * validate the data in each field!!
	 * @param {*} inputData 
	 */
	validate_form_data(inputData){
		var fieldNames = Object.keys(inputData);
		var fieldErrors = [];
		fieldNames.forEach(fieldName=>{
			var fieldErrResp = this.validate_field(fieldName,inputData[fieldName]);
			if (!fieldErrResp.valid){
				fieldErrResp.errors.forEach(error =>{
					fieldErrors.push(error);
				});
			}
		});
		return fieldErrors;
	}

	/**
	* validate one field
	*/
	validate_field(field_name, field_value) {
		var fldStorage = this.storage_container.field_storage[field_name];
		if (fldStorage !== undefined) {
			var sf = fldStorage.field;
			return sf.validate_datum(field_value);
		}
		return { valid: true, errors: [] };
	}

	/**
	*
	* Setup the data to be validated before submitting
	*/
	register_pre_submit() {
		this.dt_editor.on('preSubmit', function (e, o, a) {
			if (a !== 'remove') {
				var names = this.order();
				var vf = [];
				for (var f = 0; f < names.length; f++) {
					vf.push(this.field(names[f]));
				}
				var errors = this.dataTableCreator().validate_data(vf);
				if (errors.length > 0) {
					for (var e = 0; e < errors.length; e++) {
						this.field(errors[e].name).error(errors[e].error);
					}
					return false;
				}
			}
			return true;
		});
	}

	/**
	*  Duplicate Action
	*/
	duplicate_action(e, dt, node, config) {
		// Start in edit mode, and then change to create
		dt.editor().edit(dt.rows({ selected: true }).indexes(), {
			title: 'Duplicate record',
			buttons: 'Create from existing'
		})
			.mode('create');
	}

	/**
	*  View Action.. for the page view
	*/
	view_action(e, dt, node, config) {
		var vd = dt.data()[dt.rows({ selected: true })[0][0]];
		var dtc = dt.dataTableCreator();
		var fldName = dtc.parent_field_name;
		var table_id = dtc.table_id;

		var json_path = dtc.json_path;  // don't forget to register
		var root_id = vd['DT_RowId'];
		var parent_id = dtc.parent_id;  // don't forget to register this

		var parent_path = isEmpty(dtc.parent_path) ? pageData.path : dtc.parent_path

		var new_loc = "/page/" + parent_path + '/' + root_id;

		if (!isEmpty(parent_id)){
			// added 3/11/2020 to handle using the url path to forge our layers
			var root_path = dtc.storage_container.storage_name;
			new_loc = "/page/" + parent_path + '/' + root_path + '/' + root_id;
		}

		var path_args = { parent_id: parent_id};

		var page_info = get_or_default(pageReturn, "page", {});
		page_info['prev_page'].push(get_or_default(page_info, 'curr_page', {}));
		page_info['curr_page'] = { location: new_loc, args: path_args };
		pageReturn['page'] = page_info;
		$.get(new_loc, path_args).done(init_page);
	}

	/**
	*
	*  Calls the registered order_item callback
	*/
	order_action(e, dt, node, config) {
		var tblId = dt.dataTableCreator().table_id;
		pageReturn.tables[tblId].callbacks['order_item'](dt);
	}

	/**
	*
	*  Returns the buttons listed for use in the DataTable
	*  - Create
	*  - Edit
	*  - Duplicate
	*  -
	*/
	get_buttons(incl_buttons) {
		var create_btn = { extend: "create", editor: this.dt_editor };

		var edit_btn = { extend: "edit", editor: this.dt_editor };

		var dup_btn = {
			extend: "selected",
			text: 'Duplicate',
			action: this.duplicate_action
		};

		var del_btn = { extend: "remove", editor: this.dt_editor };

		var view_btn = {
			extend: 'selected',
			text: 'View',
			action: this.view_action
		};

		var order_btn = {
			extend: 'selected',
			text: "Edit Order",
			action: this.order_action
		};

		var invoice_btn = {
			extend: 'selected',
			text: "View Invoice",
			action: function (e, dt, node, config) {
				var tblId = dt.dataTableCreator().table_id;
				pageReturn.tables[tblId].callbacks['view_invoice'](dt);
			}
		};

		var view_cust_btn = {
			extend: 'selected',
			text: 'View Customer Item',
			action: function (e, dt, node, config) {
				//view_item(dt,'Cust_Item_Num');
				view_item_fe(dt, 'data_number_lookup');
			}
		};

		var view_plant_btn = {
			extend: 'selected',
			text: 'View Plant Item',
			action: function (e, dt, node, config) {
				view_item(dt, 'Item_Num');
			}
		};

		var avl_buttons = {
			create: create_btn,
			edit: edit_btn,
			duplicate: dup_btn,
			remove: del_btn,
			view: view_btn,
			order_item: order_btn,
			view_invoice: invoice_btn,
			cust_item: view_cust_btn,
			plant_item: view_plant_btn
		};

		var btn_list = [];

		var sel_btns = {};

		for (var i = 0; i < incl_buttons.length; i++) {
			var btnName = incl_buttons[i];
			sel_btns[btnName] = avl_buttons[incl_buttons[i]];
		}

		var proc_btns = getCustomEditorFormButtons(sel_btns, this);

		for (var i = 0; i < incl_buttons.length; i++) {
			var btnName = incl_buttons[i];
			btn_list.push(proc_btns[incl_buttons[i]]);
		}
		return btn_list;
	}

	/**
	*  Create the datatable options
	*/
	datatable_options(btns, inputData) {
		var dt_options = {
			responsive: true,
			dom: "Bfrtip",
			paging: btns.length > 1,
			searching: btns.length > 1,
			info: btns.length > 1,
			columns: this.dt_fields.editor_cols,
			order: [2, 'asc'],
			keys: {
				columns: this.dt_fields.key_columns,
				//keys: [ 9 ],
				editor: this.dt_editor,
				editOnFocus: true
			},
			select: {
				style: 'os',
				selector: 'td.select-checkbox'
			},
			buttons: btns
		};

		if (inputData === null) {
			dt_options['ajax'] = { url: this.dt_url, type: "GET" };
		} else {
			dt_options['data'] = inputData;
		}
		return dt_options;
	}

	/**
	*
	*  Create the standard datatable
	*
	*/
	create_dt(incl_buttons, inputData) {
		var btns = this.get_buttons(incl_buttons);
		if (inputData !== null && inputData !== undefined) {
			this.setup_image_data(inputData);
		}
		var dt_table = $('#' + this.table_id).DataTable(this.datatable_options(btns, inputData));
		this.set_dtc_entry(dt_table, true);
		dt_table.on('xhr.dt', function (e, settings, json, xhr) {
			var dtc = $(this).DataTable().dataTableCreator()
			dtc.setup_image_data(json.data);
		});
		return dt_table;
	}

	setup_image_data(inData) {
		var image_fields = jmespath.search(this.storage_container.fields, "[?field_type == 'image']");
		if (image_fields.length > 0) {
			for (var i = 0, ien = inData.length; i < ien; i++) {
				$.each(image_fields, function (idx, imgField) {
					if (Array.isArray(inData[i][imgField.field_name])) {
						// good
					} else {
						inData[i][imgField.field_name] = [inData[i][imgField.field_name]];
					}
				});
			}
		}
	}

	set_dtc_entry(dt_object, table) {
		var dtcList = get_or_default(dt_object, 'dataTableCreatorList', {});
		dtcList[this.table_id] = this;
		dt_object.constructor.prototype.dataTableCreatorList = dtcList;
		if (table) {
			dt_object.constructor.prototype.dataTableCreator = function () {
				var tableId = this.settings()[0].sTableId;
				return this.dataTableCreatorList[tableId];
			}
		} else {
			dt_object.constructor.prototype.dataTableCreator = function () {
				var tableId = this.s.table.slice(1);
				return this.dataTableCreatorList[tableId];
			}
		}

	}

	/**
	*
	*
	*  Function to call to create the editor and table datatables
	*
	*/
	create_edit_data_table_with_data(divId, incl_buttons, inData) {
		$.fn.dataTable.ext.errMode = 'throw';
		this.dt_fields = this.process_dt_fields();

		updateFieldOptions(this.name, this.dt_fields);

		this.create_html_table(divId);

		this.create_editor_dt();

		if (this.dt_fields.hide_cols.length > 0) {
			this.dt_editor.hide(this.dt_fields.hide_cols);
		}

		this.editor_on_click(this.dt_editor);

		this.register_pre_submit();

		this.dt_table = this.create_dt(incl_buttons, inData);

		this.finalize_load();

	}

	/**
   * Load this up into the pageReturn object
   *
   */
	finalize_load() {
		pageReturn['tables'][this.table_id] = this;
	}

}

class ItemStorage {
	constructor(id, storage_name, fields, isList) {
		this.id = id;
		this.storage_name = storage_name;
		this.fields = fields;
		this.isList = isList;
	}
}

/**
 * Load the json object of schema fields
 * @TODO Need to change this to use StorageContainer!!!
 * @param objFlds
 * @returns
 */
function get_storage_field(objFlds, storage_url) {
	var fields = [];

	for (var a = 0; a < objFlds.length; a++) {
		var objFld = objFlds[a];
		var fo = objFld.field_order;
		var ft = objFld.field_type;
		var fn = objFld.field_name;
		var fd = objFld.field_default;
		var fr = objFld.field_required;
		var _id = objFld.id;
		var isKey = objFld.is_key_field;
		var isOption = objFld.is_option_filled;
		var options = objFld.option_data;
		var category = objFld.category;
		var option_container = objFld.option_container;
		var desc = objFld.description;
		var gn = null;
		var repeated = null;
		var structure = null;
		if (ft == "group" && objFld.structure !== undefined) {
			gn = objFld.group_name;
			repeated = objFld.repeated;
			var sc = new StorageContainer(objFld.structure.id, objFld.group_name, get_storage_field(objFld.structure.fields, storage_url), repeated, storage_url, null, objFld.structure.extends);
			sc.set_field_name(fn);
			structure = sc;
			storage_items[objFld.group_name] = sc;
		} else {
			if (objFld.is_list) {
				repeated = true;
			} else {
				repeated = false;
			}
		}
		var sf = new StorageField(_id, fo, fn, gn, ft, fd, isKey, fr, isOption, options, repeated, category, structure, option_container, desc);
		fields.push(sf);
	}

	fields.sort(compareStorageFields);

	return fields;
}

function item_storage_group(field_group) {
	var id = field_group.structure.id;
	var storage_name = field_group.group_name;
	var fields = get_storage_field(field_group.structure.fields);
	var isList = field_group.structure.repeated;
	var is = new ItemStorage(id, storage_name, fields, isList);
	storage_items[storage_name] = is;
	return is;
}

/**
 * Expected properties of the object are
 * - storage_url (if blank datatable functionality won't work
 * - storage_name
 * - fields
 * - isList
 * - extends (currently nothing in StorageContainer for this)
 * @param jsonObj
 * @returns
 */
function create_container(jsonObj) {
	var id = jsonObj.id;
	var storage_url = jsonObj.storage_url;
	var storage_name = jsonObj.storage_name;
	var fields = get_storage_field(jsonObj.fields, storage_url);
	var sc = new StorageContainer(id, storage_name, fields, jsonObj.isList, storage_url, null, jsonObj.extends)
	storage_items[storage_name] = sc;
	return sc;
}

function process_container(page_container) {
	var schema_lookup = {};
	schema_lookup[page_container.storage_name] = container_fields(page_container);
	return schema_lookup;
}

function container_fields(strg_cntr) {
	var flds = strg_cntr.fields;
	var sl = {}
	for (var f = 0; f < flds.length; f++) {
		var fld = flds[f];
		fld.set_parent(strg_cntr);
		if (fld.isGroup() && fld.structure !== undefined && fld.structure !== null) {
			fld.structure.set_parent(strg_cntr);
			sl[fld.field_name] = container_fields(fld.structure);
		} else {
			sl[fld.field_name] = fld;
		}
	}
	return sl;
}

function load_json(storage_name, jsonObj) {
	var sc = storage_items[storage_name].create_copy();
	sc.load_object(jsonObj);
	return sc;
}

function item_storage_loadstr(jsonStr) {
	obj = JSON.parse(jsonStr);
	if (isNotNull(obj)) {
		return create_container(obj);
	}
	return null;
}
