var data_info = {};
var schemas = {};
var testJson;
var loadedData;
var postJson;
var pageEvents = {};
var pageData;
var pageSc;
var schemaLookup;
var pageReturn = {};
var pageOptions = {};
var optionCallBacks = {}
var openForm = null;

var imageInformation = {}
var imageReloads = [];

var modal_loading = {};
var modal_was_open = false;

$('#item_form').hide();

var typeAheadForm = `<div id="typeahead_container"><div id="typeahead-top"></div>
<form id="form-french_v1" name="form-french_v1">
    <div class="typeahead__container">
        <div class="typeahead__field">

            <span class="typeahead__query">
                <input class="js-typeahead-french_v1" name="french_v1[query]" type="search" placeholder="Search" autocomplete="off">
            </span>
            <span class="typeahead__button">
                <button class="btn btn-default">
                    <span class="glyphicon glyphicon-search" aria-hidden="true">Select</span>
                </button>
            </span>

        </div>
    </div>
</form>
</div></div>`

const jmespath_regex = /((\w*)\[\?\s*id\s*==\s*`(\w+|\{\{\w+\}\})`\])/g;
const jmespath_regex_p2 = /(\s*\|\s*\[0\])/g;

function add_loading(loading_key){
	if (Object.keys(modal_loading).length === 0){
		var htmlVals = '<div id="modal-loading-ind"><p><i class="fas fa-spinner fa-spin fa-3x"></i></p></div>';
		if (modal.modalOpen()){
			modal_was_open = true;
			$('div#modal > div#content').append(htmlVals);
			modal.addCallBack(cleanup_modal_loading);
		} else {
			modal.open({content: htmlVals},cleanup_modal_loading);
		}
	}
	modal_loading[loading_key] = true;
}

function cleanup_modal_loading(){
	modal_loading = {};
	modal_was_open = false;
}

function remove_loading(loading_key){
	delete modal_loading[loading_key];
	if (Object.keys(modal_loading).length === 0){
		if (modal_was_open){
			$('#modal-loading-ind').remove();
		}else{
			modal.close();
		}
	}
}

function getIdList(idString) {
	if (idString === "") {
		return [];
	}
	if (Array.isArray(idString)) {
		return idString;
	}
	var parts = idString.split("-");
	var idList = [];
	for (var i = 0; i < parts.length; i++) {
		idList.push(parseInt(parts[i]));
	}

	return idList;
}

function getIdString(idList) {
	var idString = "";
	for (var i = 0; i < idList.length; i++) {
		idString = idString + idList[i] + "-";
	}

	return idString.slice(0, idString.length - 1);
}

//$(window).resize(resize_div_sep);

function update_json_path(in_path) {
	var fnl_path = in_path;
	let m;
	while ((m = jmespath_regex.exec(in_path)) !== null) {
		if (m.index == jmespath_regex.lastIndex) {
			jmespath_regex.lastIndex++;
		}

		fnl_path = fnl_path.replace(m[1], m[2]);
	}
	var fnl_path2 = fnl_path;
	while ((m = jmespath_regex_p2.exec(fnl_path)) !== null) {
		if (m.index == jmespath_regex_p2.lastIndex) {
			jmespath_regex_p2++;
		}

		fnl_path2 = replace_all(fnl_path, m[1], '');
	}
	return fnl_path2;
}

function get_path_ids() {
	var jp = pageData.args.json_path;
	var ids = {}
	let m;
	while ((m = jmespath_regex.exec(jp)) !== null) {
		if (m.index == jmespath_regex.lastIndex) {
			jmespath_regex.lastIndex++;
		}
		ids[m[2] + "_id"] = m[3]
	}
	return ids;
}


function init_fields() {
	data_info = {};
	schemas = {};
	testJson;
	loadedData;
	postJson;
	pageEvents = {};
	pageData;
	pageSc;
	schemaLookup;
	pageReturn.page['dt_button'] = undefined;
}

var modal = (function () {
	var
		method = {},
		callback_on_close = [function () { console.log("closing"); }],
		isOpen = false,
		$overlay,
		$modal,
		$content,
		$close;

	// Center the modal in the viewport
	method.center = function () {
		var top, left;

		top = Math.max($(window).height() - $modal.outerHeight(), 0) / 2;
		left = Math.max($(window).width() - $modal.outerWidth(), 0) / 2;

		$modal.css({
			top: top + $(window).scrollTop(),
			left: left + $(window).scrollLeft()
		});
	};

	method.modalOpen = function(){
		return isOpen;	
	};

	method.addCallBack = function(addedCallback){
		callback_on_close.push(addedCallback);
	}

	// Open the modal
	method.open = function (settings, close_callback) {
		isOpen = true;
		if (isNotNull(close_callback)) {
			callback_on_close = [close_callback];
		}

		$content.empty().append(settings.content);

		$modal.css({
			width: settings.width || 'auto',
			height: settings.height || 'auto',
			zIndex: 15
		});

		method.center();
		$(window).bind('resize.modal', method.center);
		$modal.show();
		$overlay.show();
	};

	// Close the modal
	method.close = function () {
		isOpen = false;
		callback_on_close.forEach(callback => {
			callback();
		});
		$modal.hide();
		$overlay.hide();
		$content.empty();
		$(window).unbind('resize.modal');
	};

	// Money formatter
	// FROM: http://jsfiddle.net/hAfMM/435/
	Number.prototype.format = function (n, x) {
		var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
		return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
	};

	// Generate the HTML and add it to the document
	$overlay = $('<div id="overlay"></div>');
	$modal = $('<div id="modal"></div>');
	$content = $('<div id="content"></div>');
	$close = $('<a id="close" href="#">close</a>');

	$modal.hide();
	$overlay.hide();
	$modal.append($content, $close);

	$(document).ready(function () {
		$('body').append($overlay, $modal);
	});

	$close.click(function (e) {
		e.preventDefault();
		method.close();
	});

	return method;
}());

function get_data_tbl_data(table_id) {
	var cells = $(".modal-table-value");
	var values = [];
	for (var i = 0; i < cells.length; i++) {
		values.push($(cells.get(i)).text());
	}
	return values;
}

function clean_value(value, fieldName) {
	if (value.startsWith("[")) {
		value = value.slice(1);
	}
	if (value.endsWith("]")) {
		value = value.slice(0, value.length - 1);
	}
	var value_arr = value.split(",");
	var dArr = [];
	for (var i = 0; i < value_arr.length; i++) {
		var entry = {};
		entry[fieldName] = value_arr[i];
		dArr.push(entry);
	}
	return dArr;
}

function getUrlVars(inUrl) {
	var vars = {};
	var parts = inUrl.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
		vars[key] = value;
	});
	return vars;
}

function create_table_from_input(input_field, field_name) {
	var value = input_field.val();

	//$('div.DTED.DTED_Lightbox_Wrapper').css('z-index',-1)
	$('.modal').hide();
	$('.modal-backdrop').hide();

	create_modal_table(clean_value(value, field_name), function () {
		var values = get_data_tbl_data("modal_table");
		values = values.toString();
		input_field.val("[" + values + "]")
		// $('div.DTED.DTED_Lightbox_Wrapper').css('z-index',11)
		$('.modal-backdrop').show();
		$('.modal').show();
	});
}

function create_table_from_inline(modifier, tblId, field_name) {
	var value = pageReturn.tables[tblId].dt_table.cell(modifier.row, modifier.column).data();
	create_modal_table(clean_value(value, field_name), function () {
		var values = get_data_tbl_data("modal_table");
		values = values.toString();
		//pageReturn.tables[tblId].dt_table.cell(modifier.row,modifier.column).data("["+values+"]").draw();
		pageReturn.tables[tblId].dt_editor.edit(modifier.row, false).set(field_name, "[" + values + "]").submit();
	});

}

var create_table_arr = function (colName, inArr) {
	var dArr = [];
	for (var i = 0; i < inArr.length; i++) {
		var entry = {};
		entry[colName] = inArr[i];
		dArr.push(entry);
	}
	create_modal_table(dArr);

}

var create_table = function (inArray) {
	var detached = d3.select(document.createElement("div"));
	detached.attr("id", "modal_div_content");

	detached.append("div")
		.attr("id", "modal-form-div")
		.append("input")
		.attr("id", "modal-form-entry")
		.attr("type", "text")
		.attr("class", "form-control")
		.attr("placeholder", "New Entry...");
	detached.append("span")
		.attr("class", "input-group-btn")
		.append("button")
		.attr("id", "modal-form-button")
		.attr("class", "btn btn-secondary")
		.attr("type", "button")
		.text("Add Entry");
	detached.append("p");  // create more spacing

	var table = detached
		.append("div")
		.attr("class", "table-responsive")
		.append("table");
	table.attr("class", "table table-striped table-bordered table-hover table-condensed")
	table.attr("id", "modal_table");

	table.append("thead")
		.attr("class", 'thead-dark')
		.append("tr")
		.selectAll("th")
		.data(Object.keys(inArray[0]))
		.enter().append("th")
		.text(function (d) { return d; });

	var tbody = table.append("tbody");
	for (var i = 0; i < inArray.length; i++) {
		var d = inArray[i];
		var tr = tbody.append("tr")
			.attr("id", "modal-row-" + (i));
		var vals = d3.values(d);
		vals.forEach(function (d2) {
			tr.append("td")
				.attr("class", "modal-table-value")
				.text(d2);
		});
		//<a id="close" href="#">close</a>
		tr.append("td")
			.append("a")
			.attr("id", "delete-" + (i))
			.attr("class", "modal-row-delete")
			.attr("href", "#")
			.text("delete")
	}

	return detached;
}

var create_modal_table = function (inArray, callback) {
	var innerHtml = create_table(inArray).html();
	var fldName = Object.keys(inArray[0])[0];
	var callback_dt = function (in_data) {
		var updated_data = add_model_name(in_data, fldName);
		var dtData = $('#modal_table').DataTable().rows().data();
		var all_data = [];
		for (var i = 0; i < dtData.length; i++) {
			all_data.push(dtData[i]);
		}
		updated_data['all_data'] = JSON.stringify(all_data);
		return updated_data;
	}

	modal.open({ content: innerHtml }, callback);
	$(".modal-row-delete").off("click", delete_modal_row);
	$(".modal-row-delete").on("click", delete_modal_row);
	var button = document.getElementById('modal-form-button');
	button.onclick = add_form_entry;
	modal.center();
}

function add_form_entry() {
	var value = $('#modal-form-entry').val();
	if (value.trim() !== "") {
		add_entry(value);
		$('#modal-form-entry').val("");
	}
}

function add_entry(value) {
	var detached = d3.select(document.createElement("div"));

	var rows = $('#modal_table > tbody').children();
	var lastRow = $(rows[rows.length - 1]);
	var lastRowId = lastRow.attr("id");
	var i = 0;
	if (lastRowId !== undefined) {
		i = Number(lastRowId.slice(lastRowId.search("-") + 1)) + 1;
	}

	var tr = detached.append("tr")
		.attr("id", "modal-row-" + (i));

	tr.append("td")
		.attr("class", "modal-table-value")
		.text(value);

	tr.append("td")
		.append("a")
		.attr("id", "delete-" + (i))
		.attr("class", "modal-row-delete")
		.attr("href", "#")
		.text("delete");

	$('#modal_table > tbody:last-child').append(detached.html());
	$(".modal-row-delete").off("click", delete_modal_row);
	$(".modal-row-delete").on("click", delete_modal_row);
}

function delete_modal_row() {
	var delId = $(this).attr("id");
	var rowNum = delId.slice(delId.search("-") + 1);
	$("#modal-row-" + rowNum).remove();
}


var buttons = {
	'view': '<button class="btn btn_view">View</button>',
	'edit': '<button class="btn btn_edit">Edit</button>'
}

function ajax(options) {
	return new Promise(function (resolve, reject) {
		$.ajax(options).done(resolve).fail(reject);
	});
}

function adjust_data(update_fields) {
	// do nothing for now
	console.log(update_fields);
	var schema = schemas[update_fields.schema_name];

	return update_fields;
}
function createLabel(field_name) {
	var parts = field_name.split("_");
	var final_str = capitalizeFirstLetter(parts[0]);
	for (var i = 1; i < parts.length; i++) {
		final_str = final_str + " " + capitalizeFirstLetter(parts[i]);
	}
	return final_str;
}

function capitalizeFirstLetter(string) {
	return string.charAt(0).toUpperCase() + string.slice(1);
}

function callDtSubmit(dt, actionName, modelName, rawData, passFunctions, optionData) {
	var url = dt.ajax.url();
	return callDtSubmitUrl(dt, url, actionName, modelName, rawData, passFunctions, optionData);
}

function callDtSubmitUrl(dt, url, actionName, modelName, rawData, passFunctions, optionData) {
	var convData = convert_data(actionName, modelName, rawData, optionData);
	if (passFunctions !== undefined && passFunctions.before !== undefined) {
		passFunctions.before();
	}

	var jqxhr = $.post(url, convData, function () {
		dt.ajax.reload(function (json) {
			console.log('table reloaded');
			if (passFunctions !== undefined && passFunctions.after !== undefined) {
				passFunctions.after();
			}
			return json;
		});
	});


	return new Promise(function (resolve, reject) { jqxhr.done(resolve).fail(reject); });
}

function convert_data(actionName, modelName, jsonObj, optionData) {
	var newObj = { action: actionName, data: [] };
	var optData = null;
	if (optionData !== undefined) {
		optData = optionData;
	}
	for (var i = 0; i < jsonObj.length; i++) {
		var item = { id: "", model_name: "", data: {}, option_data: optData };
		var inItem = jsonObj[i];
		item.id = inItem['DT_RowId'];
		delete inItem['DT_RowId'];
		item.model_name = modelName;
		var keys = Object.keys(inItem);
		for (var a = 0; a < keys.length; a++) {
			var key = keys[a];
			item.data[key] = inItem[key];
		}
		newObj.data.push(item);
	}
	return { 'update': JSON.stringify(newObj) };;
}

function add_model_name(data, model_name) {
	var update = { action: data.action, data: [] };
	return add_model_name_core(data, model_name, update);
}

function add_model_name_schema(data, model_name, par_id) {
	var update = { action: data.action, data: [], parent_id: par_id };
	return add_model_name_core(data, model_name, update);
}

function add_model_name_core(data, model_name, update) {
	var keys = Object.keys(data.data);
	var dtc = $('#' + model_name + "-table").DataTable().dataTableCreator();
	var models = {};
	for (var k = 0; k < keys.length; k++) {
		// first level should be the id
		// this code will remove the necessity to do this on the server
		var aKey = keys[k];
		var model = get_or_default(models, aKey, {});
		model['id'] = aKey;
		model['model_name'] = model_name;
		var mKeys = Object.keys(data.data[aKey]);
		var flds = {};
		for (var m = 0; m < mKeys.length; m++) {
			if (mKeys[m] !== 'action') {
				flds[mKeys[m]] = data.data[aKey][mKeys[m]];
			}
		}
		model['data'] = flds;
		if (isNotNull(pageReturn.tables)) {
			var opFlds = dtc.option_fields;
			var opts = get_or_default(opFlds, model.id, null);
			model['option_data'] = "none";
			if (isNotNull(opts)) {
				model['option_data'] = opts;
			}
		}

		models[aKey] = model;
	}
	var ms = Object.keys(models);
	for (var i = 0; i < ms.length; i++) {
		var m = models[ms[i]];
		update.data.push(m);
	}
	return { 'update': JSON.stringify(update) };
}


function setPageHeader(storageContainer) {
	$('div#headingMain').append('<h1>' + createLabel(storageContainer.storage_name) + '</h1>');
	$('div#headingMsg').append("<p>Click the \"View\" Button to update items related to that " + storageContainer.storage_name + " object.</p>");
}

function setDisplayPageHeader(storageContainer, divId, level, item_id) {
	if (divId === 'page_header') {
		$('div#headingMain').append('<h' + level + '>' + createLabel(storageContainer.storage_name) + ": " + item_id + '</h' + level + '>');
		$('div#headingMsg').append("<p></p>");
	}
	else {
		d3.select('#' + divId)
			.append("h" + level).text(createLabel(storageContainer.storage_name) + ": " + item_id);
		d3.select('#' + divId)
			.append("p");
	}
}

/**
*
* Register an event to go fetch option data
*/
function get_option_data(strgFld) {
	return get_option_data_callback(strgFld, load_options);
}

function parse_option_container(field){
	var jPath = '';
	if (field.option_container.startsWith('options')){
		jPath = field.option_container.split(".")[0].replace('option_name','name');
	} else {
		var parts = field.option_container.split(":");
		jPath = (parts[1]+parts[2]).replace('item_type','name');
	}
	return jPath;
}

function get_option_data_callback(strgFld, callbackFunc) {
	var optFld = strgFld.parent.storage_name + "." + strgFld.field_name;
	var optCont = strgFld.option_container;
	var jPath = parse_option_container(strgFld);
	
	var select_options = jmespath.search(pageOptions,jPath);

	if (isNull(select_options)) {
		var option_type = jPath.startsWith("options") ? "options":"recipe_costing";
		var optionUrl = "/get_options_data/"+option_type
		//$('#text_update_spinner').show();
		if (isNull(get_or_default(optionCallBacks,optionUrl,undefined)))
		{
			add_loading(optionUrl);
			var callBacksObj = get_or_default(optionCallBacks,optionUrl,{});
			var callBackInfo = get_or_default(callBacksObj,callbackFunc.name,{func: callbackFunc, params: []});
			callBackInfo.params.push(strgFld);
			callBacksObj[callbackFunc.name] = callBackInfo;
			optionCallBacks[optionUrl] = callBacksObj;
			$.getJSON(optionUrl, function (resp) {
				Object.assign(pageOptions,resp.data);
				remove_loading(optionUrl);
				process_options_callbacks(optionUrl);
				//callbackFunc(strgFld);
			}).fail(function (resp) {
				remove_loading(optionUrl);
				console.log("failed");
				console.log(resp);
			});
		} else {
			var callBacksObj = get_or_default(optionCallBacks,optionUrl,{});
			var callBackInfo = get_or_default(callBacksObj,callbackFunc.name,{func: callbackFunc, params: []});
			callBackInfo.params.push(strgFld);
			callBacksObj[callbackFunc.name] = callBackInfo;
			optionCallBacks[optionUrl] = callBacksObj;
		}
	} else {
		return select_options;
	}
	return [];
}

/*
* TODO:  Test out this wait functionality... this was causing options to be called SOOO many times.. this will help
*/
function process_options_callbacks(inputUrl){
	var callBacksObj = optionCallBacks[inputUrl];
	var funcs = Object.keys(callBacksObj);
	funcs.forEach(aFuncName => {
		var params = callBacksObj[aFuncName].params;
		params.forEach(param => {
			callBacksObj[aFuncName].func(param);
		});
	});
	optionCallBacks[inputUrl] = undefined;
}

function load_options(strgFld) {
	var jPath = parse_option_container(strgFld);
	var select_options = jmespath.search(pageOptions,jPath);
	var tblId = strgFld.parent.storage_name + "-table";
	var dt_editor = $('#' + tblId).DataTable().dataTableCreator().dt_editor;
	dt_editor.field(strgFld.field_name).update(select_options);
}

/*
 *  Updated on 3.23.20 now the image data is stored with the items
 *
 *
 */ 
function imageDisplay(image_value) {
	if (image_value === 'Add Image Option') {
		return "<div></div>";
	}
	if (image_value !== undefined) {
		var imageArray = [];
		if (Array.isArray(image_value)) {
			imageArray = image_value;
		} else {
			imageArray.push(image_value);
		}

		var respHtml = ['<div>'];
		$.each(imageArray, function (idx, image) {
			if (image === 'Add Image Option' || image === 'undefined' || image === undefined || image === "") {
				respHtml.push('');
			} else {
				var imageHtml = '<img class="dt_image" src="'+image.image_url+'">'
				respHtml.push('<div id="' + image.image_id + '">' + imageHtml+ '</div>');
			}
		});
		respHtml.push('</div>');
		return respHtml.join('');
	} else {
		return null;
	}
}

function imageHtml(image_url) {
	return '<img class="dt_image" src="' + image_url + '"/>';
}

function updateTableOptions(tblInfo) {
	//var name = tblInfo.info.parent_field_name;
	var name = tblInfo.field_storage.parent.storage_name;
	var options = pageOptions[name + "." + tblInfo.field_storage.field_name];
	var sel_option = null;
	for (var i = 0; i < options.length; i++) {
		if (options[i].label === tblInfo.selected) {
			sel_option = options[i].json_path;
			break;
		}
	}
	var upObj = {};
	upObj[name + "_id"] = tblInfo.row.DT_RowId;
	var json_path = update_vals(tblInfo.field_storage.get_json_path(false), upObj);
	json_path = pageData.args.json_path + "." + json_path;

	// want to check if there is a duplicate
	json_path = json_path.replace(name + "." + name, name);
	var optFlds = get_or_default(tblInfo.info.option_fields, tblInfo.row.DT_RowId, {});
	optFlds[tblInfo.field_storage.field_name] = { dest_path: json_path, src_path: sel_option };
	tblInfo.info.option_fields[tblInfo.row.DT_RowId] = optFlds;
}

function getDataTableInfo_New(tblId, selection, fld) {
	var tblName = get_table_id2(tblId);
	var tbl = pageReturn.tables[tblId].dt_table;
	var fields = jmespath.search(schemaLookup, update_json_path(pageReturn.tables[tblId].json_path))[fld];
	var selected = selection;
	var row = { DT_RowId: 0 }
	return { dt_table: tbl, row: row, field_storage: fields, selected: selected, info: pageReturn.tables[tblId] }
}

function getDataTableInfo_Row(aNode, selection, fld) {
	var tblId = $(aNode).closest('table').attr('id');
	var tblName = get_table_id2(tblId);
	var tbl = pageReturn.tables[tblId].dt_table;
	var fields = jmespath.search(schemaLookup, update_json_path(pageReturn.tables[tblId].json_path))[fld];
	var selected = selection;
	var row = tbl.rows($(aNode).closest('tr')).data()[0];
	return { dt_table: tbl, row: row, field_storage: fields, selected: selected, info: pageReturn.tables[tblId] }
}

function getDataTableInfo_Cell(aNode) {
	var tblId = $(aNode).closest('table').attr('id');
	var tblName = get_table_id2(tblId);
	var tbl = $('#' + tblId).DataTable();
	//var tbl = pageReturn.tables[tblId].dt_table;
	var fldName = $(aNode).find('.form-control').attr('id').replace('DTE_Field_', '');
	var fldStorage = tbl.dataTableCreator().storage_container.getFieldStorageByName(fldName);
	var selected = null;
	var fldType = $(aNode).find('.form-control').prop('tagName');
	if (fldType == "SELECT") {
		selected = $(aNode).find('.form-control').find(':selected').text();
	}
	var row = tbl.rows($(aNode).closest('tr')).data()[0];
	if (row === undefined) {
		// could be in responsive
		row = tbl.rows($(aNode).closest('li').attr('data-dt-row')).data()[0];
	}
	return { dt_table: tbl, row: row, field_storage: fldStorage, selected: selected, info: tbl.dataTableCreator() }
}

function register_create_event(edtr, edtr_tbl) {
	edtr.on("open", function (e, type) {
		var modifier = edtr.modifier();
		var tblId = edtr_tbl.table().node().id;

		if (modifier && type == "inline") {
			var colNum = 0;
			if (modifier.column === undefined) {
				colNum = pageReturn.tables[tblId].dt_table.columns(modifier).flatten()[0];
			} else {
				colNum = modifier.column;
			}
			var cellName = pageReturn.tables[tblId].columns[colNum].data;
			var tblInfo = pageReturn.tables[tblId];
			var fields = jmespath.search(schemaLookup, update_json_path(tblInfo.json_path));
			var field = fields[cellName];
			if (field.isList()) {
				create_table_from_inline(modifier, tblId, cellName);
			}
		}
	});
}

function dt_create_click() {
	var inner_button_action = pageReturn.page.dt_button;
	var tblInfo = pageReturn.tables[inner_button_action.get_table_id()];
	var fields = jmespath.search(schemaLookup, update_json_path(tblInfo.json_path));
	var input = $($(this).children()[0]);
	var input_field = input.attr('id').replace("DTE_Field_", "");
	var field = fields[input_field];
	if (field.isList()) {

		create_table_from_input(input, input_field);
	}
	if (field.isOptionFilled()) {
		var aNode = $('#' + inner_button_action.get_table_id()).find('tr.selected');
		if (aNode.length === 0 || inner_button_action.button_type === 'create') {
			// means this is a new element
			updateTableOptions(getDataTableInfo_New(inner_button_action.get_table_id(), $(this).find(':selected').text(), input_field));
		} else {
			updateTableOptions(getDataTableInfo_Row(aNode, $(this).find(':selected').text(), input_field));
		}

	}
}

function view_item(dataTable, fldName) {
	var dataRow = dataTable.rows({ selected: true }).data()[0]

	var item_num = dataRow[fldName]
	var getUrl = '/show_item/' + item_num;
	var windowName = "Item: " + item_num;
	var newwindow = window.open(getUrl, windowName, 'height=1015,width=1400');
	if (window.focus) { newwindow.focus() }
	return false;
}

function view_item_fe(dataTable, fldName) {
	var dataRow = dataTable.rows({ selected: true }).data()[0];
	var item_num = dataRow[fldName];
	var api_url = pageData.env.APP_URL;
	var api_token = pageData.env.API_TOKEN;
	var getUrl = pageData.env.FE_URL + '/plants/' + item_num + '?api=' + api_url + '&token=' + api_token;
	var windowName = "Item: " + item_num;
	var newwindow = window.open(getUrl, windowName, 'height=1015,width=1400');
	if (window.focus) { newwindow.focus() }
	return false;
}

function process_page_data(page_data) {
	pageData = page_data;
	pageSc = create_container(page_data.schema);
	schemaLookup = process_container(pageSc);
	if (page_data.args.page_type == "TypeListing") {
		return process_page_listing(page_data, pageSc);
	} else {
		return process_page_display(page_data, pageSc);
	}
}

function process_page_display(page_data, page_container) {
	var path_parts = page_data.path.split('/');
	var root_name = path_parts[0];
	var root_id = path_parts.slice(-1)[0];
	var model_name = page_data.args.model_name;
	var parent_id = page_data.args.parent_id;

	setDisplayPageHeader(page_container, "page_header", 1, root_name);
	var prevPageArr = get_or_default(get_or_default(pageReturn, "page", {}), "prev_page", []);
	if (prevPageArr.length >= 1) {
		show_back_button();
	}

	var displayContainer = storage_items[root_name];
	if (root_name !== model_name) {
		displayContainer = storage_items[model_name];
	}

	var resp = [];
	var mainDiv = d3.select('#page_display')
		.append("div")
		.attr("id", "main_data");

	var groups = jmespath.search(displayContainer, "fields[?field_type == 'group']")

	if (groups.length > 0) {
		var tabDiv = d3.select('#page_display')
			.append("div")
			.attr("class", "container")
			.attr("id", 'tabbed_data');

		var tabHdr = model_name + " information";

		tabDiv.append('h2').text(tabHdr);
		tabDiv.append('ul')
			.attr('class', 'nav nav-tabs')
			.attr('id', model_name + "-tab")
			.attr('role', 'tablist')
			.selectAll('li')
			.data(groups).enter()
			.append('li')
			.attr('class', 'nav-item')
			.append("a")
			.attr("data-toggle", "tab")
			.attr('class', 'nav-link')
			.attr('id', function (d) { return d.field_name + "-tabber"; })
			.attr('role', 'tab')
			.attr("href", function (d) { return '#' + d.field_name + "-tabdiv" })
			.append('h4')
			.text(function (d) { return createLabel(d.field_name) });

		var tabContent = tabDiv.append("div")
			.attr("class", "tab-content");

		for (var i = 0; i < groups.length; i++) {
			var divId = groups[i].field_name + "-tabdiv";
			var divClz = "tab-pane fade";
			if (i == 0) {
				divClz = "tab-pane fade in active"
			}
			var newDiv = tabContent.append("div")
				.attr("class", divClz)
				.attr("id", divId);

			newDiv.append("p").text("Update elements here");

			var sc = groups[i].structure;
			var dtc = new DataTableCreator(root_id, sc, undefined, page_data.path);
			dtc.create_edit_data_table(divId, ['create', 'edit', 'duplicate', 'remove', 'view'])
			resp.push(dtc);
		}

		$('#' + model_name + "-tab" + ' li:first-child a').tab('show')
	}

	var dtc = new DataTableCreator('', page_container, !isEmpty(parent_id) ? parent_id: root_id,root_name);
	dtc.create_edit_data_table("main_data", []);
	resp.push(dtc);

	// ---------
	// not sure what this is doing
	// ---------
	//
	if (displayContainer.storage_name != page_container.storage_name) {
		setDisplayPageHeader(displayContainer, "main_data", 2, root_id);;
		var dtc = new DataTableCreator(parent_id, displayContainer, root_id, page_data.args.parent_path);
		dtc.create_edit_data_table("main_data", []);
		resp.push(dtc);
		}

	return resp;

}

// return the model name for the table
function get_table_id(dt) {
	var full_id = dt.table().node().id;
	return get_table_id2(full_id);
}

function get_table_id2(full_id) {
	return full_id.substring(0, full_id.length - "-table".length)
}

function process_view_field(dt, page_data) {
	var vd = dt.data()[dt.rows({ selected: true })[0][0]];
	var tblIdFull = dt.table().node().id;
	var table_id = get_table_id2(tblIdFull);

	var json_path = get_or_default(pageReturn.tables[tblIdFull], 'json_path', page_data.args.json_path);
	var root_id = vd['DT_RowId'];

	var upObj = {};
	upObj[table_id + "_id"] = root_id;
	json_path = update_vals(json_path, upObj);

	var new_loc = "/page/" + pageData.path;
	var path_args = { parent_id: parent_id };

	path_args['json_path'] = json_path;
	//window.location = new_loc+path_args;
	var page_info = get_or_default(pageReturn, "page", {});
	page_info['prev_page'].push(get_or_default(page_info, 'curr_page', {}));
	page_info['curr_page'] = { location: new_loc, args: path_args };
	pageReturn['page'] = page_info;
	$.get(new_loc, path_args).done(init_page);
}

function reset_header() {
	var pgHeadContent = '<div class="container"><div class="row"><div class="col-xs" id="headingButton"></div><div class="col-lg" id="headingMain"></div></div><div class="row"><div class="col" id="headingMsg"><p></p></div></div></div>';
	$('#page_header').empty();
	$('#page_header').append(pgHeadContent);
}

function init_page(page_data) {
	load_page_options();
	reset_header();

	$('#page_display').empty();

	pageReturn.page.curr_page['page_data'] = page_data;
	pageReturn['tables'] = {};
	init_fields();
	process_page_data(page_data);
	$('button.btn').on('click', function () {
		$('button.close').css('z-index', 50);
	});
}

function load_page_options(){
	var option_types = ['options','recipe_costing'];
	$.each(option_types, function (idx,option_type){
		if (isNull(pageOptions[option_type])){
			var load_key = "call_options__"+option_type;
		    add_loading(load_key);
		    $.getJSON('/get_options_data/'+option_type,function (resp){
			    Object.assign(pageOptions,resp.data);
			    remove_loading(load_key);
		    }).fail(function (resp){
				console.log('option call failed for: '+option_type); 
				console.log(resp);});
		}
	});
}

function show_back_button() {
	$('div#headingButton').append('<button type="button" class="btn btn-primary" id="backButton">Back</button>');
	$('button#backButton').unbind();
	$('button#backButton').click(function (e) {
		var page_info = get_or_default(pageReturn, "page", {});
		var prevPageArr = get_or_default(page_info, "prev_page", []);
		if (prevPageArr.length >= 1) {
			if (prevPageArr.length === 1) {
				location.reload();
			} else {
				var prevPage = prevPageArr.pop();
				page_info['curr_page'] = { location: prevPage.location, args: prevPage.args };
				page_info['prev_page'] = prevPageArr;
				pageReturn['page'] = page_info;
				$.get(prevPage.location, prevPage.args).done(init_page);
			}
		}
	});
}

function add_form_validation() {
	// Fetch all the forms we want to apply custom Bootstrap validation styles to
	var forms = document.getElementsByClassName('needs-validation');
	// Loop over them and prevent submission
	var validation = Array.prototype.filter.call(forms, function (form) {
		form.addEventListener('submit', function (event) {
			if (form.checkValidity() === false) {
				event.preventDefault();
				event.stopPropagation();
			}
			form.classList.add('was-validated');
		}, false);
	});
}


function process_page_listing(page_data, page_container) {
	var json_pathRoot = page_container.get_json_path(true);
	page_container.storage_url = page_container.storage_url + "?model_name=" + page_data.args.model_name;
	setPageHeader(page_container);
	var prevPageArr = get_or_default(get_or_default(pageReturn, "page", {}), "prev_page", []);
	if (prevPageArr.length >= 1) {
		show_back_button();
	}
	var resp = [];
	var dtc = new DataTableCreator(page_data.args['parent_id'], page_container, undefined, page_data.args.parent_path);
	dtc.create_edit_data_table("page_display", ['create', 'edit', 'duplicate', 'remove', 'view']);
	return resp;
}

function validate_data(storage_name, fields) {
	var sc = storage_items[storage_name];
	var errors = [];
	for (var i = 0; i < fields.length; i++) {
		var field = fields[i];
		var v = validate_field(sc, field.name(), field.val());
		if (!v.valid) {
			var err = v.errors[0].error;
			if ($("div.DTED_Lightbox_Container").length == 0) {
				// means we are inline (I think)
				for (var e = 0; e < v.errors.length; e++) {
					var msg = v.errors[e];
					set_message(msg.error + " (" + msg.field + ")", 2000, true);
				}
			}

			errors.push({ name: field.name(), error: err });
		}
	}
	return errors;
}

function validate_field(storage_container, field_name, field_value) {
	var sf = storage_container.field_storage[field_name].field;
	return sf.validate_datum(field_value);
}

function get_request_args() {
	var s1 = location.search.substring(1, location.search.length).split('&'),
		r = {}, s2, i;
	for (i = 0; i < s1.length; i += 1) {
		s2 = s1[i].split('=');
		r[decodeURIComponent(s2[0]).toLowerCase()] = decodeURIComponent(s2[1]);
	}
	return r;
};

/**
*
*  Get the recipe cost for an item
*   call /calculate_recipe_cost_item/<item_nums>
*
*/
function getRecipeCost(dataTableCreator, dtRowId, costCallBack) {
	var al = dataTableCreator.ancestry_list.slice();
	al.push(dtRowId);
	var idString = getIdString(al);
	$.get('/calculate_recipe_cost_item/' + idString, function (data) {
		costCallBack(data);
	});
}

/**
*  With the editor open... get all of the fields and data and get a cost
*/
function getEditorItemCost(dataTableCreator, costCallBack) {
	var flds = dataTableCreator.dt_editor.fields();
	var data = {};
	$.each(flds, function (idx, key) {
		var value = dataTableCreator.dt_editor.field(key).val();
		if (Array.isArray(value)) {
			value = value.join(",")
		}
		data[key] = value;
	});
	postRecipeCost(dataTableCreator, data, costCallBack);
}
/**
* Post some data to the recipe calculator
*
*/
function postRecipeCost(dataTableCreator, data, costCallBack) {
	$.post('/calculate_recipe_cost/' + dataTableCreator.name, data, 'json')
		.done(costCallBack);
}

/**
 * GET/STORE Image Information... stored here: imageInformation and then if not there... get it from here: /upload_image as a GET request
 *
 * Just send the imageNumber
 * and receive:  fileName and web_path (or url)..
 *
 */

function getImageInformation(imgNumber) {
	var imgInfo = imageInformation[imgNumber];
	if (imgInfo === undefined) {
		getSingleImage(imgNumber);
	}
	return imgInfo;
}

function chkImageLoadCustom(callback) {
	if (Object.keys(imageInformation).length === 0) {
		//callForImages(callback);
		// removed as of 6/24/2019
	}
}

function chkImageLoad() {
	if (Object.keys(imageInformation).length === 0) {
		//callForImages(imageCallbackLoad);
		//removed as of 6/24/2019
	}
	// want to load page options... so using this to do that
	load_page_options();
}

function imageCallbackLoad(data) {
	imageInformation = data['files'];
	console.log("Image Load was performed.");
	while (imageReloads.length > 0) {
		var imgNum = imageReloads.pop();
		var imgInfo = imageInformation[imgNum];
		if (imgInfo !== undefined) {
			$('div#' + imgNum).empty();
			var imgHtml = imageHtml(imgInfo.web_path);
			$('div#' + imgNum).append(imgHtml);
		}

	}
}

function getSingleImage(image_name) {
	if (isNotNull(image_name) && image_name !== "") {
		$.get("/get_image/" + image_name, function (json) {
			$('div#' + image_name).empty();
			if (json.status === 'success') {
				imageInformation[image_name] = json.data.data;
				var imgHtml = imageHtml(json.data.data.image_url);
				$('div#' + image_name).append(imgHtml);
			} else {
				$('div#' + image_name).append("<div>Error!</div>");
				console.log("Error retreiving image: " + image_name);
			}
		});
	} else {
		console.log("The image_name value is empty");
	}
}

function callForImages(callback) {
	$.get("/upload_image", callback);
}
/**
 *
 * EXAMPLE ITEM: { disabled: false, element: option#cust_select_1, id: "Choose...", selected: true, text: "Choose...", title: "" }
 *
 * If there is a "Choose..." element, make sure that is always first... then the "create new" is at the end
 *
 * @param items
 * @returns sorted items (alphabetically)
 */
function dropDownSort(items) {
	var begArray = [];
	var endArray = [];
	var innerArray = [];

	for (var i = 0; i < items.length; i++) {
		if (items[i].text.startsWith('Choose')) {
			begArray.push(items[i]);
		} else {
			if (items[i].text.startsWith('create new')) {
				endArray.push(items[i]);
			} else {
				innerArray.push(items[i]);
			}
		}
	}

	var b = innerArray.sort(function (a, b) {
		if (a.text < b.text) {
			return -1;
		} else {
			if (a.text > b.text) {
				return 1;
			} else {
				return 0;
			}
		}
	});
	var c = b.concat(endArray);

	return begArray.concat(c);
}