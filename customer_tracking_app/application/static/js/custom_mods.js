/**
 *  This is for custom code that will be written to add functionality to DataTables
 *  The top will be a function that will be called and passed in to get functions for an event type
 *   Pass in the table name i.e. customer, the datatable type (dt or edit) and the event... and what you will get
 *    back will be a list of functions to execute and their callbacks (if required)
 *    all functions will be here in this script
 */

 /**
  * HEre is the global variables to be used
  */
var current_plant_item = null;
var showCustomForm = "plant_img";
var plant_item_dtc = null;
var plant_item_plantField = null;

/**
 * Add additional fields to display on the edit form
 */
function getAdditionalEditorFields(storageName) {
  // removed on 4/14/2019 and added to plant_item
  //if (storageName === 'cust_plant_item' || storageName === 'plant_item' || storageName === 'item_order'){
  //    return [{label: 'Recipe Cost', name: 'recipe_cost', type:'readonly'}];
  //}
  return [];
}

/**
* Give the option to update any field definitions setup for the storageName
*/
function updateFieldOptions(storageName, fields) {
  if (storageName === 'cust_plant_item' || storageName === 'plant_item' || storageName === 'item_order') {
    // Request is that the user can select multiple plant values
    jmespath.search(fields, "editor_fields[?name == 'Plants'] | [0]").opts['multiple'] = true;
  }
}

/**
 * Get any functions that need to fire for a editor form close
 */
function getEditorCloseFunctions(dtc) {
  var storageName = dtc.name;
  var funcs = [];
  if (storageName === 'cust_plant_item' || storageName === 'plant_item' || storageName === 'item_order') {
    funcs.push(processPlantItemClose);
  }
  return funcs;
}


/*
 * Get any functions that need to be executed before the form is submitted
 */
function getEditorPreSubmitFunctions(dtc) {
  var storageName = dtc.name;
  var funcs = [];

  //if (storageName === 'cust_plant_item' || storageName === 'plant_item' || storageName === 'item_order'){
  //   funcs.push( function ( e, data, action ) {
  //               console.log(data);
  //                })
  //}

  return funcs;
}

/**
 * This happens when someone clicks a link on the left side of the form
 * @param {*} form_name 
 */
function process_custom_form_click(form_name) {

  var currentForm = "div#cust_form_" + showCustomForm + ".one_form_container"
  var newForm = "div#cust_form_" + form_name + ".one_form_container"
  $(currentForm).hide();
  showCustomForm = form_name;
  $(newForm).show();
  $('.co-item-select').select2({ placeholder: "Please Select One..." });
  $('.co-item-select_plants').select2({placeholder: "Select A Value..."});
}

/**
 * 
 * @param {image_id: <image_id,image_url: <image_url>} imgInfo 
 */
function add_plant_item_img(imgInfo) {
  var liStart = '<li id="' + imgInfo.image_id + '">';
  var img_tag = '<img onclick="show_plant_item_image(\''+imgInfo.image_url +'\')"class="dt_image" style="cursor: pointer;" src="'+imgInfo.image_url +'">';
  var paddingSpan = '<span style="padding-left: 2em;"></span>';
  var removeSpan = '<span style="cursor: pointer; color: red;" onclick="remove_plant_item_img(\''+imgInfo.image_id+'\')"><i class="fas fa-times-circle fa-2x"></i></span>';
  var liEnd = "</li>";
  var imgHtml = liStart + img_tag + paddingSpan + removeSpan + liEnd;
  $('#plant_item_img_list').append(imgHtml);
  //current_plant_item.plant_image.push(imgInfo);
}

/**
 * Show an image in the modal
 * @param {} img 
 */
function show_plant_item_image(img){
    modal.open({content:'<div>click to center</div><div><span onclick="modal.center()"><img src="'+img+'"></span></div>'});
    modal.center();
}

/**
 * Remove image with the given id from the list of images
 */
function remove_plant_item_img(imgId){
    $('li#'+imgId).remove();
}

/**
 * Allows user to clear a drop down selection
 * @param {*} select_id 
 */
function clear_select2_selection(select_id){
    $('select#'+select_id).val(null).trigger("change");
    $('select#'+select_id).select2({placeholder: "Select A Value..."});
}

/**
 * After an image is selected from the file system.. this will give a preview
 * @param {*} input 
 */
function plant_item_readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      $('#plant_item_imageResult')
        .attr('src', e.target.result);
    };
    reader.readAsDataURL(input.files[0]);
    $('#plant_item_img-buttons').show();
    remove_loading("ImageSelected");
  }
}

/**
 * Not sure this function is needed... copied it from an example
 */
$(function () {
  $('#plant_item_upload').on('change', function () {
    plant_item_readURL(this);
  });
});


/**
 * I don't think this one is needed either.. but gets called from an event??
 * leave it for now
 * @param {*} event 
 */
function showFileName(event) {
  var input = event.srcElement;
  var fileName = input.files[0].name;
  infoArea.textContent = 'File name: ' + fileName;
}

/**
 * So we like the preview.. this gets called when they click the "add" button
 * We'll post it to the server
 */
function pi_upload_img() {
  add_loading("ImageUpload");
  var fd = new FormData();
  fd.append('upload',$('input#plant_item_upload')[0].files[0]);
  var ajaxOptions = {url:'/upload_image',type:'post',data:fd,contentType:false,processData:false};
  ajaxOptions['success'] = function(resp){add_plant_item_img(resp.upload.id); pi_clear_img(); remove_loading("ImageUpload");};
  ajaxOptions['error'] = function(response){remove_loading("ImageUpload"); modal.open({content:'<h3>Upload Error!</h3>'}); console.log(response);};
  $.ajax(ajaxOptions);
}

/**
 * Don't like the preview and want to discard the image
 */
function pi_clear_img() {
  var $image = $('#plant_item_imageResult');
  $image.removeAttr('src').replaceWith($image.clone());
  $('#plant_item_img-buttons').hide();
  infoArea.textContent = 'Choose file';
}

/**
 * A plant is being added... if it is just a name we'll  create the structure
 * otherwise expectation is that it is an object with keys "plant" and "qty"
 * the "plant" key will be a value like "recipe_costing-134|Bonita"
 * @param {*} plant_obj 
 */
function add_plant_item_plant(plant_obj){
  if (typeof plant_obj === 'string'){
    var parts = plant_obj.split("|");
    var _id = parts[0];
    var name = parts[1];
    var qty = 1;
    plant_obj = {id: _id,name: name,qty: qty,value:plant_obj};
  } else{
    var parts = plant_obj.plant.split("|");
    var _id = parts[0];
    var name = parts[1];
    plant_obj['id'] = _id;
    plant_obj['name'] = name;
  }
  var plHtml = [];
  plHtml.push('<tr id="one_plant_item_plant_'+plant_obj.id+'">');
  plHtml.push('<th scope="row" id="one_plant_item_plant_id_'+plant_obj.id+'">'+plant_obj.id+'</th>');
  plHtml.push('<td id="one_plant_item_plant_name_'+plant_obj.id+'">'+plant_obj.name+'</td>');
  plHtml.push('<td id="one_plant_item_plant_qty_'+plant_obj.id+'">'+plant_obj.qty+'</td>');
  plHtml.push('<td id="one_plant_item_plant_buttons_'+plant_obj.id+'">');
  plHtml.push('<button id="plant_item_edt_plant_'+plant_obj.id+'" onclick="edit_pi_plants_entry(\''+plant_obj.id+'\')" type="button" class="btn btn-primary"><span><i class="fas fa-edit"></i></span></button>');
  plHtml.push('<button id="plant_item_del_plant_'+plant_obj.id+'" onclick="delete_pi_plants_entry(\''+plant_obj.id+'\')" type="button" class="btn btn-primary"><span><i class="far fa-times-circle"></i></span></button>');
  plHtml.push('</tr>');
  $('#table_body_plant_item_plants').append(plHtml.join(''));
}

/**
 * Basically open the modal and create a drop down plant selection
 * and entry for the Quantity
 * A button will add it to the table... essentiall calling add_plant_item_plant
 */
function add_plant_to_form(){
    __create_pi_plant_modal_content(undefined);
}

/**
 * if plt_data is null...fill out junk data
 * @param {*} plt_data 
 */
function __create_pi_plant_modal_content(plt_data){
   var plInfo = {};
   if (isNull(plt_data)){
       plInfo['id'] = '123';
       plInfo['name'] = '';
       plInfo['qty'] = 1;
       plInfo['value'] = '';
   } else {
       plInfo = plt_data;
       plInfo['value'] = plt_data.id+"|"+plt_data.name;
   }
   var pHtml = [];
   pHtml.push('<div><div id="edit_add_pi_plant_id" style="display: none;">'+plInfo.id+'</div>');
   pHtml.push('<div id="edit_add_pi_plant_value" style="display: none;">'+plInfo.value+'</div>');
   pHtml.push('<div><select id="edit_add_pi_plants_name" class="form-control co-item-select_plants"></select></div>');
   pHtml.push('<div id="UPDATE_ADD_PI__INV" class="invalid-feedback"></div>');
   pHtml.push('<div><input type="text" class="form-control" id="edit_add_pi_plants_qty" value="'+plInfo.qty+'"></input></div>');
   pHtml.push('<div><button id="edit_add_pi_plants_button" onclick="process_pi_plants_modal()" type="button" class="btn btn-primary">Save</button></div></div>');
   modal.open({content: pHtml.join('')});   
   var optArr = get_option_data_callback(plant_item_plantField, cpi_plant_callback);
   cpi_plant_loading(optArr, plant_item_plantField);
}

/**
 * Grab the data selected and update or add it to the table
 */
function process_pi_plants_modal(){
  var _id = $('div#edit_add_pi_plant_id').text().trim();
  var prevValue = $('div#edit_add_pi_plant_value').text().trim();
  var value = $('select#edit_add_pi_plants_name').val();
  var dup = false;
  if (_id === '123'){
    dup = $('tr#one_plant_item_plant_'+value.split("|")[0]).length > 0;
  }else{
    dup = prevValue !== value && $('tr#one_plant_item_plant_'+value.split("|")[0]).length > 0;
  }
  if (dup){
    $('div#UPDATE_ADD_PI__INV').text("This plant is a duplicate... ");
    $('div#UPDATE_ADD_PI__INV').show();
  }else{
    var qty = $('input#edit_add_pi_plants_qty').val();
    if (_id !== '123'){
        $('tr#one_plant_item_plant_'+_id).remove()
    }
    add_plant_item_plant({plant: value,qty: qty});
    modal.close();
  }
  
}

/**
 * grab data elements for:
 * one_plant_item_plant_id_{plant_id}
 * one_plant_item_plant_name_{plant_id}
 * one_plant_item_plant_qty_{plant_id}
 * @param {*} plant_id 
 */
function edit_pi_plants_entry(plant_id){
   console.log("looking to edit a plant... ");
   var _id = $('#one_plant_item_plant_id_'+plant_id).text();
   var name = $('#one_plant_item_plant_name_'+plant_id).text();
   var qty = $('#one_plant_item_plant_qty_'+plant_id).text();
   __create_pi_plant_modal_content({id:_id,name:name,qty:qty});
}

/**
 * Just grab the row...
 * one_plant_item_plant_{plant_id}
 * @param {*} plant_id 
 */
function delete_pi_plants_entry(plant_id){
   console.log("Looking to delete a plant... ");
   $('#one_plant_item_plant_'+plant_id).remove();
}

/**
 * when we finally get the options.. this function will grab it and load the drop down selector
 * @param {*} strgFld 
 */
function cpi_plant_callback(strgFld) {
  var jPath = parse_option_container(strgFld);
  var select_options = jmespath.search(pageOptions, jPath);
  cpi_plant_loading(select_options, strgFld);
}

/**
 *  Go through fields and grab updated information
 * Special processing for plants and images
 */
function grab_plant_items_data_update(){
    // plant_item_dtc
    var piFields = plant_item_dtc.storage_container.fields;
    piFields.forEach(piFld =>{
       var oldVal = current_plant_item.data[piFld.field_name];
       var newVal = '';
       if (piFld.is_option_filled){
          if (piFld.field_name === 'Plants'){
            var rows = $('table#table_plant_item_plants > tbody > tr');
            var newVal = [];
            var i = 0;
            for (i = 0; i < rows.length; i++) {
                newVal.push(__process_pi_plants_row(rows[i]));
            }
          }else{
            newVal = $('select#' + piFld.field_name).val();
          }
       }else{
         if (piFld.field_name === 'plant_image'){
           // get from img div
           var imgsLi = $('ul#plant_item_img_list > li')
           newVal = [];
           for (i = 0; i < imgsLi.length; i++) {
             var img = imgsLi[i];
             var image_id = $(img).attr('id');
             var image_url = $($(img).find('img')[0]).attr('src');
             newVal.push({image_id:image_id,image_url:image_url});
           }
         }else{
           // get from input
           newVal = $('input#'+piFld.field_name).val();
         }
       }
       current_plant_item.data[piFld.field_name] = newVal;
    });
}

/**
 * For each plant added.. create an object like
 * {plant: 'recipe_costing-139|Bonita',qty:3}
 * @param {*} inRow 
 */
function __process_pi_plants_row(inRow){
    var _id = $(inRow).attr('id').replace('one_plant_item_plant_','');
    var cols = $(inRow).find('td');
    var result = {};
    var i = 0;
    for (i = 0; i < cols.length; i++) {
      var col = cols[i];
      var key = $(col).attr('id').replace('one_plant_item_plant_','').replace('_'+_id,'');
      if (key !== 'buttons'){
        var value = $(col).text();
        if (key === 'name'){
          key = 'plant';
          value = _id+"|"+$(col).text()
        }
        result[key] = value
      }
    }
    return result;
}

/**
 * given the options load them for the field passed.. in this case it is for the plant add modal
 * @param {*} options_arr 
 * @param {*} field 
 */
function cpi_plant_loading(options_arr, field) {
  options_arr.forEach(optVal => {
    var nd = d3.select(document.createElement('div'));
    nd.append('option').attr('value',optVal.value).text(optVal.label);
    $('select#edit_add_pi_plants_name').append(nd.html());
  });
  $('select#edit_add_pi_plants_name').select2({placeholder: "Select A Value..."});
  var selValue = $('div#edit_add_pi_plant_value').text().trim();
  if (selValue === ''){
    selValue = null;
  }
  $('select#edit_add_pi_plants_name').val(selValue).trigger('change');
}

/**
 * Remove a plant entry from the form
 * @param {*} plant_id 
 */
function plant_item_remove_plant(plant_id){
  $('div#one_plant_'+plant_id).remove();
}

/*
* Here we'll add any custom editor buttons
 */
function getCustomEditorFormButtons(selBtns, dtc) {
  var storageName = dtc.name;

  if (storageName === 'cust_plant_item' || storageName === 'plant_item') {
    //selBtns['edit'] = { extend: 'edit', editor: dtc.dt_editor, formButtons: ['Update Item',{label: '<div><div id="recipeCalcLoading" class="page_hide"><i class="fas fa-spinner fa-spin"></i></div><div>Update Recipe Costs</div></div>',
    //                                                                          fn: getUpdatedRecipeCost}]}
    selBtns['create'] = {
      extend: 'create', editor: dtc.dt_editor, formButtons: ['Save Item', {
        label: 'Update Recipe Costs',
        fn: getUpdatedRecipeCost
      }]
    }
    editButton = get_or_default(selBtns, 'edit', undefined);
    if ((isNotNull(editButton)) && storageName == 'cust_plant_item') {
      selBtns['edit'] = run_plant_item_form();
    }

    if ((isNotNull(editButton)) && storageName == 'plant_item') {
      selBtns['edit'] = run_plant_item_form();
    }
  }
  return selBtns;
}

/**
 * This is the new updated way to edit a plant_item
 */
function run_plant_item_form() {
  var new_edit = {
    extend: 'selected',
    text: 'Edit Item',
    action: process_edit_plant_item
  };
  return new_edit;
}

/**
 * Process an edit click
 * @param {*} e 
 * @param {*} dt 
 * @param {*} node 
 * @param {*} config 
 */
function process_edit_plant_item(e, dt,node,config){
  var vd = dt.data()[dt.rows({ selected: true })[0][0]];
  var dtc = dt.dataTableCreator();
  load_show_cpi_form(vd,dtc,'update',{model_name: 'cust_plant_item', parent_id: undefined, collection: 'items'});
}

/**
 * Alright.. they've clicked the button... now let's
 * - get the item we're editing.. save it to current_plant_item
 * - grab the DataTableCreator and save it to plant_item_dtc
 * - clear out the plant images
 * - clear out the plant entries
 * - hide the regular screen
 * - show the form
 * - load the data.. special instructions for plants and images
 * @param {*} vd
 * @param {*} dtc 
 */
function load_show_cpi_form(vd, dtc, action, createInfo) {
  process_custom_form_click('plant_img');
  var location_inv_loc_default = 'unknown';
  var location_id = vd.parent_id;
  if (isNotNull(location_id) && (dtc.storage_container.parent.id !== 'admin_items')){
    var location_data = jmespath.search(dtc_items[dtc.storage_container.parent.id].get_data(),"[?DT_RowId == '"+location_id+"'] | [0]");
    if (isNotNull(location_data)){
      if (isNotNull(location_data.inventory_location)){
        location_inv_loc_default = location_data.inventory_location;
      }
    }
  } else {
    $('#item_form_inv_location_div').hide();
  }

  $('#item_inv_location_text').text("");
  $('#item_inv_location_text').append("<span>Inventory Location (Default: <strong>"+location_inv_loc_default+"</strong>)</span>");
  
  current_plant_item = {action: action, data: vd,model_name: createInfo.model_name, parent_id: createInfo.parent_id, collection: createInfo.collection};
  plant_item_dtc = dtc;
  $('ul#plant_item_img_list').empty();
  $('#table_body_plant_item_plants').empty();
  $('#page_display').hide();
  $('#custom_forms').show();
  var tFlds = dtc.storage_container.fields;
  tFlds.forEach(tFld => {
    if (tFld.is_option_filled) {
      if (tFld.field_name === 'Plants') {
        plant_item_plantField = tFld;
        var pltArr = vd[tFld.field_name];
        pltArr.forEach(plt => {
          add_plant_item_plant(plt);
        });
      } else {
        $('select#' + tFld.field_name).find('option').remove();
        var optArr = get_option_data_callback(tFld, cpi_custom_option_callback);
        cpi_custom_option_loading(optArr, tFld);
        $('select#' + tFld.field_name).val(get_or_default(vd, tFld.field_name, tFld.field_default)).trigger('change'); 
      }
    } else {
      if (tFld.field_name === 'plant_image') {
        var imgArr = vd[tFld.field_name];
        imgArr.forEach(img => {
          add_plant_item_img(img);
        });
      } else {
        $('input#' + tFld.field_name).val(get_or_default(vd, tFld.field_name, tFld.field_default));
      }
    }

  });
}

/**
 * This loads the options for the selections for the form
 * @param {*} strgFld 
 */
function cpi_custom_option_callback(strgFld) {
  var jPath = parse_option_container(strgFld);
  var select_options = jmespath.search(pageOptions, jPath);
  cpi_custom_option_loading(select_options, strgFld);
}

/**
 * Given the options loaded... we'll setup the select2 entries
 * @param {*} options_arr 
 * @param {*} field 
 */
function cpi_custom_option_loading(options_arr, field) {
  options_arr.forEach(optVal => {
    var nd = d3.select(document.createElement('div'));
    nd.append('option').attr('value',optVal.value).attr('id',optVal.id).text(optVal.label);
    $('select#' + field.field_name).append(nd.html());
  });
  var sel2Options = {placeholder: "Select A Value..."};
  if (field.isList()){
    sel2Options['multiple'] = true;
  }
  $('select#' + field.field_name).select2(sel2Options);
}

/**
 * 3 action types supported... 
 * - Delete:  Remove the item 
 * - Save: Update/Save the item
 * - Cancel:  go back to the previous screen
 * 
 * for Save and Delete... we'll have to update the Datatable with the new data or updated data
 * @param {*} action_type 
 */
function process_custom_form_action(action_type) {
  if (action_type === 'cancel') {
    __clear_invalid_feedback();
    $('#custom_forms').hide();
    $('#page_display').show();
  }
  if (action_type === 'save') {
    grab_plant_items_data_update();
    var errors = plant_item_dtc.validate_form_data(current_plant_item.data);
    if (errors.length === 0){
      __save_form_entry_plant_item();
    }else{
      __display_form_errors(errors);
    }
  }
  if (action_type === 'costs') {
    grab_plant_items_data_update();
    __update_costs_form_entry_plant_item();
  }
}

/**
 * Make sure we have a clear slate!!
 */
function __clear_invalid_feedback(){
  var fldNames = plant_item_dtc.storage_container.getFieldNames();
  fldNames.forEach(fld =>{
    $('div#'+fld+'__INV').empty();
  });
  $('.invalid-feedback').hide();
}

/**
 * Given the list of errors in the form {error: <error message>, field: <field_name>}
 * go through each error and update the following div <field_name>__INV
 * then show the invalid feedback
 * .invalid-feedback
 * @param {*} errors 
 */
function __display_form_errors(errors){
  __clear_invalid_feedback()
  var errHtml = '<div><h4>The following fields had errors!</h4><ul>';
  errors.forEach(error => {
    $('div#'+error.field+"__INV").append(error.error+"<br>");
    errHtml = errHtml + '<li>'+error.field+'</li>';
  });
  $('.invalid-feedback').show();
  errHtml = errHtml + '</ul></div>';
  modal.open({content:errHtml});
}

/**
 * Save the current_plant_item
 */
function __save_form_entry_plant_item(){
  var ajaxOptions = {url:'/plant_item_update',type:'post',data:JSON.stringify(current_plant_item),contentType: "application/json"};
  ajaxOptions['success'] = __handle_pi_update_success;
  ajaxOptions['error'] = __handle_pi_update_error;
  add_loading("PI_Update");
  $.ajax(ajaxOptions);
}

/**
 * Here we'll send for a recipe update
 */
function __update_costs_form_entry_plant_item(){
  var payload = {data: current_plant_item.data,data_type: plant_item_dtc.storage_container.storage_name};
  var ajaxOptions = {url:'/update_recipe_costs',type:'post',data:JSON.stringify(payload),contentType: "application/json"};
  ajaxOptions['success'] = __handle_pi_cost_success;
  ajaxOptions['error'] = __handle_pi_update_error;
  add_loading("Cost_Update");
  $.ajax(ajaxOptions);
}

/**
 * get the response and save the values to the current data item
 * @param {*} response 
 */
function __handle_pi_cost_success(resp){
  remove_loading('Cost_Update');
    var cost = resp.cost.total_cost;
    var costFormat = cost.format(2);
    current_plant_item.data.recipe_cost = costFormat;
    $('input#recipe_cost').val(costFormat);
    var listPrice =$('input#List_Price').val();
    var profitMargin = (listPrice - costFormat).format(2);
    current_plant_item.data.List_Price = profitMargin;
    $('input#profit_margin').val(profitMargin);
}

/**
 * It was a success... now
 * 1. Update The Data Table
 * 2. Hide the form
 * 3. Show the Display
 * @param {*} response 
 */
function __handle_pi_update_success(response){
   remove_loading("PI_Update");
   load_show_cpi_form(response.data, plant_item_dtc, response.action,{model_name: 'cust_plant_item', parent_id: undefined, collection: 'items'});
   if (response.action == 'create'){
     plant_item_dtc.dt_table.row.add(response.data).draw(false);
   }else{
     var editRowNum = plant_item_dtc.dt_table.rows({ selected: true })[0][0];
     plant_item_dtc.dt_table.row(editRowNum).data(response.data).draw();
   }
   process_custom_form_action('cancel');
}

/**
 * There was an error..
 * 1. Pop the modal and show some information
 * 2. Give the user an opportunity to notify an admin
 * @param {*} response 
 */
function __handle_pi_update_error(response){
  remove_loading("PI_Update");
  console.log("Error...");
  var rawErr = response.responseJSON.error;
  var regMatch = /Exception: (?<err_msg>.*)/.exec(rawErr)
  var errMsg = "";
  if (isNotNull(regMatch) && isNotNull(regMatch.groups)){
    errMsg = regMatch.groups.err_msg;
  }
  var today = new Date();
  var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
  var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
  var errTime = date+" "+time;
  var err = rawErr.replace(/(\r\n|\n|\r)/g,"<br />");
  var errHtml = "<div><div><h3>ERROR SAVING..</h3><br><h4>"+errMsg+"</h4></div><div><p>"+err+"</p></div></div>";
  var notify = '<div><a href="#" onclick="system_error_notification(\''+escape(rawErr)+'\',\''+errTime+'\')">Notify System Admin</a></div>';
  modal.open({content: errHtml+notify});
  modal.center();
}

/**
 * This is the opportunity to grab updated data and get a new recipe cost
 */
function getUpdatedRecipeCost() {
  var dtc = this.dataTableCreator();
  var ff = dtc.dt_editor.fields();
  var dataValues = {};
  $.each(ff, function (idx, f) {
    dataValues[f] = dtc.dt_editor.val(f);
    if (Array.isArray(dataValues[f])) {
      dataValues[f] = dataValues[f].join(",");
    }
  });
  $('div#recipeCalcLoading').show();
  updateRecipeCostCalc(dtc, dataValues);
}

/*
 * Get any functions that need to execute once the form is opened
 */
function getEditorOpenFunctions(dtc) {
  var storageName = dtc.name;
  var funcs = [];
  if (storageName === 'cust_plant_item' || storageName === 'plant_item' || storageName === 'item_order') {
    // funcs.push(processPlantItemOpen);
    funcs.push(setRecipeCalcsReadOnly);
  }

  if (storageName === 'cust_plant_item' || storageName === 'plant_item') {
    funcs.push(showPlantTemplate);
  }

  funcs.push(setupImages);

  return funcs;
}

/**
 * If you edit form needs a custom template, specify that here
 */
function getCustomTemplates(dtc) {
  if (dtc.name == "plant_item" || dtc.name == "cust_plant_item") {
    return '#item_form';
  }
  return undefined;
}

/**
 *  set the recipe update flag to false
 */
function processPlantItemClose() {
  var dtc = this.dataTableCreator();
  dtc.dtc_states['PLANT_ITEM_RECIPE'] = false;
}
/**
 * The context here for "this" is the dt_editor
 */
function processPlantItemOpen(e, mode, action) {
  // get the DataTableCreator
  var dtc = this.dataTableCreator();
  getEditorItemCost(dtc, function (resp) {
    var cost = resp.cost.total_cost;
    dtc.dt_editor.field('recipe_cost').val(cost.format(2));
  });
}

/**
* Set fields for recipe to readonly
*/
function setRecipeCalcsReadOnly(e, mode, action) {
  var dtc = this.dataTableCreator();
  this.field('recipe_cost').disable();
  this.field('profit_margin').disable();
}

/**
 *
 */
function addFieldChangeEvents(dtc) {
  var storageName = dtc.name;
  //
  // removed on 6/4/2019 inconsistent on how it works
  //
  //if (storageName === 'cust_plant_item' || storageName === 'plant_item' || storageName === 'item_order'){
  //    addRecipeChangeProcessing(dtc);
  //}
  // Box change event added on 6/24/2019
  if (storageName === 'cust_plant_item' || storageName === 'plant_item' || storageName === 'item_order') {
    addRecipeBoxChangeEvent(dtc);
  }

}

/**
 * Adding an event processing mechanism anytime the field Box changes... as long as it is a valid value
  * @param dtc
 */
function addRecipeBoxChangeEvent(dtc) {
  dtc.dt_editor.dependent("Box", processBoxChange, { event: 'change' });
}
/**
* Set PLANT_ITEM_RECIPE to false before you start
*/
function addRecipeChangeProcessing(dtc) {
  dtc.dtc_states['PLANT_ITEM_RECIPE'] = false;
  var recipeFields = jmespath.search(dtc.storage_container, "fields[?category == 'Recipe']");
  $.each(recipeFields, function (idx, fld) {
    dtc.dt_editor.dependent(fld.field_name, processRecipeChange, { event: 'change' });
  });
}

/**
 *
 * @param val
 * @param data
 * @param callback
 */
function processBoxChange(val, data, callback) {
  if (isNotNull(val)) {
    var parts = val.split("|");
    if (parts.length > 1) {
      var boxId = parts[0];
      $.get('/get_box_dimensions/' + boxId, updateDimensions);
    }
  }
}

function updateDimensions(recipeItem) {
  if (isNotNull(recipeItem) && isNotNull(recipeItem.data)) {
    var len = recipeItem.data.length;
    var wid = recipeItem.data.width;
    var hgt = recipeItem.data.height;
    var dtc = null;
    if (isNotNull(pageReturn.tables['cust_plant_item-table'])) {
      dtc = pageReturn.tables['cust_plant_item-table'];
    } else {
      dtc = pageReturn.tables['plant_item-table']
    }
    dtc.dt_editor.field('Case_Length').val(len);
    dtc.dt_editor.field('Case_Height').val(hgt);
    dtc.dt_editor.field('Case_Width').val(wid);
  }

}

/**
*  This function gets called anytime a recipe field gets changed
*  Problem is that this also gets called on the initial load of the form
*   -- Solution:  Set a flag to only update the recipe cost after the first field has a changed value
*/
function processRecipeChange(val, data, callback) {
  if (isNotNull(data) && isNotNull(data.row)) {
    // First step here is to get the information we need to get the DataTableCreator
    var lookupVal = data.row.Cust_Item_Num;
    if (lookupVal === undefined) {
      lookupVal = data.row.Item_Num;
    }

    if (lookupVal !== undefined) {
      lookupVal = lookupVal.split('-')[0] + "-table";
      var dtc = pageReturn.tables[lookupVal];
      var recipeUpdate = get_or_default(dtc.dtc_states, 'PLANT_ITEM_RECIPE', false);
      if (recipeUpdate) {
        console.log('sending for recipe cost calc');
        if (Array.isArray(data.values.Plants)) {
          data.values.Plants = data.values.Plants.join(",");
        }
        updateRecipeCostCalc(dtc, data.values);
      } else {
        var recipeFields = jmespath.search(dtc.storage_container, "fields[?category == 'Recipe']");
        $.each(recipeFields, function (idx, fld) {
          // does it match and did it change??
          if ((data.values[fld.field_name] === val) && (val !== data.row[fld.field_name])) {
            console.log('Something changed.. now update recipe cost  calc');
            dtc.dtc_states['PLANT_ITEM_RECIPE'] = true;
            if (Array.isArray(data.values.Plants)) {
              data.values.Plants = data.values.Plants.join(",");
            }
            updateRecipeCostCalc(dtc, data.values);
          }
        });
      }
    }
  }
}

/**
 * update the recipe cost calculation
 */
function updateRecipeCostCalc(dtc, updateData) {
  postRecipeCost(dtc, updateData, function (resp) {
    var cost = resp.cost.total_cost;
    var costFormat = cost.format(2);
    dtc.dt_editor.field('recipe_cost').val(costFormat);
    var listPrice = dtc.dt_editor.field('List_Price').val();
    var profitMargin = (listPrice - costFormat).format(2);
    dtc.dt_editor.field('profit_margin').val(profitMargin);
    $('div#recipeCalcLoading').hide();
  });
}

function showPlantTemplate() {
  $('#item_form').show();
}

function setupImages() {
  var imgFields = $('.DTE_Field_Type_upload.dt_image_full');
  for (var x = 0; x < imgFields.length; x++) {
    var imgFld = imgFields[x];
    var imgDiv = $(imgFld).find('.form_image_div');
    if (imgDiv.length > 0) {
      $(imgDiv).empty();
    }
    var imgId = $(imgFld).find('.dt_image').parent('div').attr('id');
    var imgInfo = imageInformation[imgId];
    if (imgInfo !== undefined) {
      var imgLocator = 'form_image_' + imgId + ''
      var imgTag = '<img class="' + imgLocator + '"src="' + imgInfo.web_path + '"/>';
      if (imgDiv.length > 0) {
        $(imgDiv.append(imgTag));
      } else {
        imgTag = '<div class="form_image_div">' + imgTag + '</div>'
        $(imgFld).append(imgTag);
      }
    }
  }
}
