# -*- coding: utf-8 -*-

from flask import request, render_template
from flask.views import View

from application.decorators import login_required
from application.errors import InvalidUsage
from application.models import DataNumberLookup
from application.views.common import return_one_page, return_page, \
    generate_invoice, get_item


class LoginHome(View):

    @login_required
    def dispatch_request(self):
        return render_template('home.html')
    

class Page_Display(View):
    
    @login_required
    def dispatch_request(self,path):
        return return_one_page(path,request.args)
    
class Item_Display(View):
    
    @login_required
    def dispatch_request(self):
        return return_page("customer",{"first":"true"},"custom/customer_items.html")

class Item_Master_Display(View):
    
    @login_required
    def dispatch_request(self):
        return return_page("admin_items",{"first":"true"},"custom/master_items.html")

class Item_Reserve(View):

    @login_required
    def dispatch_request(self):
        return return_page("customer",{"first":"true"},"custom/item_reserves.html")
    
class Show_Item(View):
    
    @login_required
    def dispatch_request(self, itemNum):
        return get_item(itemNum)
    
class Order_Display(View):
    
    @login_required
    def dispatch_request(self):
        return return_page("customer",{"first":"true"},"custom/customer_orders.html")

class Get_Custom_HTML(View):
    
    @login_required
    def dispatch_request(self,path):
        return render_template('custom/'+path+'.html');
    
class Order_Form(View):
    
    @login_required
    def dispatch_request(self):
        return generate_invoice(request.args)
        