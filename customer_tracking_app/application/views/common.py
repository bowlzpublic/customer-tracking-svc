'''
Created on Feb 18, 2018

@author: jason
'''
import json
import sys, traceback, logging, os

from flask import render_template, jsonify
#from google.appengine.api import users, mail, app_identity

import jmespath

from application.app_utils import process_path, process_args, get_user_email, get_user_nickname, get_app_hostname, Email
from application.errors import InvalidUsage
from application.models import DataStorageType, StorageBlob, DataNumberLookup, ApiUser

def __get_env_variables():
    '''
    This will grab all environment variables needed for a return call

    FLASK_CONF: 'PROD'
  PI_NUM_SPLIT: 100
  PUBSUB_TOPIC: app_updates
  TOPIC_APP_NAME: bigquery-gitlabsync-co
  APP_NAME: customer_tracking
  # This token is used to verify that requests originate from your
  #  RUN THIS COMMAND: gcloud pubsub topics create storage_blob_data
  # application. It can be any sufficiently random string.
  PUBSUB_VERIFICATION_TOKEN: RQz0qYk0yWvwwQx5Qxbo
  ## Plant Item Frontend
  FE_URL: https://plant-item-fe-dev.appspot.com
    :return:
    '''
    retDict = {}

    retDict['FLASK_CONF'] = os.environ.get('FLASK_CONF','')
    retDict['PI_NUM_SPLIT'] = os.environ.get('PI_NUM_SPLIT','')
    retDict['PUBSUB_TOPIC'] = os.environ.get('PUBSUB_TOPIC', '')
    retDict['TOPIC_APP_NAME'] = os.environ.get('TOPIC_APP_NAME', '')
    retDict['APP_NAME'] = os.environ.get('APP_NAME', '')
    retDict['PUBSUB_VERIFICATION_TOKEN'] = os.environ.get('PUBSUB_VERIFICATION_TOKEN', '')
    retDict['FE_URL'] = os.environ.get('FE_URL', 'http://localhost:3000')

    ## Now get some other stuff that is needed
    server_url = os.environ.get('ITEM_APP_URL', 'http://localhost:8000')
    
    if server_url.endswith("/"):
        server_url = server_url[:-1]

    retDict['APP_URL'] = server_url
    retDict['API_TOKEN'] = ApiUser.getInternalApiToken()

    return retDict

def get_item(item_num):
    '''
    We assume that the thing we're getting here is a child of the plant_item type
    '''
    sbName = item_num.split("-")[0]
    schema = DataStorageType.get_dataStorageType(sbName)
    item = DataNumberLookup.get_storeblob(item_num)

    init_data = {}
    init_data['env'] = __get_env_variables()
    init_data['schema'] = schema.get_schema(True)
    init_data['path'] = item_num
    init_data['group'] = sbName
    init_data['data'] = item.get_dict()
    init_data['headers'] = ['Item',item_num]

    cin = init_data['data']['data']['customer_item_number']
    cid = init_data['data']['data']['customer_item_description']

    return render_template('custom/view_item.html',schema=json.dumps(init_data),root_name=item.data_type, root_id=item.key.id(), custItemNum=cin, custItemDesc=cid)


def return_item_page(path):
    path_info = process_path(path)
    parent = path_info['parent']
    root = path_info.get('root',{})
    default_nm = root['name']
    default_id = root['id']
    parent_name = parent.get('name',default_nm) if parent else default_nm
    parent_id = parent.get('id',default_id) if parent else default_id
    schema_name = default_nm
    #sb = StorageBlob.query(StorageBlob.data_type==path_info['root']['name']).get()
    sb = StorageBlob.getSB_of_Type(default_nm)[0]

    header1=default_nm
    header2=""
    root_id = ""
    root_name = default_nm

    if sb:
        header1 = sb.data_type+": "+sb.key_fields
        root_id = sb.path

    if not parent_id:
        parent_id = root_id

    schema = DataStorageType.get_dataStorageType(schema_name)
    init_data = {}
    init_data['env'] = __get_env_variables()
    if schema:
        s = schema.get_schema(True)
        model_name = jmespath.search("fields[?field_name == '"+parent_name+"'] | [0]",s)['group_name']
        addl_args = '&ancestry_list='+str(root_id)+'&ancestry_names='+root_name
        s['storage_url'] = '/storage_blob/dt_update?model_name='+model_name+addl_args
        init_data['schema'] = s
        init_data['path'] = path
        init_data['group'] = model_name
        init_data['args'] = {}
        init_data['args']['parent_id'] = sb.data_number_lookup
        init_data['args']['parent_path'] = sb.path
        init_data['headers'] = [header1,header2]
    else:
        init_data = {'schema':"__NOT_FOUND__",'path':path,'headers':[header1,header2]}

    return render_template('custom/item_page.html',schema=json.dumps(init_data),root_name=schema_name, root_id=sb.data_number_lookup)

def return_one_page(path, args):
    return return_page(path,args,"page.html")

def return_page(path,args,template_name):
    path_parts = path.split('/')
    #fs_type = 'document' if (len(path_parts) % 2) == 0 else 'collection'
    page_type = "TypeListing" if len(path_parts) == 1 else "TypeDisplay"
    parent_id = '' if len(path_parts) < 4 else path_parts[-3]
    parent_path = path_parts[0] if len(path_parts) < 2 else "/".join(path_parts[:-1])

    first = args.get('first','false')

    init_data = {}
    init_data['args'] = {'parent_id': parent_id, 'parent_path': parent_path,'page_type': page_type, 'model_name': parent_path.split('/')[-1]}
    init_data['env'] = __get_env_variables()

    root_nm = path_parts[0]

    schema = DataStorageType.get_dataStorageType(root_nm)

    if schema:
        s = schema.get_schema(True)
        s['storage_url'] = '/storage_blob/dt_update'
        init_data['schema'] = s
        init_data['path'] = path
    else:
        init_data = {'schema':"__NOT_FOUND__",'path':path}


    if first == "true":
        return render_template(template_name,schema=json.dumps(init_data))
    return jsonify(init_data)

def email_order(order_num, sender,base_url, new_or_edit="new"):
    receivers = []
    try:
        #emails = StorageBlob.query(StorageBlob.data_type == "invoice_email")
        emails = StorageBlob.getSB_of_Type('invoice_email')
        for email in emails:
            receivers.append(email.data['email_address'])

    except:
        logging.error("There was an error getting the email setup!!!")
        receivers = ['jasonbowles@analyticssupply.com']

    try:
        html = generate_invoice({"order_num":int(order_num)},template_name='custom/simple_email_invoice_jinja.html')

        #
        # TODO: Replace the email stuff!!
        #
        email = Email()
        email.sender=sender
        email.subject="Invoice for Order: "+str(order_num)
        if new_or_edit == "edit":
            email.subject=email.subject+" (UPDATED)"
        email.to = receivers
        email.body = "The order: {}, has been created/updated.  Please review the invoice here: {}".format(str(order_num),base_url+"/get_order?order_num="+str(order_num))
        email.html = html

        email.send()
    except:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        logging.error("email error")
        traceback.print_exception(exc_type, exc_value, exc_traceback,file=sys.stdout)

def generate_simple_invoice(request_args):
    return render_template("custom/simple_order.html")

def generate_invoice(request_args, template_name='custom/order_template_jinja.html'):
    order_num = request_args.get('order_num',None)
    #sb = StorageBlob.query(StorageBlob.data_number_lookup == "order-"+str(order_num)).get()
    sb = StorageBlob.get_by_dnl("order-"+str(order_num))

    if not sb:
        raise InvalidUsage("that order number does not exist")
    par = sb.get_parent()
    root = par.get_parent
    #user = users.GetCurrentUser()
    order_date = sb.data['order_date']
    deliv_date = sb.data['order_delivery_date']
    update_date = sb.data['update_date'][:16]
    customer_name = root.key_fields.replace("|",", ")
    location_name = par.key_fields.replace("|",", ")

    #sb_order_items = StorageBlob.query(StorageBlob.data_type == "item_order",ancestor=sb.key)
    #sb_order_items = ParentChildBlob.get_childen_by_parent(sb.key, 'item_order')
    sb_order_items = sb.get_children_by_type('item_order')

    user_name = get_user_nickname()
    user_email = get_user_email()
    order_instructions = sb.data['order_instructions']
    instructs = []
    total_num = 0
    total_cost = 0.0
    order_items = []
    instructs.append("")
    instructs.append("Item Instructions: ")
    for sboi in sb_order_items:
        item = {}
        #item_sb = DataNumberLookup.get_storeblob(sboi.data['order_item_num'])
        #if item_sb:
        item['item_num'] = sboi.data['order_item']
        item['collection'] = 'removed' #sboi.data['Collection']
        item['product_name'] = sboi.data['Product_Name']
        item['quantity'] = sboi.data['order_quantity']
        item['list_price'] = sboi.data['List_Price']
        item['item_total'] = int(sboi.data['order_quantity'])*float(sboi.data['List_Price'])
        if len(sboi.data['order_item_instructions'].strip()) > 0:
            instructs.append(sboi.data['order_item_instructions'])
        total_num = total_num + int(sboi.data['order_quantity'])
        total_cost = total_cost + item['item_total']
        order_items.append(item)
    if len(instructs) == 2:
        instructs.append("None")
    instruct_num = max(10,len(instructs)+1)


    return render_template(template_name,order_num=order_num,order_date=order_date, deliv_date=deliv_date, update_date=update_date,
                           user_name=user_name, user_email=user_email, order_instructions=order_instructions, order_items=order_items,
                           instructs=instructs, instruct_num=instruct_num,total_num=total_num,customer_name=customer_name, location_name=location_name, total_cost=total_cost )
