# -*- coding: utf-8 -*-

from flask.views import View

from flask import redirect, url_for, render_template, request, jsonify

from application.decorators import admin_required
from application.models import DataStorageType, StorageField, StorageBlob, ReportingConfig

import json

from application.decorators import login_required
from application.views.common import return_one_page, return_item_page,\
    return_page

class AdminInvoice(View):

    @admin_required
    def dispatch_request(self):
        return render_template('order_template.html')

class AdminUser(View):

    @admin_required
    def dispatch_request(self):
        return render_template("custom/api_admin.html")

class AdminHome(View):

    @admin_required
    def dispatch_request(self):
        return render_template('admin_home.html')

class Admin_Display(View):

    @admin_required
    def dispatch_request(self, path):
        return return_one_page(path,request.args)

class Admin_RecipePage(View):

    @admin_required
    def dispatch_request(self):
        path = "recipe_costing"
        args = {"first":"true"}
        return return_page(path,args,"custom/recipe_costing.html")

class Admin_ItemPage(View):

    @admin_required
    def dispatch_request(self,path):
        return return_item_page(path)

class AdminContainerSelect(View):

    @admin_required
    def dispatch_request(self):
        ret_json = request.args.get('json',"false")
        q = DataStorageType.getAll_DST()

        storeNames = ReportingConfig.getAllReporting()
        contNames = []

        ctrs = []
        for container in q:
            ctrs.append({'name':container.storage_name,'id':container.id})
            contNames.append(container.storage_name)


        rptState = {}
        for storeName in storeNames:
            rptState[storeName] = "on"

        for contName in contNames:
            if contName not in storeNames:
                rptState[contName] = "off"


        if ret_json and ret_json.lower().strip() == "true":
            return jsonify(ctrs)
        else:
            return render_template('admin_container_select.html',containers=json.dumps(ctrs),rptNames=json.dumps(rptState))

class AdminBulkLoad(View):

    @admin_required
    def dispatch_request(self):
        model_name = request.args.get('model_name',None)
        if model_name:
            if model_name.strip().lower() != "none":
                schema = DataStorageType.get_dataStorageType(model_name)

                return jsonify({"schema":schema.get_dict(),"path":model_name,
                                "args":{"base":None,"json_path":model_name,"parent":None, "root":{"name":model_name,"id":None}},
                                "parent_root":{}})
            else:
                return jsonify({"schema":{}})
        else:
            return render_template('storage_bulk_load.html')


class AdminItemUpdatePush(View):

    @admin_required
    def dispatch_request(self):
        path = "customer"
        args = {"first":"true"}
        return return_page(path,args,"custom/admin_item_push.html")
class AdminUploadSpreadsheet(View):

    @login_required
    def dispatch_request(self):
        return render_template('custom/upload_spreadsheet.html')
        
class AdminContainerUpdate(View):

    @admin_required
    def dispatch_request(self, container_name):
        container = DataStorageType.get_dataStorageType(container_name)
        if (not container.exists):
            container.storage_name = container_name
            container.extends = "none"

            container.update_ndb()
            first_field = StorageField(**{'field_name':'first_field_CHANGE_ME', 'field_type':'string','is_key_field':False})
            container.add_storage_fields([first_field])
            container.update_ndb()

        ext = container.extends
        if not ext:
            ext = "None"
        s = container.get_schema()


        return render_template('admin_container_update.html',schema=json.dumps(s), name=container_name, ext=ext, isList=container.isList, id_num=container.id)
