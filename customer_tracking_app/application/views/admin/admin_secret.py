# -*- coding: utf-8 -*-

from flask.views import View

from flask import flash, redirect, url_for, render_template, request

from application.decorators import admin_required

from application.forms import RestUserAdd


class AdminUpdate(View):
    
    @admin_required
    def dispatch_request(self):
        model_list = []
        return render_template('update_backend.html',models=model_list)