# -*- coding: utf-8 -*-

from flask.views import View
from flask import redirect, url_for


class PublicWarmup(View):

    def dispatch_request(self):
        #return render_template('custom/test_order_form.html')
        return redirect(url_for('login_home'))
