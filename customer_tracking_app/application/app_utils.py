'''
Created on Jan 21, 2018

@author: jason
'''

import re
import os
from datetime import datetime
from flask import request
from mailjet_rest import Client


class Email(object):

    def __init__(self):
        ''' hey there I send emails '''
        self.sender = None
        self.subject = None
        self.receivers = []
        self.body = None
        self.html = None
        self.signer = str(datetime.now()).replace("-","").replace(":","").replace(" ","").replace(".","")
        self.api_key = os.environ.get('MJ_API_KEY', None)
        self.api_secret = os.environ.get('MJ_API_SECRET', None)
        if self.api_key is None or self.api_secret is None:
            raise Exception(
                "Cannot setup mail client without the key or the secret")
        self.mailjet = Client(
            auth=(self.api_key, self.api_secret), version='v3.1')

    def send(self):
        if not self.sender:
            raise Exception("You must set the sender")

        if not self.subject:
            raise Exception("You did not set the subject")

        if len(self.receivers) == 0:
            raise Exception("You must have at least 1 receiver")

        if not self.body and not self.html:
            raise Exception("You need a text body or an html body")

        data = self.__create_data()
        result = self.mailjet.send.create(data=data)
        if not result.status_code == 200:
            raise Exception("None 200 status ({}), message: {}".format(
                str(result.status_code), str(result.json())))
        return result.json()

    def __create_data(self):
        to_array = [{"Email":x,"Name":x.split("@")[0]} for x in self.receivers]
        data = {
            'Messages': [
                {
                    "From": {
                        "Email": self.sender,
                        "Name": self.sender.split("@")[0]
                    },
                    "To": to_array,
                    "Subject": self.subject,
                    "TextPart": self.body,
                    "HTMLPart": self.html,
                    "CustomID": self.subject+"_"+self.signer
                }
            ]
        }

        return data


def get_user_email(defaultEmail="system@analyticssupply.com"):
    defRetEmail = 'none:'+defaultEmail
    if defaultEmail is None:
        defRetEmail = None
    user_email_str = request.headers.get(
        'X-Goog-Authenticated-User-Email', defRetEmail)
    if user_email_str is None:
        return None
    return user_email_str.split(":")[1]


def get_user_nickname(defaultNickname="system"):
    defRetNick = defaultNickname
    if defRetNick is None:
        defRetNick = None
    nickname_str = request.headers.get(
        'X-Appengine-User-Nickname', defRetNick)
    if nickname_str is None:
        return None
    return nickname_str


def get_app_hostname(defaultHost="localhost"):
    defHostName = defaultHost
    if defaultHost is None:
        defHostName = 'localhost'
    return request.headers.get('X-Appengine-Default-Version-Hostname', defHostName)


def chunks(l, n):
    """Yield n number of striped chunks from l."""
    # looping till length l
    for i in range(0, len(l), n):
        yield l[i:i + n]


'''
This is setup to replace variables in the json path returned for option variables
'''


def replace_vars(entries):
    ret_entries = []
    for idx in range(len(entries)):
        upd_entry = replace_vars_one(entries[idx], idx)
        ret_entries.append(upd_entry)
    return ret_entries


def get_root_id(json_path):
    regex = r"^((\w*)\[\?\s*id\s*==\s*`(\d+)`\])"
    matches = re.finditer(regex, json_path)
    resp = {}
    for _, match in enumerate(matches):
        resp = {'model_name': match.group(2), 'id': int(match.group(3))}

    return resp


def replace_vars_one(entry, item_num=None):
    '''
     Process and replace information in each value of the dictionary
     where the pattern matches {{variable_name}},  
     regex is r"(\{\{\s*(\w+)\s*\}\})"
    '''
    for key in entry.keys():
        jp = entry[key]
        entry[key] = replace_values(jp, entry, item_num)
    return entry


def replace_values(path, dObj, item_num=None):
    if isinstance(path, str) and path.find("{{") >= 0 and path.find("}}") > 0:
        regex = r"(\{\{\s*(\w+)\s*\}\})"
        matches = re.finditer(regex, path)
        for _, match in enumerate(matches):
            if len(match.groups()) == 2 and match.group(2) in dObj.keys():
                path = path.replace(match.group(1), str(dObj[match.group(2)]))
            elif len(match.groups()) == 2 and match.group(2) == 'idx' and item_num != None:
                path = path.replace(match.group(1), str(item_num+1))
    return path


'''
Expect that the args will be passed are foo_id, where "foo" is the field name and value is the "id"
'''


def process_gen_args(in_args):
    ret_d = {}
    for key in in_args.keys():
        if key.endswith("_id"):
            name = key[:len(key)-3]
            name_id = in_args[key]
            ret_d[name] = int(name_id)
    ret_d['option_field'] = in_args.get('option_field', None)

    return ret_d


'''
As of 3/4/2018 I need to revamp... 

The pass in will now be a long string separated by dashes.. i.e.
32432-3351-9911, would be 3 numbers 32432, 3351, 9911

that will be passed as ancestry_list

if the ancestry_list argument is there... thne it is a TypeListing, otherwise TypeDisplay

'''


def process_args(in_args):
    ret_d = {'parent_id': '', 'parent_path': '',
             'page_type': 'TypeListing'}

    if 'parent_id' in in_args.keys() and in_args['parent_id'] != '':
        ret_d['page_type'] = 'TypeDisplay'
        ret_d['parent_id'] = in_args['parent_id']

    if 'parent_path' in in_args.keys() and in_args['parent_path'] != '':
        ret_d['parent_path'] = in_args['parent_path']

    return ret_d


'''
if the string starts with "id_" return the int version after that
None otherwise
'''


def parse_id(id_path):
    if id_path.find("id_") == 0:
        return int(id_path[3:])
    return None


'''
Will return list of matches and then a list of lists for groups
'''


def process_regex(regex, reStr):

    ret_re = {'matches': [], 'groups': []}
    matches = re.finditer(regex, reStr)

    for _, match in enumerate(matches):
        ret_re['matches'].append(match.group())
        g = []
        for groupNum in range(0, len(match.groups())):
            g.append(match.group(groupNum+1))
        ret_re['groups'].append(g)
    return ret_re


'''
process the path from update_data and page_data
'''


def process_path(in_path, data=None):
    resp_d = {'root': None,
              'parent': None,
              'base': None}

    part_d = {0: 'root', 1: 'parent', 2: 'base'}

    path_parts = in_path.split("/")
    n_pts = len(path_parts)

    if n_pts > 0:
        i = 0
        p = 0
        while i < n_pts:
            if p > 2:
                break
            name = path_parts[i]
            data_id = None
            if (i+1) < n_pts:
                data_id = parse_id(path_parts[i+1])
            resp_d[part_d[p]] = {'name': name, 'id': data_id}
            i += 1
            p += 1
            if data_id:
                i += 1
        if data:
            p = p - 1
            resp_d[part_d[p]]['data'] = data
    return resp_d
