#!/bin/bash
helpFunction()
{
   echo ""
   echo "Usage: $0 -e <environment>"
   echo -e "\t-e name of environment to use ('production' or 'development')"
   exit 1 # Exit script after printing help
}

while getopts "e:" opt
do
   case "$opt" in
      e ) environment="$OPTARG" ;;
      ? ) helpFunction ;; # Print helpFunction in case parameter is non-existent
   esac
done

if [ "$environment" != 'production' ] && [ "$environment" != 'development' ]
then
   echo "You must pass a correct enviroment name, either <production> or <development> (remove the brackets)"
   helpFunction
fi
echo "Generating Secret Keys"
(cd customer_tracking_app/application;python generate_keys.py --force)

# Begin script in case all parameters are correct
echo "Using Environment: -->$environment<--"
gcloud auth activate-service-account --key-file /home/analyticssupply/tmp/auth.json
rm customer_tracking_app/live_app.yaml

if [ "$environment" = 'production' ]
then
   echo "Deploying to Production..."
   cp customer_tracking_app/app.yaml customer_tracking_app/live_app.yaml
   gcloud config set project colororchids-apps
else
   if [ "$environment" = 'development' ]
   then
      echo "Deploying to Development..."
      cp customer_tracking_app/items_srv_py3_dev.yaml customer_tracking_app/live_app.yaml
      gcloud config set project backend-firestore-test
   else
      echo "Boy... I have no idea what you just said...Environment: $environment"
      helpFunction
   fi
fi

(cd customer_tracking_app;gcloud app deploy live_app.yaml --quiet)
echo "Deploy Complete"

rm customer_tracking_app/live_app.yaml