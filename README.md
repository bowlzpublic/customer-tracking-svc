## This is the Customer Tracking Application
From this we'll use endpoints, datastore, and then the eCommerce application
[Dev Environment](https://customer-tracking-188018.appspot.com/home)


## For recipe data

This is the process I used to update the recipe data
1. Save the file to ODS (linux open office)
2. Manipulate the file and save as a csv
3. go to https://www.csvjson.com/csv2json
4. Convert to Json
5. Download JSON
6. Save all of this in folder [test_data]


## API for getting plant information

[Also Documented in Google Docs](https://docs.google.com/document/d/1DuehmSQX79LFt6miMw_f4l_D1e4jbHhPiAArv_5hVJc/edit?usp=sharing)

Code to call and get list of plant items:  Returns 2 types "plant_item" which is the "master list" and then "cust_plant_item" which is the customer versions of the master list.

```javascript
var received_token;
var items;

$.get('http://localhost:8888/api/auth?userName=test&userPass=testaa',function(data){
    received_token = data.token;});

var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8888/api/data/sb/v1/plant_items",
    "method": "GET",
    "headers": {
        "Authorization": "Bearer "+received_token
    }
}

$.ajax(settings).done(function (response) {
    items = response;
})
```

After this you should have a variable called "items" with the following information

```
{
  "cust_plant_item": [
    "cust_plant_item-103",
    "cust_plant_item-100",
    "cust_plant_item-101",
    "cust_plant_item-105",
    "cust_plant_item-106"
  ],
  "plant_item": [
    "plant_item-105",
    "plant_item-137",
    "plant_item-121",
    "plant_item-113",
    "plant_item-129",
    "plant_item-109",
    "plant_item-125",
    "plant_item-117"]}

```
The plant_item as said before is the "master list"... this is the generic list of items for customers, the cust_plant_item is the customer version of a plant_item (or could be completely custom)

Now here is how to call for that information:

```javascript
var item;
var settings2 = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8888/api/data/sb/v1/get_item/"+items['plant_item'][0],
    "method": "GET",
    "headers": {
        "Authorization": "Bearer "+received_token
    }
}

$.ajax(settings2).done(function (response) {
    item = response;
})
```

Now you should have all the details of one item that was returned

```
{
  "Assortment": "",
  "Availability": "False",
  "Box": "6729011161989120|Box #02",
  "Branches": null,
  "CO_Case_Num": "9206",
  "CO_Item_Num": "9201VER",
  "Case_Height": "20",
  "Case_Length": "23",
  "Case_Quantity": "0",
  "Case_UPC": "851203005053",
  "Case_Weight_lbs": "25",
  "Case_Width": "12",
  "Cases_Per_Pallet": "18",
  "Collection": "Large Garden",
  "Commission_Pricing": "16.5",
  "Consumer_UPC": "851203005015",
  "Cust_Item_Num": "cust_plant_item-103",
  "Double_Spike_Percent": "0",
  "Ethyl_Block": "0",
  "Grade": null,
  "Heat_Pack": "0",
  "Hi": "3",
  "Insert": null,
  "Is_Available_For_Samples": "True",
  "Item_Num": "plant_item-118",
  "Lead_Time_For_Sample": "False",
  "List_Price": "16.5",
  "List_Price_FOB_CO": "15.675",
  "POP": null,
  "Pack_Size": "6",
  "Pick": null,
  "Plants": [
    "6372769394589696|2.5\" Frosty Fern",
    "6654244371300352|2.5\" Kalanchoe"
  ],
  "Pot_Size_ML": "236",
  "Pot_Size_Oz": "8",
  "Product_Name": "Versailes",
  "Recipe": "",
  "Simple_Wick": null,
  "Sleeve": null,
  "Special_Instructions_Recipe": "",
  "Staging": "",
  "Tag_Type": null,
  "Ti": "6",
  "Top_Dressing": null,
  "Tray": "6531099068989440|Tray #04",
  "UPC_Location": "Custom",
  "Vase_Style": "6557487348056064|3\" Duo Designer",
  "customer_case_number": "232",
  "customer_item_description": "sssss",
  "customer_item_number": "sfssss",
  "plant_image": [
    {
      "description": "Uploaded File From Datatables",
      "id": 6173208034148352,
      "image_number": "image-111",
      "image_url": "http://127.0.0.1:8888/_ah/img/encoded_gs_file:YXBwX2RlZmF1bHRfYnVja2V0LzIwMTkwNjI1MDQwODMwNzk1MDAwX2Zsb3dlcjIuanBn",
      "name": "flower2.jpg"
    }
  ],
  "profit_margin": "10.83",
  "recipe_cost": "5.67",
  "retail_price": "32"
}
```
