Select Distinct Pick
from
  (SELECT distinct Pick FROM `bigquery-gitlabsync-co.customertracking.cust_plant_item`
   union all
   SELECT distinct Pick FROM `bigquery-gitlabsync-co.customertracking.plant_item`)
