FROM registry.gitlab.com/analyticssupply_engineering/docker-images/python_3_base:3-7-365_clt1

ENV FLASK_ENV=test

ENV APP_FIRESTORE_COMPANY=Color_Orchids

ENV APP_FIRESTORE_NAME=Customer_Tracking

ENV FLASK_CONF='DEV'
ENV PUBSUB_TOPIC='repush_topic'
ENV TOPIC_APP_NAME='get-drive-data-as'
ENV APP_NAME='service-client1'
ENV PUBSUB_VERIFICATION_TOKEN='dl8jVNIK4BPLdRyWIqWo'
ENV GOOGLE_CLOUD_PROJECT='service-client1'
ENV DATA_DOWNLOAD_BUCKET='analytics_supply_download_data'

RUN mkdir /home/company1

COPY . /home/company1